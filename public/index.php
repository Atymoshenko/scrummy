<?php
if ($_SERVER['HTTP_HOST'] == 'dev.scrummy.net') {
//	$cookie = $_GET['taccess'] ?? null;
//	if ($cookie) {
//		setcookie('taccess', $cookie, time()+1728000);
//	}
	// 'Validator.nu/LV http://validator.w3.org/services'
	if (
		!in_array($_SERVER['REMOTE_ADDR'], ['77.52.199.234','176.36.20.111','178.54.253.146'])
		&& (!isset($_SERVER['HTTP_USER_AGENT']) || !strstr($_SERVER['HTTP_USER_AGENT'], 'validator.w3.org'))
		/*&& (!isset($_COOKIE['taccess']) || $_COOKIE['taccess'] !== 'f5djhGDD4$G%fddkl$343')*/
	) {
		header('HTTP/1.0 403 Forbidden');
		die('Access denied.');
	}
}

require_once __DIR__ . '/../src/Framework/Debug.php';
require_once __DIR__ . '/../vendor/autoload.php';
$app = new \App\Framework\App();
$app->run();
