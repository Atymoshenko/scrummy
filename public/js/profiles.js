$(function() {
	'use strict';
	account.checkUserTask = 'profile';
	account.listener = function() {

	};
	var $formProfiles = $('form[name="profiles"]');
	$formProfiles.on('submit', function(e) {searchProfiles($(this), e)});
	var offset = 0;
	var limit = 10;
	var isFinishedLoadMore = false;
	function searchProfiles($form, e) {
		e.preventDefault();
		offset = 0;
		isFinishedLoadMore = false;
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		data.push({'name': 'offset', 'value': offset});
		data.push({'name': 'limit', 'value': limit});
		disableForm($form);
		var $container = $('#profiles');
		$container.empty();
		$container.next('.loader').removeClass('invisible');
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: data
			, complete: function() {
				enableForm($form);
				$container.next('.loader').addClass('invisible');
			}
			, success: function(data) {
				if (data.needAuth === true) {
					redirect(routes['signin']['path'] + '?returnUrl=' + getRouteHref());
					return;
				}
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (data.success) {
					$.each(data.users, function() {
						var $p = $('<div>', {class: 'profile list-group-item'});
						var thumbString = '<img src="'+this.thumbUrl+'" alt="" class="photo-profile photo-profile-medium" />';
						var nameSurnameString = '<p>'+this.name+' '+this.surname+'</p>';
						$p.attr('data-id', this.id);
						$p.append('<h2>'+thumbString+'<a href="'+this.url+'" target="_self">'+this.nickname+'</a></h2>'+nameSurnameString);
						$container.append($p);
						offset++;
					})
				}
				enableLoadMore($('#profiles'), $form);
			}
			, error: function(data) {}
		});
	}
	function enableLoadMore($container, $form) {
		if (isNaN(enableLoadMore.isEnabled)) {
			enableLoadMore.isEnabled = false;
		}
		if (enableLoadMore.isEnabled) {
			return;
		}
		enableLoadMore.isEnabled = true;
		$(document).on('scroll', function() {
			checkLoadMore($container, $form)
		});
		$(window).resize(function() {
			checkLoadMore($container, $form)
		});
	}
	function checkLoadMore($container, $form) {
		if (isNaN(checkLoadMore.inProcess)) {
			checkLoadMore.inProcess = false;
		}
		if (checkLoadMore.inProcess || isFinishedLoadMore) {
			return;
		}
		checkLoadMore.inProcess = true;
		var bottom = $container.offset().top + $container.height();
		if (bottom - $(window).scrollTop() < $(window).height()) {
			var data = $form.serializeArray();
			data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
			data.push({'name': 'offset', 'value': offset});
			data.push({'name': 'limit', 'value': limit});
			$container.next('.loader').removeClass('invisible');
			$.ajax({
				url: $form.attr('action'),
				method: $form.attr('method'),
				data: data
				, complete: function() {
					checkLoadMore.inProcess = false;
					$container.next('.loader').addClass('invisible');
				}
				, success: function(data) {
					if (!data.apiToken) {
						localStorage.removeItem('apiToken');
						initUser();
						return;
					} else {
						localStorage.setItem('apiToken', data.apiToken);
					}
					parseNotifications(data.notifications, $form);
					if (data.success) {
						if (data.users.length < 1) {
							isFinishedLoadMore = true;
						}
						$.each(data.users, function() {
							var $p = $('<div>', {class: 'profile list-group-item'});
							var thumbString = '<img src="'+this.thumbUrl+'" alt="" class="photo-profile photo-profile-medium" />';
							var nameSurnameString = '<p>'+this.name+' '+this.surname+'</p>';
							$p.attr('data-id', this.id);
							$p.append('<h2>'+thumbString+'<a href="'+this.url+'" target="_self">'+this.nickname+'</a></h2>'+nameSurnameString);
							$('#profiles').append($p);
							offset++;
						})
					}
				}
				, error: function(data) {}
			});
		} else {
			checkLoadMore.inProcess = false;
		}
	}
});