$(function() {
	'use strict';
	$('.loader').hide();

	var $formMessage = $('form[name="message"]');
	var $fieldMessage = $formMessage.find('input[name="message"]');
	var $buttonPrivate = $formMessage.find('[name="sendPrivate"]');
	var $sectionChatGeneral = $('.section-chat-general');
	var $sectionChatPrivate = $('.section-chat-private');
	var $chatGeneral = $('.chat-general');
	var $chatPrivate = $('.chat-private');
	var $containerUsers = $('.container-users');
	var $userListMale = $containerUsers.find('.users-list-male');
	var $userListFemale = $containerUsers.find('.users-list-female');
	var $smileList = $('.smiles-list');
	var $roomList = $('.rooms-list');
	var $chatTabs = $('#chat-tabs');
	var intervalLoadMessages = 2;
	if (env === 'dev') {
		intervalLoadMessages = 10;
	}
	var intervalLoadUsers = 30;
	var intervalLoadRooms = 3600;
	var $intervalLoadMessages;
	var $intervalLoadUsers;
	var $intervalLoadRooms;
	var maxMessagesCount = 1000;

	enableEvents();
	chatStart();

	function checkApiToken() {
		if (isApiToken()) {
			return true;
		}

		$chatGeneral.empty();
		$chatPrivate.empty();
		loadMessages.messagesHashes = {};
		loadMessages.messagesPrivateHashes = {};

		chatStop();
		redirect(routes['signin']['path'], true);
	}
	function enableEvents() {
		$buttonPrivate.click(function(e) {formMessageSubmit($(this).closest('form'), e, true)});
		$formMessage.submit(function(e) {formMessageSubmit($(this), e);});
		$userListMale.on('click', '[data-user]', function(e) {choseUser($(this), e)});
		$userListFemale.on('click', '[data-user]', function(e) {choseUser($(this), e)});
		$chatGeneral.on('click', '[data-user]', function(e) {choseUser($(this), e)});
		$chatPrivate.on('click', '[data-user]', function(e) {choseUser($(this), e)});
		$roomList.on('click', '[data-room-id]', function() {changeRoom($(this))});

		$('button[name="loadMessages"]').on('click', function() {loadMessages();});
		$('button[name="loadUsers"]').on('click', function() {loadUsers();});
		$('button[name="loadRooms"]').on('click', function() {loadRooms();});
		$smileList.on('click', 'img', function() {choseSmile($(this));});
		$fieldMessage.on('change keyup', function() {normalizeFieldMessage();});
		$chatTabs.find('.nav-link').on('click', function(e) {changeChatTab($(this), e);});
		$('.scrollDown[data-toggle="tooltip"]').tooltip({
			boundary: 'window'
		});
		$('.scrollDown').on('click', function() {
			$(this).hide();
			var $elParent = $(this).parent();
			$elParent.animate({scrollTop: $elParent.children().first().height()}, 500);
		});
	}
	function chatStart() {
		if (!checkApiToken()) {
			$chatGeneral.empty();
			$chatPrivate.empty();
			return;
		}
		sortUsersBySex();
		normalizeFieldMessage();
		loadMessages(true);
		loadRooms(true);
		if (env === 'prod') {
			$intervalLoadMessages = setInterval(loadMessages, intervalLoadMessages * 1000);
			$intervalLoadUsers = setInterval(loadUsers, intervalLoadUsers * 1000);
			$intervalLoadRooms = setInterval(loadRooms, intervalLoadRooms * 1000);
		}
	}
	function chatStop() {
		clearInterval($intervalLoadMessages);
		clearInterval($intervalLoadUsers);
		clearInterval($intervalLoadRooms);
	}
	function loadMessages(reload) {
		reload = !!reload;
		if (isNaN(loadMessages.inProcess)) {
			loadMessages.inProcess = false;
		}
		if (loadMessages.inProcess) {
			return;
		}
		loadMessages.inProcess = true;
		if (loadMessages.messagesHashes === undefined) {
			loadMessages.messagesHashes = {};
		}
		if (loadMessages.messagesPrivateHashes === undefined) {
			loadMessages.messagesPrivateHashes = {};
		}
		if (reload) {
			$chatGeneral.empty();
			$chatPrivate.empty();
		}
		var data = {
			'apiToken': localStorage.getItem('apiToken'),
			'reload': reload ? 1 : 0,
			'chatMode': chatMode,
			'groupId': groupId
		};
		$.ajax({
			url: routes['apiChatMessages']['path'],
			method: 'POST',
			cache: false,
			data: data,
			complete: function() {
				loadMessages.inProcess = false;
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					checkApiToken();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				var createdString;
				var messageHashes = {};
				var isNeedScrollGeneral = reload || isNeedScroll($sectionChatGeneral);
				var isNeedScrollPrivate = reload || isNeedScroll($sectionChatPrivate);
				$.each(data.messages, function() {
					if (this.hash in loadMessages.messagesHashes) {
						delete loadMessages.messagesHashes[this.hash];
					} else {
						messageHashes[this.hash] = this.created;
						loadMessages.messagesHashes[this.hash] = this.created;
						var created = new Date(this.created * 1000);
						createdString = ("0"+(created.getDate())).slice(-2)+'/'+("0"+(created.getMonth()+1)).slice(-2)+'&nbsp;'+("0"+(created.getHours())).slice(-2)+':'+("0"+(created.getMinutes())).slice(-2)+':'+("0"+(created.getSeconds())).slice(-2);
						var nicknameToString = this.nicknameTo ? '&gt;<span>'+this.nicknameTo+'</span>:' : '';
						var message = this.message.replace(/&lt;(.+?)&gt;/gm, '<img src="images/smiles/$1.gif" alt="" />');
						$chatGeneral.append('<div><span>'+createdString+'</span>&nbsp;<span data-user data-user-nickname="'+this.nickname+'">'+this.nickname+'</span>'+nicknameToString+'&nbsp;<span>'+message+'</span></div>');
						removeOldMessages($chatGeneral);
					}
				});
				if (isNeedScrollGeneral) {
					$sectionChatGeneral.animate({scrollTop: $chatGeneral.height()}, 500);
					$sectionChatGeneral.find('.scrollDown').hide();
				} else {
					$sectionChatGeneral.find('.scrollDown').show();
				}
				loadMessages.messagesHashes = messageHashes;

				var messagePrivateHashes = {};
				$.each(data.messagesPrivate, function() {
					if (this.hash in loadMessages.messagesPrivateHashes) {
						delete loadMessages.messagesPrivateHashes[this.hash];
					} else {
						messagePrivateHashes[this.hash] = this.created;
						loadMessages.messagesPrivateHashes[this.hash] = this.created;
						var created = new Date(this.created * 1000);
						createdString = ("0"+(created.getDate())).slice(-2)+'/'+("0"+(created.getMonth()+1)).slice(-2)+'&nbsp;'+("0"+(created.getHours())).slice(-2)+':'+("0"+(created.getMinutes())).slice(-2)+':'+("0"+(created.getSeconds())).slice(-2);
						var nicknameToString = '&gt;<span>'+this.nicknameTo+'</span>:';
						var message = this.message.replace(/&lt;(.+?)&gt;/gm, '<img src="images/smiles/$1.gif" alt="" />');
						$chatPrivate.append('<div><span>'+createdString+'</span>&nbsp;<span data-user data-user-nickname="'+this.nickname+'">'+this.nickname+'</span>'+nicknameToString+'&nbsp;<span>'+message+'</span></div>');
						removeOldMessages($chatPrivate);
					}
				});
				if (isNeedScrollPrivate) {
					$sectionChatPrivate.animate({scrollTop: $chatPrivate.height()}, 500);
					$sectionChatPrivate.find('.scrollDown').hide();
				} else {
					$sectionChatPrivate.find('.scrollDown').show();
				}
				loadMessages.messagesPrivateHashes = messagePrivateHashes;
			}
			, error: function(data) {}
		});
		function isNeedScroll($container) {
			var scrollBottom = $container[0].scrollHeight - $container.scrollTop() - $container.height();
			return scrollBottom <= 5;
		}
		function removeOldMessages($container) {
			if ($container.children('div').length <= maxMessagesCount) return;
			$container.children('div:lt('+($container.children('div').length-maxMessagesCount)+')').remove();
		}
	}
	function reloadMessages() {
		loadMessages(true);
	}
	function loadUsers() {
		if (isNaN(loadUsers.inProcess)) {
			loadUsers.inProcess = false;
		}
		if (loadUsers.inProcess) {
			return;
		}
		loadUsers.inProcess = true;
		var data = {
			'apiToken': localStorage.getItem('apiToken'),
			'chatMode': chatMode,
			'groupId': groupId
		};
		$.ajax({
			url: routes['apiChatUsers']['path'],
			method: 'POST',
			cache: false,
			data: data,
			complete: function() {
				loadUsers.inProcess = false;
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					checkApiToken();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				$containerUsers.find('[data-online]').empty();
				var usersByRoomsCount = {};
				$.each(data.users, function(k,user) {
					var isOnline = user.lastVisit != null && (parseInt(new Date().getTime() / 1000)-parseInt(user.lastVisit)) <= onlineTime;
					var $container;
					if (user.sex === 'M') {
						$container = $userListMale;
					} else if (user.sex === 'F') {
						$container = $userListFemale;
					}
					$container = $container.find('[data-online="'+(isOnline?'1':'0')+'"]');
					if ($container.length) {
						var nicknameText = user.nickname;
						if (user.isFriend) {
							nicknameText = '<b>'+nicknameText+'</b>';
						}
						var urlProfileString = '<a href="'+user.urlProfile+'" target="'+user.nickname+'">info</a>';
						var online = '<span class="badge badge-pill badge-'+(isOnline?'success':'secondary')+'" data-toggle="tooltip" data-placement="left" title="'+(isOnline?'Online':'Offline')+'">'+urlProfileString+'</span>&nbsp;';
						$container.append('<div data-user data-user-nickname="'+user.nickname+'" data-is-online="'+(isOnline?'1':'0')+'">'+online+'<span data-nickname>'+nicknameText+'</span></div>');
					}
					if (user.roomId in usersByRoomsCount) {
						usersByRoomsCount[user.roomId]++;
					} else {
						usersByRoomsCount[user.roomId] = 1;
					}
				});
				$containerUsers.find('[data-toggle="tooltip"]').tooltip({
					boundary: 'window'
				});
				$roomList.find('[data-room-id]').find('span').text('');
				$.each(usersByRoomsCount, function(roomId,roomUsersCount) {
					if (roomUsersCount) {
						$roomList.find('[data-room-id="'+roomId+'"]').find('span').text('('+roomUsersCount+')');
					}
				});
			}
			, error: function(data) {}
		});
	}
	function loadRooms(isLoadUsers) {
		isLoadUsers = !!isLoadUsers;
		if (isNaN(loadRooms.inProcess)) {
			loadRooms.inProcess = false;
		}
		if (loadRooms.inProcess) {
			return;
		}
		loadRooms.inProcess = true;
		var data = {
			'apiToken': localStorage.getItem('apiToken'),
			'chatMode': chatMode,
			'groupId': groupId
		};
		$.ajax({
			url: routes['apiChatRooms']['path'],
			method: 'POST',
			cache: false,
			data: data,
			complete: function() {
				loadRooms.inProcess = false;
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					checkApiToken();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				$roomList.empty();
				$.each(data.rooms, function(k,room) {
					$roomList.append('<div'+(!!room.active ? ' class="active"' : '')+' data-room-id="'+room.id+'">'+room.name+'&nbsp;<span></span></div>');
				});
				if (isLoadUsers) {
					loadUsers();
				}
			}
			, error: function(data) {}
		});
	}
	function formMessageSubmit($form, e, isPrivate) {
		e.preventDefault();
		isPrivate = !!isPrivate;
		if (!localStorage.getItem('apiToken')) {
			return;
		}
		if (isNaN(formMessageSubmit.inProcess)) {
			formMessageSubmit.inProcess = false;
		}
		if (formMessageSubmit.inProcess) {
			return;
		}
		formMessageSubmit.inProcess = true;
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		if (isPrivate) {
			data.push({'name': 'sendPrivate', 'value': true});
		}
		data.push({'name': 'chatMode', 'value': chatMode});
		data.push({'name': 'groupId', 'value': groupId});
		normalizeFieldMessage();
		data = $.param(data);
		disableForm($form);
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: data,
			complete: function() {
				formMessageSubmit.inProcess = false;
				enableForm($form);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					checkApiToken();
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (!Object.keys(data.notifications.errors).length) {
					$fieldMessage.val('');
				}
			}
			, error: function(data) {}
		});
	}
	function choseSmile($smile) {
		var matches = $smile.attr('src').match(/^.+\/(.+?)\.[a-z]+$/i);
		if (matches[1] === undefined) {
			return;
		}
		$fieldMessage.val($fieldMessage.val()+' <'+matches[1]+'> ');
		normalizeFieldMessage();
	}
	function choseUser($el) {
		var nickname = $el.attr('data-user-nickname');
		$fieldMessage.val($.trim($fieldMessage.val()));
		var matches = $fieldMessage.val().match(/^[^\s,:]+\s*:\s*/g);
		if (matches == null) {
			$fieldMessage.val(nickname+': '+$fieldMessage.val());
		} else {
			$fieldMessage.val(nickname+': '+$fieldMessage.val().substr(matches[0].length));
		}
		normalizeFieldMessage();
	}
	function normalizeFieldMessage() {
		$fieldMessage.focus();
		$fieldMessage.val($fieldMessage.val().replace(/^\s+/, ''));
		$fieldMessage.val($fieldMessage.val().replace(/\s+/gm, ' '));
	}
	function changeRoom($el) {
		normalizeFieldMessage();
		$.ajax({
			url: routes['apiChatRoomSwitch']['path'],
			method: 'POST',
			data: {
				'apiToken': localStorage.getItem('apiToken'),
				'roomId': $el.attr('data-room-id'),
				'chatMode': chatMode,
				'groupId': groupId
			},
			complete: function() {}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					checkApiToken();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.roomId) {
					if (data.mode === 'group') {
						chatMode = 'group';
						groupId = data.groupId;
					} else {
						chatMode = 'chat';
						groupId = null;
					}
					$roomList.find('[data-room-id]').removeClass('active');
					$el.addClass('active');
					loadUsers();
					reloadMessages();
				}
			}
			, error: function(data) {}
		});
	}
	function sortUsersBySex() {
		var apiToken = localStorage.getItem('apiToken');
		var userSex = apiToken.substr(apiToken.length-1);
		$containerUsers.find('[data-sex]').sort(function (a, b) {
			var sexA = $(a).attr('data-sex');
			var sexB = $(b).attr('data-sex');
			return userSex === 'M' ? ((sexA > sexB) ? 1 : -1) : ((sexB > sexA) ? 1 : -1);
		}).appendTo($containerUsers);
	}
	function changeChatTab($tab, e) {
		if (isNaN(changeChatTab.tabName)) {
			changeChatTab.tabName = $tab.closest('.nav-tabs').find('.nav-link.active').data('name');
		}
		e.preventDefault();
		$tab.closest('.nav-tabs').find('.nav-link').removeClass('active');
		$tab.addClass('active');
		changeChatTab.tabName = $tab.data('name');
		console.log(changeChatTab.tabName);
		$('[data-chat-tabs-container]').removeClass('active-chat-tabs-container');
		$('[data-chat-tabs-container="'+changeChatTab.tabName+'"]').addClass('active-chat-tabs-container');
	}
	function renderUser() {

	}
});