$(function() {
	'use strict';
	var offset = 0;
	var limit = 10;
	var $container = $('[data-component="comments"]');
	var $formCommentCreate = $('form[name="commentCreate"]');
	var object = $container.data('object');
	var objectId = $container.data('id');
	var isFinishedLoadMore = false;
	account.listener = function() {
		init();
		loadComments();
	};

	function init() {
		enableAddComment();
		enableLoadMoreComments();
		$container.on('click', '[data-action="remove"]', function(e) {
			removeComment($(this), e);
		});
	}
	function enableAddComment() {
		$formCommentCreate.on('submit', function(e) {
			createComment($(this), e);
		});
	}
	function createComment($form, e) {
		e.preventDefault();
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		data.push({'name': 'object', 'value': object});
		data.push({'name': 'objectId', 'value': objectId});
		disableForm($form);
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: data
			, complete: function() {
				enableForm($form);
			}
			, success: function(data) {
				enableForm($form);
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (data.success) {
					$form.find('textarea').val('');
					var $item = getCommentItemAsHtml(data.comment);
					$container.find('ul').prepend($item);
				}
			}
			, error: function(data) {}
		});
	}
	function removeComment($button, e) {
		e.preventDefault();
		var data = {'apiToken': localStorage.getItem('apiToken'), 'object': object, 'objectId': objectId};
		$button.prop('disabled', true);
		$.ajax({
			url: $button.data('url'),
			method: 'POST',
			data: data
			, complete: function() {
				$button.prop('disabled', false);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.success) {
					$button.closest('li').remove();
				}
			}
			, error: function(data) {}
		});
	}
	function getCommentItemAsHtml(comment) {
		var $item = $('<li class="mt-1">', {class: ''});
		var thumbString = '<img src="'+comment.thumbUrl+'" alt="" class="photo-comment" />';
		var createdAt = new Date(comment.createdAt * 1000);
		var createdAtString = '<span class="date">'+("0"+(createdAt.getDate())).slice(-2)+'.'+("0"+(createdAt.getMonth()+1)).slice(-2)+'.'+((createdAt.getFullYear()))+'&nbsp;'+("0"+(createdAt.getHours())).slice(-2)+':'+("0"+(createdAt.getMinutes())).slice(-2)+':'+("0"+(createdAt.getSeconds())).slice(-2)+'</span>';
		var nicknameString = '<div><a href="'+comment.urlProfile+'" target="_self"><b>'+comment.nickname+'</b></a></div>';
		var btnEdit = /*comment.btnEdit*/'';
		var btnRemove = comment.btnRemove;
		$item.append(thumbString+nicknameString+'<div>'+createdAtString+'<span>'+btnEdit+'</span><span>'+btnRemove+'</span>'+'</div>');
		$item.append('<p>'+comment.content+'</p>');
		return $item;
	}
	function loadComments() {
		if (isNaN(loadComments.inProcess)) loadComments.inProcess = false;
		if (loadComments.inProcess) return;
		loadComments.inProcess = true;
		var data = {'apiToken': localStorage.getItem('apiToken'), 'object': object, 'objectId': objectId, 'offset': offset, 'limit': limit};
		$container.find('.loader').removeClass('invisible');
		$.ajax({
			url: $container.data('url'),
			method: 'POST',
			data: data
			, complete: function() {
				loadComments.inProcess = false;
				$container.find('.loader').addClass('invisible');
				checkLoadMoreComments();
			}
			, success: function(data) {
				if (data.needAuth === true) {
					redirect(routes['signin']['path'] + '?returnUrl=' + getRouteHref());
					return;
				}
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.success) {
					if (data.commentList.length < 1) {
						isFinishedLoadMore = true;
					}
					$.each(data.commentList, function() {
						var $item = getCommentItemAsHtml(this);
						$container.find('ul').append($item);
						offset++;
					});
				}
			}
			, error: function(data) {}
		});
	}
	function enableLoadMoreComments() {
		if (isNaN(enableLoadMoreComments.isEnabled)) enableLoadMoreComments.isEnabled = false;
		if (enableLoadMoreComments.isEnabled) return;
		enableLoadMoreComments.isEnabled = true;
		$(document).on('scroll', function() {
			checkLoadMoreComments()
		});
		$(window).resize(function() {
			checkLoadMoreComments();
		});
	}
	function checkLoadMoreComments() {
		if (isFinishedLoadMore) return;
		var bottom = $container.offset().top + $container.height();
		if (bottom - $(window).scrollTop() < $(window).height()) {
			loadComments();
		}
	}
});