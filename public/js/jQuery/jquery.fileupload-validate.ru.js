$(function () {
	'use strict';
	$('#fileupload').fileupload({
		messages: {
			maxNumberOfFiles: 'Превышено максимальное количество файлов.',
			acceptFileTypes: 'Тип файла не допускается.',
			maxFileSize: 'Файл слишком большой.',
			minFileSize: 'Файл слишком маленький.'
		}
	});
});