$(function () {
	'use strict';
	$('#fileupload').fileupload({
		messages: {
			maxNumberOfFiles: 'Maximum number of files exceeded.',
			acceptFileTypes: 'File type not allowed.',
			maxFileSize: 'File is too large.',
			minFileSize: 'File is too small.'
		}
	});
});