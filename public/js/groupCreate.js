$(function() {
	'use strict';
	account.checkUserTask = 'profile';
	account.listener = function() {
		groupInitCreate();
	};
	function groupInitCreate() {
		var $loader = $('.loader');
		if (account.profile.countryCode && account.profile.cityId) {
			$('[data-component="group-create-block"]').show();
			var $buttonGroupAccept = $('#button-group-accept');
			$buttonGroupAccept.on('click', function() {
				$('[data-component="group-create-accept"]').remove();
				$loader.removeClass('hide');
				$.ajax({
					url: routes['apiGroupCreateForm']['path'],
					method: 'post',
					data: {'apiToken': localStorage.getItem('apiToken')},
					complete: function() {
						$loader.addClass('hide');
					}
					, success: function(data) {
						if (!data.apiToken) {
							localStorage.removeItem('apiToken');
							initUser();
							return;
						} else {
							localStorage.setItem('apiToken', data.apiToken);
						}
						var $componentCreateForm = $('[data-component="group-create-form"]');
						$componentCreateForm.html(data.html);
						$componentCreateForm.find('form').on('submit', function(e){onSubmitGroupCreateForm($(this),e)})
					}
					, error: function(data) {}
				});
			});
			if (account.profile.groupCount > 0) {
				$buttonGroupAccept.trigger('click');
			}
		} else {
			$('[data-component="group-create-require"]').show();
		}
	}
	function onSubmitGroupCreateForm($form, e) {
		e.preventDefault();
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		disableForm($form);
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: data
			, complete: function() {
				enableForm($form);
			}
			, success: function(data) {
				enableForm($form);
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (data.success) {
					$('[data-component="group-create-form"]').empty();
					$('[data-component="group-create-success"]').show();
					setTimeout(function() {redirect(routes['group_edit']['path'].replace(/{groupId}/, data.group.id))}, 2000);
				}
			}
			, error: function(data) {}
		});
	}
});