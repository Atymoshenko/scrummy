$(function() {
	'use strict';
	account.checkUserTask = 'profile';
	account.listener = function() {
		loadMyGroups();
	};
	var $formGroups = $('form[name="groups"]');
	$formGroups.on('submit', function(e) {searchGroups($(this), e)});
	function loadMyGroups() {
		if (account.profile.groupCount > 0) {
			$.ajax({
				url: routes['apiGroupsMy']['path'],
				method: 'post',
				data: {'apiToken': localStorage.getItem('apiToken')}
				, success: function(data) {
					if (!data.apiToken) {
						localStorage.removeItem('apiToken');
						initUser();
						return;
					} else {
						localStorage.setItem('apiToken', data.apiToken);
					}
					if (data.myGroups.length > 0) {
						var $ul = $('<ul>', {class: 'list-group'});
						$.each(data.myGroups, function() {
							var item = '<li class="list-group-item"><div class="float-right"><a href="'+this.urlEdit+'" target="_self" class="btn btn-primary">'+messages['Edit']+'</a></div><div><a href="'+this.url+'" target="_self">'+this.name+'</a></div></li>';
							$ul.append(item);
						});
						$('[data-component="my-groups"]').append($ul).show();
					}
				}
				, error: function(data) {}
			});
		}
	}
	function searchGroups($form, e) {
		e.preventDefault();
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		disableForm($form);
		$('#groups').empty();
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: data
			, complete: function() {
				enableForm($form);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (data.success) {
					$.each(data.groups, function() {
						var $g = $('<div>', {class: 'group list-group-item'});
						$g.attr('data-id', this.id);
						$g.append('<h2><a href="'+this.url+'" target="_self">'+this.name+'</a></h2>');
						$g.append('<div>'+(this.private?messages['Private group']:messages['Public group'])+'</div>');
						$g.append('<p>'+this.description+'</p>');
						$('#groups').append($g);
					})
				}
			}
			, error: function(data) {}
		});
	}
});