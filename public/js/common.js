var onlineTime = 30;
$.urlParam = function(key){
	var results = new RegExp('[\?&]'+key+'=([^&#]*)').exec(window.location.href);
	if (results === null){
		return null;
	} else{
		return decodeURI(results[1]) || 0;
	}
};
$(function() {
	'use strict';
	$('.domloader').remove();
	initClientFromJquery();
	var $formSign = $('form[name="sign"]');
	var $formProfileEdit = $('form[name="profileEdit"]');
	var $handlerSignOut = $('[data-action="signout"]');
	var $formSignInGoogle = $('form[name="signinGoogle"]');
	var $tabs = $('[data-tabs]');
	var $fieldCountry = $('select[data-field="country"]');
	var $fieldCity = $('select[data-field="city"]');

	$formSign.submit(function(e){formSignSubmit($(this), e)});
	$formProfileEdit.submit(function(e){formProfileEditSubmit($(this), e)});
	$handlerSignOut.click(function(e){doSignOut($(this), e);});
	$formSignInGoogle.submit(function(e){googleFormSignInSubmit($(this), e)});
	$tabs.find('.nav-link').on('click', function() {tabSwitch($(this));});
	tabSwitch();
	//$tabs.find('.nav-link.active').trigger('click');
	$fieldCountry.on('change', function() {initFieldCities($(this), $fieldCity, account.profile.cityId);});
	$fieldCountry.trigger('change');

	initUser();
});
function tabSwitch($el) {
	var $tabs;
	if (!$el) {
		$tabs = $('[data-tabs="'+getRouteHash().replace(/-.+$/gm, '')+'"]');
		$el = $tabs.find('[data-tab-name="'+getRouteHash().replace(/^.+?-/gm, '')+'"]')
	}
	if (!$el || $el.length < 1) {
		$tabs = $('[data-tabs]');
		$el = $tabs.find('.nav-link.active');
	}
	if (!$el || $el.length < 1) {
		$tabs = $('[data-tabs]');
		$el = $tabs.find('.nav-item').first().find('.nav-link');
	}
	$tabs = $el.closest('[data-tabs]');
	var tabsName = $tabs.attr('data-tabs');
	$tabs.find('.nav-link').removeClass('active');
	$el.addClass('active');
	$('[data-tab-container]').hide();
	$('[data-tab-container="'+tabsName+'-'+$el.attr('data-tab-name')+'"]').show();
}

function isApiToken() {
	return !!localStorage.getItem('apiToken');
}
function getRouteHref() {
	return window.location.href;
}
function getRoutePath(data) {
	data = data ? data : window.location.href;
	var $a = $('<a>', {href: data});
	// console.log($a.prop('protocol'));
	// console.log($a.prop('hostname'));
	// console.log($a.prop('pathname'));
	// console.log($a.prop('search'));
	// console.log($a.prop('hash'));
	return ($a.prop('pathname').substr(0,1) === '/') ? $a.prop('pathname').substr(1) : $a.prop('pathname');
}
function getRouteName() {
	var $a = $('<a>', {href: window.location.href});
	var pathname = ($a.prop('pathname').substr(0,1) === '/') ? $a.prop('pathname').substr(1) : $a.prop('pathname');
	var routeName = null;
	$.each(routes, function(name, routeData) {
		if (routeData.path === pathname) {
			routeName = name;
			return true;
		}
	});
	return routeName;
}
function getRouteSearchParam(key) {
	return $.urlParam(key);
}
function getRouteHash(data) {
	data = data ? data : window.location.href;
	var $a = $('<a>', {href: data});
	return $a.prop('hash').substr(1);
}
function isRouteNeedAuth() {
	var result = false;
	$.each(routes, function(routeName,route) {
		if (route.auth === true) {
			var pattern = new RegExp("^"+route.path.replace(/{.+?}/g, '[^\/]+')+"$", "g");
			if (pattern.test(getRoutePath())) {
				result = true;
			}
		}
	});
	return result;
}
function redirect(url, useReturn) {
	useReturn = !!useReturn;
	window.location.href = url + (useReturn ? ('?returnUrl=' + getRouteHref()) : '');
	//window.history.pushState("SomeState", "Title", url);
}
function formSignSubmit($form, e) {
	e.preventDefault();
	var data = $form.serialize();
	disableForm($form);
	$.ajax({
		url: $form.attr('action'),
		method: $form.attr('method'),
		data: data,
		cache: false,
		complete: function() {
			enableForm($form);
		}
		, success: function(data) {
			parseNotifications(data.notifications, $form);
			if (getRouteName() === 'signin') {
				if (data.apiToken) {
					localStorage.setItem('apiToken', data.apiToken);
					redirect(getRouteSearchParam('returnUrl') ? getRouteSearchParam('returnUrl') : routes['']['path']);
				}
			}
			if (data.formSaved) {
				if (data.apiToken) {
					localStorage.setItem('apiToken', data.apiToken);
				}
				$componentSignUpSuccess = $('[data-component="form-sign-up-success"]');
				$('[data-component="form-sign-up"]').hide();
				$componentSignUpSuccess.show();
				$componentSignUpSuccess.find('[data-dynamic="info"]').html(data.emailConfirmExpireText);
				$('[data-user-anonyme]:not([data-disabled])').hide();
				$('[data-user-authorized]:not([data-disabled])').show();
			}
		}
		, error: function(data) {}
	});
}

var auth2;
var googleUser;
var initClient = function() {
	gapi.load('auth2', initSigninV2);
};
function initClientFromJquery() {
	gapi.load('auth2', initSigninV2);
}
var onSuccess = function(user) {
	googleOnSignIn(user);
	// document.getElementById('sign').style.display = 'none';
	// document.getElementById('name').innerText = "Signed in: " + user.getBasicProfile().getName();
	// document.getElementById('name').style.display = '';
	//console.log('Signed in as ' + user.getBasicProfile().getName());
};
var onFailure = function(error) {
	//console.log(error);
};
var initSigninV2 = function() {
	auth2 = gapi.auth2.init({
		client_id: $('meta[name="google-signin-client_id"]').attr('content'),
		scope: 'profile email openid'
	});
	auth2.attachClickHandler(document.getElementById('googleButtonSignIn'), {}, onSuccess, onFailure);
	// Listen for sign-in state changes.
	auth2.isSignedIn.listen(signinChanged);
	// Listen for changes to current user.
	auth2.currentUser.listen(userChanged);
	// Sign in the user if they are currently signed in.
	if (auth2.isSignedIn.get() === true) {
		auth2.signIn();
	}
	// Start with the current live values.
	refreshValues();
};
var signinChanged = function (val) {
	//console.log('Signin state changed to ', val);
	//console.log('signed-in-cell: ');
	//console.log(val);
	if (!document.getElementById('sign')) {
		return;
	}
	if (val) {
		document.getElementById('sign').style.display = 'none';
		document.getElementById('name').innerText = "Signed in: " + googleUser.getBasicProfile().getName();
		document.getElementById('name').style.display = '';
	} else {
		document.getElementById('sign').style.display = '';
		document.getElementById('name').style.display = 'none';

	}
};
var userChanged = function (user) {
	//console.log('User now: ', user);
	googleUser = user;
	updateGoogleUser();
	//console.log('curr-user-cell:');
	//console.log(JSON.stringify(user, undefined, 2));
};
var updateGoogleUser = function () {
	if (googleUser) {
		//console.log('userId: ' + googleUser.getId());
		//console.log('user-scopes: ' + googleUser.getGrantedScopes());
		//console.log('auth-response: ' + JSON.stringify(googleUser.getAuthResponse(), undefined, 2));
	} else {
		// document.getElementById('user-id').innerText = '--';
		// document.getElementById('user-scopes').innerText = '--';
		// document.getElementById('auth-response').innerText = '--';
	}
};
var refreshValues = function() {
	if (auth2){
		// console.log('Refreshing values...');

		googleUser = auth2.currentUser.get();

		// console.log('curr-user-cell:');
		// console.log(JSON.stringify(googleUser, undefined, 2));
		// console.log('signed-in-cell:');
		// console.log(auth2.isSignedIn.get());

		updateGoogleUser();
	}
};

function googleOnSignIn(googleUser) {
	var $form = $('[name="signinGoogle"]');
	$('[data-form="signin"]').hide();
	$('[data-form="signup"]').hide();
	$form.find('input[name="idToken"]').val(googleUser.getAuthResponse().id_token);
	var id_token = googleUser.getAuthResponse().id_token;
	$.ajax({
		url: $form.attr('action'),
		method: 'post',
		data: {'idToken': id_token},
		complete: function() {}
		, success: function(data) {
			if (data.locale && $.cookie('locale') !== data.locale) {
				$.cookie('locale', data.locale);
			}
			if (data.apiToken) {
				localStorage.setItem('apiToken', data.apiToken);
				redirect(getRouteSearchParam('returnUrl') ? getRouteSearchParam('returnUrl') : routes['']['path']);
				return;
			}
			parseNotifications(data.notifications, $form);
			if (data.requireNickname) {
				var $fieldNickname = $('[data-field-block="nickname"]');
				$fieldNickname.show();
			}
		}
		, error: function(data) {}
	});
}
function googleSignOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {

	});
}
function googleFormSignInSubmit($form, e) {
	e.preventDefault();
	var data = $form.serialize();
	disableForm($form);
	$.ajax({
		url: $form.attr('action'),
		method: $form.attr('method'),
		data: data,
		cache: false,
		complete: function() {
			enableForm($form);
		}
		, success: function(data) {
			parseNotifications(data.notifications, $form);
			if (data.apiToken) {
				localStorage.setItem('apiToken', data.apiToken);
				redirect('/');
			}
		}
		, error: function(data) {}
	});
}

function formProfileEditSubmit($form, e) {
	e.preventDefault();
	var data = $form.serializeArray();
	data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
	data = $.param(data);
	disableForm($form);
	$.ajax({
		url: $form.attr('data-action'),
		method: $form.attr('method'),
		data: data,
		complete: function() {
			enableForm($form);
		}
		, success: function(data) {
			var needReload = false;
			if (data.locale && $.cookie('locale') !== data.locale) {
				$.cookie('locale', data.locale);
				needReload = true;
			}
			if (!data.apiToken) {
				localStorage.removeItem('apiToken');
				initUser();
				return;
			} else {
				localStorage.setItem('apiToken', data.apiToken);
			}
			parseNotifications(data.notifications, $form);
			if (data.emailConfirmSuccess) {
				$('.alert-success').show();
				$('.fieldBlock-emailConfirmToken').remove();
				$("#btn-submit").attr('data-disabled', 1).prop('disabled', true);
			}
			if (data.formSaved && data.profile) {
				var profile = account.profile;
				profile.cityId = data.profile.cityId;
				account.profile = profile;
				if (needReload) {
					document.location.reload();
				}
			}
		}
		, error: function(data) {}
	});
}
function disableForm($form) {
	$form.find('input,button,select,textarea').prop('disabled', true);
}
function enableForm($form) {
	$form.find('input:not([data-disabled]),button:not([data-disabled]),select:not([data-disabled]),textarea:not([data-disabled])').prop('disabled', false);
}
function parseNotifications(notifications, $form) {
	if (!$form) {
		$form = $('<form>');
	}
	var $notificationsCommon = $('.notifications');
	if (!$notificationsCommon.length) {
		$notificationsCommon = $('<div>', {class: 'notifications container'});
		$('body').prepend($notificationsCommon);
	}
	$notificationsCommon.empty();
	if ($form.length) {
		$form.find('.invalid-feedback,.invalid-tooltip').empty();
		$form.find('.is-invalid').removeClass('is-invalid');
	}
	if (typeof notifications !== 'object') return;
	$.each(notifications, function(type,subNotifications) {
		if (typeof subNotifications !== 'object') {
			addNotification(type, subNotifications);
			return true;
		}
		$.each(subNotifications, function(field,subSubNotifications) {
			var $field = $form.find('[name="'+field+'"]');
			if (typeof subSubNotifications !== 'object') {
				if ($form.length) {
					if (type === 'success' && subSubNotifications === true) {
						$field.addClass('is-valid');
						return true;
					}
				}
				addNotification(type, subSubNotifications);
				return true;
			}
			$.each(subSubNotifications, function() {
				if (type === 'errors') {
					$field.addClass('is-invalid');
					var $container;
					if ($field.next('.invalid-feedback').length) {
						$container = $field.next('.invalid-feedback');
					} else if ($field.next('label').length && $field.next('label').next('.invalid-feedback').length) {
						$container = $field.next('label').next('.invalid-feedback');
					} else if ($field.next('.invalid-tooltip').length) {
						$container = $field.next('.invalid-tooltip');
					} else if ($field.closest('.group-error').length) {
						$container = $field.closest('.group-error');
					} else {
						$container = $field.closest('.form-group').find('.invalid-feedback');
					}
					if ($container.length) {
						$container.append("<div>"+this+"</div>");
						return true;
					}
				}
				addNotification(type, this);
			})
		})
	});
	function addNotification(type, notification) {
		if (type === 'errors') {
			type = 'danger';
		}
		var $block = $notificationsCommon.find('.alert-'+type);
		if (!$block.length) {
			$block = $('<div>', {class: 'alert alert-'+type+' alert-dismissible fade show'});
		}
		$notificationsCommon.append($block);
		$block.append('<div>'+notification+'</div>');
		$block.append('<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>');
	}
}
function doSignOut($button, e) {
	e.preventDefault();
	googleSignOut();
	$.ajax({
		url: $button.data('api-method'),
		method: 'post',
		data: {apiToken: localStorage.getItem('apiToken')}
	});
	localStorage.removeItem('apiToken');
	window.location.href = routes['']['path'];
}
function initUser()  {
	if (!isApiToken() && isRouteNeedAuth()) {
		redirect(routes['signin']['path'] + '?returnUrl=' + getRouteHref());
		return;
	}

	if (firewallSignUpConfirmEmail()) {
	} else if (firewallPasswordRecovery()) {
	} else if (firewallPasswordRecoveryConfirm()) {
	} else {
		firewallCheckUser();
	}
	postInitUser();
}
function postInitUser() {
	if (isApiToken()) {
		$('[data-user-anonyme]:not([data-disabled])').hide();
		$('[data-user-authorized]:not([data-disabled])').show();
	} else {
		$('[data-user-anonyme]:not([data-disabled])').show();
		$('[data-user-authorized]:not([data-disabled])').hide();
	}
}
function firewallSignUpConfirmEmail() {
	if (getRoutePath() === 'signup' && getRouteSearchParam('id') && getRouteSearchParam('code')) {
		$('[data-component="form-sign-up"]').attr('data-disabled', '1');
		$('[data-component="form-sign-up-success"]').attr('data-disabled', '1');
		var $componentFormSignUpEmailConfirm = $('[data-component="form-sign-up-email-confirm"]');
		$componentFormSignUpEmailConfirm.show();
		$.ajax({
			url: routes['apiSignUpConfirm']['path'],
			method: 'post',
			data: {'id': getRouteSearchParam('id'), 'code': getRouteSearchParam('code')},
			complete: function() {
				$componentFormSignUpEmailConfirm.find('.loader').hide();
			}
			, success: function(data) {
				if (data.apiToken) {
					localStorage.setItem('apiToken', data.apiToken);
					$componentFormSignUpEmailConfirm.find('.alert-success').show();
					$componentFormSignUpEmailConfirm.find('p').show();
				} else {
					localStorage.removeItem('apiToken');
					$componentFormSignUpEmailConfirm.find('.alert-danger').show();
				}
				postInitUser();
			}
		});
		return true;
	}
	return false;
}
function firewallPasswordRecovery() {
	if (getRoutePath() === 'password_forgot') {
		var $form = $('form[name="password-forgot"]');
		$form.on('submit', function(e) {
			e.preventDefault();
			var data = $form.serialize();
			disableForm($form);
			$.ajax({
				url: $form.attr('action'),
				method: $form.attr('method'),
				data: data,
				complete: function() {
					enableForm($form);
				}
				, success: function(data) {
					parseNotifications(data.notifications, $form);
					if (data.success && data.success === true) {
						$('[data-component="form-password-forgot"]').hide();
						$('[data-component="form-password-forgot-sent"]').show();
					}
				}
				, error: function(data) {}
			});
		});
		return true;
	}
	return false;
}
function firewallPasswordRecoveryConfirm() {
	if (getRoutePath() === 'password_forgot_confirm' && getRouteSearchParam('id') && getRouteSearchParam('code')) {
		var $loader = $('.loader');
		var $form = $('form[name="password-reset"]');
		$form.on('submit', function(e) {
			e.preventDefault();
			var data = $form.serializeArray();
			data.push({'name': 'id', 'value': getRouteSearchParam('id')});
			data.push({'name': 'code', 'value': getRouteSearchParam('code')});
			data = $.param(data);
			disableForm($form);
			$.ajax({
				url: $form.attr('action'),
				method: $form.attr('method'),
				data: data,
				complete: function() {
					enableForm($form);
				}
				, success: function(data) {
					parseNotifications(data.notifications, $form);
					if (data.success && data.success === true) {
						$('[data-component="form-password-reset"]').hide();
						$('[data-component="form-password-reset-success"]').show();
					}
				}
				, error: function(data) {}
			});
		});
		return true;
	}
	return false;
}
function firewallCheckUser() {
	if (localStorage.getItem('apiToken')) {
		var data = {
			'apiToken': localStorage.getItem('apiToken')
			, 'tasks': JSON.stringify(account.checkUserTask)
		};
		$.each(account.checkUserParams, function(){
			data[this.key] = this.value;
		});
		$.ajax({
			url: routes['apiCheckUser']['path'],
			method: 'post',
			data: data
			, success: function(data) {
				if (data.locale && $.cookie('locale') !== data.locale) {
					$.cookie('locale', data.locale);
				}
				if (data.redirect) {
					if (data.redirect !== getRouteHref()) {
						redirect(data.redirect);
						return;
					}
				}
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					redirect('/signin');
				} else {
					localStorage.setItem('apiToken', data.apiToken);
					var accountData = [];
					$.each(account.checkUserTask, function() {
						accountData[this] = data[this];
					});
					account.group = data.group;
					account.user = data.user;
					account.profile = data.profile;
					account.data = accountData;
				}
				postInitUser();
			}
		});
	}
}
function initFieldCities($fieldCountry, $fieldCity, cityId) {
	if ($fieldCity.length < 1) return;
	$fieldCity.empty();
	if ($fieldCountry.val()) {
		$fieldCity.prop('disabled', true);
		$fieldCity.append('<option value="" selected>'+$fieldCity.data('loading')+'</option>');
		$.ajax({
			url: routes['apiCities']['path'],
			method: 'post',
			data: {'countryCode': $fieldCountry.val()},
			complete: function() {
				if (!$fieldCity.attr('data-disabled')) {
					$fieldCity.prop('disabled', false);
				}
			}
			, success: function(data) {
				$fieldCity.empty();
				$fieldCity.append('<option value=""></option>');
				$.each(data.cities, function() {
					var selected = '';
					if (this.id === cityId) {
						selected = ' selected';
					}
					$fieldCity.append('<option value="'+this.id+'"'+selected+'>'+this.name+'</option>');
				});
			}
		});
	}
}
function chatGetUserString(nickname,lastVisit,urlProfile) {
	var isOnline = lastVisit != null && (parseInt(new Date().getTime() / 1000)-parseInt(lastVisit)) <= onlineTime;
	var result = '<a href="'+urlProfile+'" target="'+nickname+'">info</a>';
	result = '<span class="badge badge-pill badge-'+(isOnline?'success':'secondary')+'" data-toggle="tooltip" data-placement="left" title="'+(isOnline?'Online':'Offline')+'">'+result+'</span>&nbsp;';
	result = '<div data-user data-user-nickname="'+nickname+'" data-is-online="'+(isOnline?'1':'0')+'">'+result+'<span data-nickname>'+nickname+'</span></div>';
	return result;
}