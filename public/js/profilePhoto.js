$(function () {
	'use strict';
	var filesCount = 0;
	var $form = $('#fileupload');
	var maxNumberOfFiles = $form.data('photo-count-max');
	$form.fileupload({
		url: $form.attr('action')
		, formData: {apiToken: localStorage.getItem('apiToken')}
		, autoUpload: true
		, minFileSize: 5000
		, maxFileSize: 9000000
		, previewMaxWidth: 300
		, previewMaxHeight: 300
		, sequentialUploads: true
		, limitMultiFileUploads: maxNumberOfFiles
		, maxNumberOfFiles: maxNumberOfFiles
		, acceptFileTypes: /(\.|\/)(jpe?g|png)$/i
	})
		.bind('fileuploadsubmit',          function (e, data) {
			$('.fileinput-button').addClass('disabled').find('input').prop('disabled', true);
		})
		.bind('fileuploaddone',            function (e, data) {
			if (data.result.files[0].error === undefined) {
				filesCount++;
			}
		})
		.bind('fileuploadstop',            function (e)       {
			if (filesCount < maxNumberOfFiles) {
				$('.fileinput-button').removeClass('disabled').find('input').prop('disabled', false);
			} else {
				$('.fileinput-button').addClass('disabled').find('input').prop('disabled', true);
			}
		});

	$form.addClass('fileupload-processing');
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: routes['apiProfilePhotos']['path'],
		method: 'post',
		data: {apiToken: localStorage.getItem('apiToken')},
		dataType: 'json',
		context: $form[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		if (result.files.length) {
			filesCount+= result.files.length;
		}
		$form.fileupload('option', 'limitMultiFileUploads', maxNumberOfFiles - filesCount);
		if (filesCount < maxNumberOfFiles) {
			$('.fileinput-button').removeClass('disabled').find('input').prop('disabled', false);
		} else {
			$('.fileinput-button').addClass('disabled').find('input').prop('disabled', true);
		}
		$(this).fileupload('option', 'done')
			.call(this, $.Event('done'), {result: result});
	});
	$('body').on('click', 'button.deletePhoto', function(e) {
		e.preventDefault();
		var $button = $(this);
		var url = $button.attr('data-url');
		$.ajax({
			url: url,
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken')},
		}).always(function () {

		}).done(function (response) {
			if (response.success === true) {
				filesCount--;
				$form.fileupload('option', 'limitMultiFileUploads', maxNumberOfFiles - filesCount);
				$button.closest('tr').remove();
				$('.fileinput-button').removeClass('disabled').find('input').prop('disabled', false);
			}
		});
	});
});