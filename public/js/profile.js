$(function() {
	'use strict';
	var userId = $('#userProfileContainer').data('user-id');
	account.checkUserTask = 'profileView';
	account.checkUserParams = {'key': 'userId', 'value': userId};
	account.listener = function() {
		loadUserData();
	};

	var $views = $('[data-views]');
	var $lastVisit = $('[data-last-visit]');

	var $buttonFriend = $('button[data-friend]');
	var $buttonFriendDecline = $('button[data-friend-decline]');
	var $buttonBlock = $('button[data-block]');
	var isFriendNeedApprove;
	var iBlocked = false;

	function loadUserData() {
		$lastVisit.text(account.data.profileView.user.lastVisit).closest('.disabled').removeClass('disabled');
		$views.text(account.data.profileView.profile.viewsCount).closest('.disabled').removeClass('disabled');
		var isFriend = account.data.profileView.isFriend;
		var isFriendRequestSent = account.data.profileView.isFriendRequestSent;
		isFriendNeedApprove = account.data.profileView.isFriendNeedApprove;
		iBlocked = account.data.profileView.iBlocked;
		if (iBlocked) {
			return;
		}
		if (isFriend !== null) {
			$buttonFriend.on('click', function(e) {propFriend($(this), e)});
			$buttonFriendDecline.on('click', function(e) {friendDecline($(this), e)});
			if (isFriend) {
				setFriend();
			} else {
				if (isFriendRequestSent) {
					setFriendSent();
				} else if (isFriendNeedApprove) {
					setFriendNeedApprove();
				} else {
					setNotFriend();
				}
			}
		}

		var isBlocked = account.data.profileView.isBlocked;
		if (isBlocked !== null) {
			$buttonBlock.on('click', function(e) {propBlock($(this), e)});
			if (isBlocked) {
				setBlocked();
			} else {
				setNotBlocked();
			}
		}
	}
	function propFriend($button, e) {
		e.preventDefault();
		var status = $button.data('status');
		if ($.inArray(status, ['friend','notFriend','sent','needApprove']) < 0) {
			return;
		}
		var prevText = $button.text();
		$button.text($button.data('text-processing'));
		$button.attr('disabled', true);
		$.ajax({
			url: ($.inArray(status, ['friend','sent']) > -1 ? $button.data('url-remove') : $button.data('url-add')),
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken'), userId: userId}
			, complete: function() {}
			, success: function(data) {
				$button.text(prevText);
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.status === 'friend') {
					setFriend();
				} else if (data.status === 'notFriend') {
					setNotFriend();
				} else if (data.status === 'sent') {
					setFriendSent();
				}
			}
			, error: function(data) {}
		});
	}
	function friendDecline($button, e) {
		e.preventDefault();
		var prevText = $button.text();
		$button.text($button.data('text-processing'));
		$button.attr('disabled', true);
		$.ajax({
			url: $button.data('url'),
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken'), userId: userId}
			, complete: function() {}
			, success: function(data) {
				$button.text(prevText);
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.status === 'notFriend') {
					setNotFriend();
				}
			}
			, error: function(data) {}
		});
	}
	function propBlock($button, e) {
		e.preventDefault();
		var status = $button.data('status');
		if ($.inArray(status, ['blocked','notBlocked']) < 0) {
			return;
		}
		var prevText = $button.text();
		$button.text($button.data('text-processing'));
		$button.attr('disabled', true);
		$.ajax({
			url: (status === 'blocked' ? $button.data('url-remove') : $button.data('url-add')),
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken'), userId: userId},
			complete: function() {}
			, success: function(data) {
				$button.text(prevText);
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.status === 'blocked') {
					setBlocked();
				} else if (data.status === 'notBlocked') {
					setNotBlocked();
				}
			}
			, error: function(data) {}
		});
	}
	function setFriend() {
		$buttonFriend.text($buttonFriend.data('text-friend-remove'));
		$buttonFriend.data('status', 'friend');
		$buttonFriend.prop('disabled', false);
		$buttonFriend.removeClass('invisible');
		$buttonFriendDecline.hide();
	}
	function setNotFriend() {
		$buttonFriend.text($buttonFriend.data('text-friend-add'));
		$buttonFriend.data('status', 'notFriend');
		$buttonFriend.prop('disabled', false);
		$buttonFriend.removeClass('invisible');
		$buttonFriendDecline.hide();
	}
	function setFriendSent() {
		$buttonFriend.text($buttonFriend.data('text-friend-sent'));
		$buttonFriend.data('status', 'sent');
		$buttonFriend.prop('disabled', false);
		$buttonFriend.removeClass('invisible');
		$buttonFriendDecline.hide();
	}
	function setFriendNeedApprove() {
		$buttonFriend.text($buttonFriend.data('text-friend-approve'));
		$buttonFriend.data('status', 'needApprove');
		$buttonFriend.prop('disabled', false);
		$buttonFriend.removeClass('invisible');
		$buttonFriendDecline.text($buttonFriendDecline.data('text-decline'));
		$buttonFriendDecline.show();
	}
	function setBlocked() {
		$buttonBlock.text($buttonBlock.data('text-block-remove'));
		$buttonBlock.data('status', 'blocked');
		$buttonBlock.prop('disabled', false);
		$buttonBlock.removeClass('invisible');
		$buttonFriend.prop('disabled', true);
		$buttonFriendDecline.hide();
	}
	function setNotBlocked() {
		$buttonBlock.text($buttonBlock.data('text-block-add'));
		$buttonBlock.data('status', 'notBlocked');
		$buttonBlock.prop('disabled', false);
		$buttonBlock.removeClass('invisible');
		$buttonFriend.prop('disabled', false);
		if (isFriendNeedApprove === true) {
			$buttonFriendDecline.show();
		} else {
			$buttonFriendDecline.hide();
		}
	}
});