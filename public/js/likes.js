$(function() {
	'use strict';
	var $container = $('[data-component="likes"]');
	var $buttonLikeCreate = $container.find('[data-like-action="create"]');
	var $buttonLikeRemove = $container.find('[data-like-action="remove"]');
	var object = $container.data('object');
	var objectId = $container.data('id');
	account.listener = function() {
		init();
		loadLikeCount();
	};

	function init() {
		$buttonLikeCreate.on('click', function(e) {
			createLike($(this), e);
		});
		$buttonLikeRemove.on('click', function(e) {
			removeLike($(this), e);
		});
		loadLikeCount();
	}
	function createLike($button, e) {
		e.preventDefault();
		var data = {'apiToken': localStorage.getItem('apiToken'), 'object': object, 'objectId': objectId};
		$button.prop('disabled', true);
		$.ajax({
			url: $button.data('url'),
			method: 'POST',
			data: data
			, complete: function() {
				$button.prop('disabled', false);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.success) {
					$button.hide();
					$buttonLikeRemove.show();
					$container.find('[data-like-count]').text(data.likeCount);
				}
			}
			, error: function(data) {}
		});
	}
	function removeLike($button, e) {
		e.preventDefault();
		var data = {'apiToken': localStorage.getItem('apiToken'), 'object': object, 'objectId': objectId};
		$button.prop('disabled', true);
		$.ajax({
			url: $button.data('url'),
			method: 'POST',
			data: data
			, complete: function() {
				$button.prop('disabled', false);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.success) {
					$button.hide();
					$buttonLikeCreate.show();
					$container.find('[data-like-count]').text(data.likeCount);
				}
			}
			, error: function(data) {}
		});
	}
	function loadLikeCount() {
		if (isNaN(loadLikeCount.inProcess)) loadLikeCount.inProcess = false;
		if (loadLikeCount.inProcess) return;
		loadLikeCount.inProcess = true;
		var data = {'apiToken': localStorage.getItem('apiToken'), 'object': object, 'objectId': objectId};
		$.ajax({
			url: $container.data('url'),
			method: 'POST',
			data: data
			, complete: function() {
				loadLikeCount.inProcess = false;
			}
			, success: function(data) {
				if (data.needAuth === true) {
					redirect(routes['signin']['path'] + '?returnUrl=' + getRouteHref());
					return;
				}
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.success) {
					$container.find('[data-like-count]').text(data.likeCount);
					$buttonLikeCreate.prop('disabled', false);
					if (data.iLiked) {
						$buttonLikeCreate.hide();
						$buttonLikeRemove.show();
					} else {
						$buttonLikeCreate.show();
						$buttonLikeRemove.hide();
					}
					$container.removeClass('disabled');
				}
			}
			, error: function(data) {}
		});
	}
});