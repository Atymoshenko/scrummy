$(function() {
	'use strict';
	var groupId = $('[data-group-id]').data('group-id');
	account.checkUserTask = 'profile';
	account.checkUserTask = 'group_details';
	account.checkUserParams = {'key': 'groupId', 'value': groupId};
	account.listener = function() {
		initDetails();
	};

	var group = {};
	var isOwner = false;
	var isMember = false;
	var isRequestSent = false;

	var $menuItemJoin = $('[data-item-group-join]');
	var $menuItemLeave = $('[data-item-group-leave]');
	var $menuItemChat = $('[data-item-group-chat]');

	function initDetails() {
		$('.loader').remove();
		group = account.data.group_details.group;
		if (!group) return;

		$menuItemJoin.find('.nav-link').on('click', function(e) {groupJoin($(this), e)});
		$menuItemLeave.find('.nav-link').on('click', function(e) {groupLeave($(this), e)});

		isOwner = account.data.group_details.isOwner;
		isMember = isOwner || (!!account.data.group_details.groupUser && (account.data.group_details.groupUser.approved || !group.private));
		isRequestSent = !!account.data.group_details.groupUser;
		if (isMember) {
			setMember();
			if (isOwner) {
				setOwner();
				initUserRequests(account.data.group_details.userRequests);
			}
		} else {
			if (isRequestSent) {
				setWaiting();
			} else {
				setNoMember();
			}
		}
	}
	function groupJoin($item, e) {
		e.preventDefault();
		$item.closest().addClass('disabled');
		$.ajax({
			url: $item.data('action'),
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken'), groupId: groupId},
			complete: function() {
				$item.closest().removeClass('disabled');
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.success === true) {
					if (group.private === true) {
						setWaiting();
					} else {
						setMember();
					}
				}
			}
			, error: function(data) {}
		});
	}
	function groupLeave($item, e) {
		e.preventDefault();
		$item.closest().addClass('disabled');
		$.ajax({
			url: $item.data('action'),
			method: 'post',
			data: {apiToken: localStorage.getItem('apiToken'), groupId: groupId},
			complete: function() {
				$item.closest().removeClass('disabled');
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications);
				if (data.success === true) {
					setNoMember();
				}
			}
			, error: function(data) {}
		});
	}
	function setWaiting() {
		isRequestSent = true;
		isMember = false;
		isOwner = false;
		$menuItemJoin.find('.nav-link').text($menuItemJoin.find('.nav-link').data('text-sent'));
		$menuItemJoin.find('.nav-link').addClass('disabled');
		$menuItemJoin.show();
		$menuItemLeave.hide();
		$menuItemChat.find('.nav-link').addClass('disabled');
	}
	function setMember() {
		isMember = true;
		$menuItemJoin.hide();
		$menuItemLeave.show();
		$menuItemChat.find('.nav-link').removeClass('disabled');
	}
	function setOwner() {
		isOwner = true;
		setMember();
		$menuItemLeave.find('.nav-link').addClass('disabled');
	}
	function setNoMember() {
		isRequestSent = false;
		isMember = false;
		isOwner = false;
		$menuItemJoin.show();
		$menuItemJoin.find('.nav-link').text($menuItemJoin.find('.nav-link').data('text-join'));
		$menuItemJoin.find('.nav-link').removeClass('disabled');
		$menuItemLeave.hide();
		$menuItemChat.find('.nav-link').addClass('disabled');
	}
	function initUserRequests(userRequests) {
		if (typeof userRequests !== 'object' || userRequests.length < 1) return;
		var $component = $('[data-component="group-requests"]');
		$component.show();
		var $container = $component.find('.list-group');
		$.each(userRequests, function() {
			var buttonAccept = '<button class="btn btn-primary" data-action="accept" data-user-id="'+this.id+'">'+messages['Accept']+'</button>';
			var buttonDecline = '<button class="btn btn-danger" data-acrion="decline" data-user-id="'+this.id+'">'+messages['Decline']+'</button>';
			var $item = $('<div>', {class: 'list-group-item list-group-item-action'});
			$item.append('<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><a href="'+this.urlProfile+'" target="_blank">'+this.nickname+'</a></h5>'+'<small class="hide">3 days ago</small></div>');
			$item.append('<p class="mb-1 hide">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p><small class="hide">Donec id elit non mi porta.</small>');
			$item.append('<div>'+buttonAccept+' '+buttonDecline+'</div>');
			$container.append($item);
		});
		$component.on('click', 'button[data-user-id]', function(e) {
			e.preventDefault();
			var $button = $(this);
			$button.prop('disabled', true);
			var url = $button.attr('data-action') === 'accept' ? routes['apiGroupRequestAccept']['path'] : routes['apiGroupRequestDecline']['path'];
			$.ajax({
				url: url,
				method: 'post',
				data: {apiToken: localStorage.getItem('apiToken'), groupId: groupId, userId: $button.attr('data-user-id')},
				complete: function() {
					$button.prop('disabled', false);
				}
				, success: function(data) {
					if (!data.apiToken) {
						localStorage.removeItem('apiToken');
						initUser();
						return;
					} else {
						localStorage.setItem('apiToken', data.apiToken);
					}
					parseNotifications(data.notifications);
					if (data.success === true) {
						$button.closest('div.list-group-item').remove();
					}
				}
				, error: function(data) {}
			});
		});
	}
});