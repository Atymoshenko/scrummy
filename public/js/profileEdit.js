$(function() {
	'use strict';
	account.checkUserTask = 'user';
	account.checkUserTask = 'profile';
	account.listener = function() {
		loadUserProfile();
	};
	var $fieldCountry = $('select[data-field="country"]');
	var $fieldCity = $('select[data-field="city"]');
	var $buttonViewProfile = $("#btn-view-profile");

	function loadUserProfile() {
		$('h1').text(account.user.nickname);
		var $fieldEmail = $('#user-email');
		$fieldEmail.val(account.user.email);
		$('input[name="id"]').val(account.profile.userId);
		if (!account.user.emailConfirmed) {
			var $fieldBlockEmailConfirmToken = $('.fieldBlock-emailConfirmToken');
			$fieldBlockEmailConfirmToken.find('.form-text').html(account.user.emailConfirmExpireText);
			$fieldBlockEmailConfirmToken.show();
			$("#btn-submit").prop('disabled', false);
		}
		$fieldCountry = $('#profile-country');
		$fieldCity = $('#profile-city');
		$('#profile-locale').val(account.profile.locale);
		$('#profile-name').val(account.profile.name);
		$('#profile-surname').val(account.profile.surname);
		$fieldCountry.val(account.profile.countryCode).trigger('change');
		if (account.profile.groupCount > 0) {
			$fieldCountry.attr('data-disabled', 1).prop('disabled', true);
			$fieldCity.attr('data-disabled', 1).prop('disabled', true);
		}
		$buttonViewProfile.attr('href', $buttonViewProfile.attr('href').replace('userId', account.profile.userId));
		$buttonViewProfile.removeClass('invisible');
	}
});