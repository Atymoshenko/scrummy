$(function() {
	'use strict';
	var $container = $('[data-component="photos"]');
	var $carousel = $('#userProfilePhotosCarousel');
	var $modal = $('#userProfilePhotosModal');
	account.listener = function() {
		init();
		loadPhotos();
	};
	function init() {
		$container.removeClass('disabled');
		$container.on('click', 'li', function() {
			$carousel.find('.carousel-item').removeClass('active');
			$carousel.find('.carousel-item[data-id="'+$(this).data('id')+'"]').addClass('active');
			window.location.hash = 'photoId='+$(this).data('id');
			$modal.modal('show');
		});
		$(window).resize(function() {
			initHeight();
		});
	}
	function initHeight() {
		/* 207 */
		var maxHeight = $(window).height() - 250;
		if (maxHeight < 213) {
			maxHeight = 213;
		}
		$carousel.find('.carousel-inner').find('.carousel-item').find('img').css('max-height', maxHeight);
	}
	function loadPhotos() {
		var data = {'apiToken': localStorage.getItem('apiToken')};
		$.ajax({
			url: $container.data('url'),
			method: 'POST',
			data: data
			, complete: function() {}
			, success: function(data) {
				if (data.needAuth === true) {
					redirect(routes['signin']['path'] + '?returnUrl=' + getRouteHref());
					return;
				}
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				if (data.success) {
					if (data.photoList.length > 0) {
						$container.find('.slider').find('.hide').remove();
						$.each(data.photoList, function() {
							var $item = getPhotoItemAsHtml(this);
							$container.find('.slider').find('ul').append($item);
							var $itemCarousel = getPhotoItemAsHtmlForCarousel(this);
							$carousel.find('.carousel-inner').append($itemCarousel);
						});
						initHeight();
					} else {
						$container.find('.slider').find('.hide').show();
					}
				}
			}
			, error: function() {}
		});
	}
	function getPhotoItemAsHtml(photo) {
		var $item = $('<li data-id="'+photo.id+'" data-url="'+photo.url+'" data-name="'+photo.name+'">');
		var thumbString = '<img src="'+photo.thumbnailUrl+'" alt="" />';
		$item.append(thumbString);
		return $item;
	}
	function getPhotoItemAsHtmlForCarousel(photo) {
		var $item = $('<div data-id="'+photo.id+'" class="carousel-item" style="width: auto!important">');
		var imgString = '<img src="'+photo.url+'" class="d-block w-100_" style="max-width:1200px" alt="'+photo.name+'">';
		$item.append(imgString);
		return $item;
	}
});