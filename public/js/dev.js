$(function() {
	$('body').prepend('<div id="debug" style="position:fixed;bottom:0;z-index:5000;padding:1px;border:1px solid #000;background-color:#000;color:#fff;opacity:.7;font:normal normal normal 10px/12px Verdana;"><span></span></div>');
	$(window).resize(function() {onResize();});
	function onResize() {
		$('#debug').find('span').text($(window).width()+'x'+$(window).height());
	}
	onResize();

	$('[name="generateMessages"]').click(function() {generateMessages()});

	function generateMessages() {
		var message = 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.';
		var $chatGeneral = $('.chat-general');
		if ($chatGeneral.find('ul').length) {
			$chatGeneral = $chatGeneral.find('ul');
		} else {
			$chatGeneral.append('<ul></ul>');
			$chatGeneral = $chatGeneral.find('ul');
		}
		var $chatPrivate = $('.chat-private');
		if ($chatPrivate.find('ul').length) {
			$chatPrivate = $chatPrivate.find('ul');
		} else {
			$chatPrivate.append('<ul></ul>');
			$chatPrivate = $chatPrivate.find('ul');
		}

		for (var i = 0; i < 115; i++) {
			$chatGeneral.append('<li><span>00/00 00:00:00</span>&nbsp;<span data-user>NicknameSome</span>&nbsp;<span>'+message+'</span></li>');
			$chatPrivate.append('<li><span>00/00 00:00:00</span>&nbsp;<span data-user>NicknameSome</span>NicknameTo&nbsp;<span>'+message+'</span></li>');
		}
	}
});