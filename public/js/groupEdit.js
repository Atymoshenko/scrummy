$(function() {
	'use strict';
	var $componentEditForm = $('[data-component="group-edit-form"]');
	account.checkUserTask = 'group';
	account.checkUserParams = {'key': 'groupId', 'value': $componentEditForm.data('group-id')};
	account.listener = function() {
		groupInitEdit();
	};
	function groupInitEdit() {
		var $loader = $('.loader');
		if (account.group) {
			$.ajax({
				url: routes['apiGroupCreateForm']['path'],
				method: 'post',
				data: {'apiToken': localStorage.getItem('apiToken')},
				complete: function() {
					$loader.addClass('hide');
				}
				, success: function(data) {
					if (!data.apiToken) {
						localStorage.removeItem('apiToken');
						initUser();
						return;
					} else {
						localStorage.setItem('apiToken', data.apiToken);
					}

					$componentEditForm.append(data.html);
					$componentEditForm.find('input[name="apiMethod"]').val('groupEdit');
					$componentEditForm.find('#group_name').val(account.group.name);
					$componentEditForm.find('#group_description').val(account.group.description);
					$componentEditForm.find('input[name="private"]').prop('checked', account.group.private);
					$componentEditForm.find('form').on('submit', function(e){onSubmitGroupEditForm($(this),e)})
				}
				, error: function(data) {}
			});
		} else {
			$loader.addClass('hide');
			$componentEditForm.remove();
			$('[data-component="group-edit-error"]').show();
		}
	}
	function onSubmitGroupEditForm($form, e) {
		e.preventDefault();
		var data = $form.serializeArray();
		data.push({'name': 'apiToken', 'value': localStorage.getItem('apiToken')});
		data.push({'name': 'id', 'value': account.group.id});
		disableForm($form);
		$.ajax({
			url: routes['apiGroupEdit']['path'],
			method: $form.attr('method'),
			data: data
			, complete: function() {
				enableForm($form);
			}
			, success: function(data) {
				if (!data.apiToken) {
					localStorage.removeItem('apiToken');
					initUser();
					return;
				} else {
					localStorage.setItem('apiToken', data.apiToken);
				}
				parseNotifications(data.notifications, $form);
				if (data.success) {
				}
			}
			, error: function(data) {}
		});
	}
});