<?php
namespace App\Entity;

class Photo {
	/** @var int */
	private $id;
	/** @var int */
	private $userId;
	/** @var bool */
	private $enabled = true;
	/** @var string|null */
	private $name;
	/** @var \DateTime */
	private $created;
	/** @var bool */
	private $approved = false;
	/** @var \DateTime|null */
	private $approvedAt;
	/** @var int|null */
	private $approvedUserId;
	/** @var string */
	private $hash;

	public function __construct(User $user, string $hash)
	{
		$this->userId = $user->getId();
		$this->hash = $hash;
		$this->created = new \DateTime();
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function isEnabled(): bool
	{
		return $this->enabled;
	}

	public function setEnabled(bool $enabled): void
	{
		$this->enabled = $enabled;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function isApproved(): bool
	{
		return $this->approved;
	}

	public function setApproved(bool $approved): void
	{
		$this->approved = $approved;
	}

	public function getApprovedAt(): ?\DateTime
	{
		return $this->approvedAt;
	}

	public function setApprovedAt(\DateTime $approvedAt): void
	{
		$this->approvedAt = $approvedAt;
	}

	public function getApprovedUserId(): ?int
	{
		return $this->approvedUserId;
	}

	public function setApprovedUserId(int $approvedUserId): void
	{
		$this->approvedUserId = $approvedUserId;
	}

	public function getHash(): string
	{
		return $this->hash;
	}

	public function setHash(string $hash): void
	{
		$this->hash = $hash;
	}
}