<?php
namespace App\Entity;

class Room {
	/** @var int */
	private $id;
	/** @var string */
	private $name;
	/** @var bool */
	private $default = false;
	/** @var int|null */
	private $groupId;

	public function __construct(string $name)
	{
		$this->name = $name;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function isDefault(): bool
	{
		return $this->default;
	}

	public function setDefault(bool $default): void
	{
		$this->default = $default;
	}

	public function getGroupId(): ?int
	{
		return $this->groupId;
	}

	public function setGroup(Group $group): void
	{
		$this->groupId = $group->getId();
	}
}