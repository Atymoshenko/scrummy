<?php
namespace App\Entity;

class GroupUser {
	/** @var int */
	private $groupId;
	/** @var int */
	private $userId;
	/** @var \DateTime */
	private $createdAt;
	/** @var bool */
	private $approved = false;
	/** @var \DateTime|null */
	private $approvedAt;
	/** @var int|null */
	private $approvedUserId;

	public function __construct(Group $group, User $user)
	{
		$this->groupId = $group->getId();
		$this->userId = $user->getId();
		$this->createdAt = new \DateTime();
	}

	public function getGroupId(): int
	{
		return $this->groupId;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function isApproved(): bool
	{
		return $this->approved;
	}

	public function setApproved(bool $approved): void
	{
		$this->approved = $approved;
	}

	public function getApprovedAt(): ?\DateTime
	{
		return $this->approvedAt;
	}

	public function setApprovedAt(): void
	{
		$this->approvedAt = new \DateTime();
	}

	public function getApprovedUserId(): ?int
	{
		return $this->approvedUserId;
	}

	public function setApprovedUserId(User $approvedUser): void
	{
		$this->approvedUserId = $approvedUser->getId();
	}
}