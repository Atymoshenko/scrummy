<?php
namespace App\Entity;

class PhotoData {
	/** @var int */
	private $photoId;
	/** @var string */
	private $data;

	public function __construct(Photo $photo, string $data)
	{
		$this->photoId = $photo->getId();
		$this->data = $data;
	}

	public function getPhotoId(): int
	{
		return $this->photoId;
	}

	public function getData(): string
	{
		return $this->data;
	}
}