<?php
namespace App\Entity;

class UserProfileLike {
	/** @var int */
	private $userId;
	/** @var int */
	private $userIdLike;
	/** @var \DateTime */
	private $createdAt;

	public function __construct(User $user, User $userLike)
	{
		$this->userId = $user->getId();
		$this->userIdLike = $userLike->getId();
		$this->setCreatedAt();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdLike(): int
	{
		return $this->userIdLike;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function setCreatedAt(): void
	{
		$this->createdAt = new \DateTime();
	}

}