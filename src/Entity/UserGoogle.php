<?php
namespace App\Entity;

class UserGoogle {
	/** @var int */
	private $userId;
	/** @var string */
	private $googleId;
	/** @var string */
	private $payload;

	public function __construct(User $user, string $googleId, string $payload)
	{
		$this->userId = $user->getId();
		$this->googleId = $googleId;
		$this->payload = $payload;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getGoogleId(): string
	{
		return $this->googleId;
	}

	public function getPayload(): string
	{
		return $this->payload;
	}
}