<?php
namespace App\Entity;

class User
{
	/** @var int */
	private $id;
	/** @var string|null */
	private $email;
	/** @var bool */
	private $emailConfirmed = false;
	/** @var string|null */
	private $emailConfirmToken;
	/** @var \DateTime|null */
	private $emailConfirmExpire;
	/** @var bool */
	private $emailConfirmSended = false;
	/** @var string */
	private $nickname;
	/** @var string */
	private $sex;
	/** @var int */
	private $lifetime = 0;
	/** @var string */
	private $password;
	/** @var string */
	private $passwordHash;
	/** @var string */
	private $ipCreated;
	/** @var string */
	private $hostCreated;
	/** @var string */
	private $userAgentCreated;
	/** @var string */
	private $userAcceptLanguage;
	/** @var \DateTime */
	private $created;
	/** @var int */
	private $roomId;
	/** @var \DateTime */
	private $lastVisit;
	/** @var \DateTime|null */
	private $lastMessageLoad;
	/** @var string|null */
	private $apiToken;
	/** @var \DateTime|null */
	private $apiTokenExpire;
	/** @var bool */
	private $friend = false;

	public function __construct(string $email, string $nickname, string $password, string $sex, string $ipCreated, string $hostCreated, string $userAgentCreated, string $userAcceptLanguage)
	{
		$this->setEmail($email);
		$this->setNickname($nickname);
		$this->setSex($sex);
		$this->created = new \DateTime();
		$this->ipCreated = $ipCreated;
		$this->hostCreated = $hostCreated;
		$this->userAgentCreated = $userAgentCreated;
		$this->userAcceptLanguage = $userAcceptLanguage;
		$this->setPassword($password);
		$this->setPasswordHash();
		$this->setEmailConfirmExpire();
		$this->setEmailConfirmToken();
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): void
	{
		$this->email = strtolower($email);
	}

	public function isEmailConfirmed(): bool
	{
		return $this->emailConfirmed;
	}

	public function setEmailConfirmed(bool $emailConfirmed): void
	{
		$this->emailConfirmed = $emailConfirmed;

		if ($this->isEmailConfirmed()) {
			$this->emailConfirmExpire = new \DateTime();
		}
	}

	public function getEmailConfirmToken(): ?string
	{
		return $this->emailConfirmToken;
	}

	public function setEmailConfirmToken(): void
	{
		$this->emailConfirmToken = self::generateEmailConfirmToken($this->getEmail(), $this->getEmailConfirmExpire());
	}

	public function removeEmailConfirmToken(): void
	{
		$this->emailConfirmToken = null;
	}

	public function getEmailConfirmExpire(): ?\DateTime
	{
		return $this->emailConfirmExpire;
	}

	public function setEmailConfirmExpire(): void
	{
		$this->emailConfirmExpire = new \DateTime('now + 1 day');
	}

	public function removeEmailConfirmExpire(): void
	{
		$this->emailConfirmExpire = null;
	}

	public function isEmailConfirmExpired(): bool
	{
		return !$this->isEmailConfirmed() && ($this->getEmailConfirmExpire() && $this->getEmailConfirmExpire() < new \DateTime());
	}

	public function isEmailConfirmSended(): bool
	{
		return $this->emailConfirmSended;
	}

	public function setEmailConfirmSended(bool $emailConfirmSended): void
	{
		$this->emailConfirmSended = $emailConfirmSended;
	}

	public function getNickname(): string
	{
		return $this->nickname;
	}

	public function setNickname(string $nickname): void
	{
		$this->nickname = $nickname;
	}

	public function getSex(): ?string
	{
		return $this->sex;
	}

	public function setSex(string $sex): void
	{
		$this->sex = $sex;
	}

	public function getLifetime(): int
	{
		return $this->lifetime;
	}

	public function setLifetimeFrom(\DateTime $lastUpdate): void
	{
		$currentDate = new \DateTime();

		$this->lifetime+= $currentDate->getTimestamp() - $lastUpdate->getTimestamp();
	}

	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	public function getPasswordHash(): string
	{
		return $this->passwordHash;
	}

	public function setPasswordHash(): void
	{
		$this->passwordHash = self::generatePasswordHash($this->getEmail(), $this->getNickname(), $this->getPassword(), $this->getCreated());
	}

	public function getIpCreated(): string
	{
		return $this->ipCreated;
	}

	public function getHostCreated(): string
	{
		return $this->hostCreated;
	}

	public function getUserAgentCreated(): string
	{
		return $this->userAgentCreated;
	}

	public function getUserAcceptLanguage(): string
	{
		return $this->userAcceptLanguage;
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getRoomId(): int
	{
		return $this->roomId;
	}

	public function setRoomId(int $roomId): void
	{
		$this->roomId = $roomId;
	}

	public function getLastVisit(): ?\DateTime
	{
		return $this->lastVisit;
	}

	public function setLastVisit(): void
	{
		$this->lastVisit = new \DateTime();
	}

	public function getLastMessageLoad(): ?\DateTime
	{
		return $this->lastMessageLoad;
	}

	public function setLastMessageLoad(\DateTime $lastMessageLoad): void
	{
		$this->lastMessageLoad = $lastMessageLoad;
	}

	public function removeLastMessageLoad(): void
	{
		$this->lastMessageLoad = null;
	}

	public function getApiToken(): ?string
	{
		return $this->apiToken;
	}

	public function setApiToken(string $apiToken): void
	{
		$this->apiToken = $apiToken;
	}

	public function removeApiToken(): void
	{
		$this->apiToken = null;
	}

	public function getApiTokenExpire(): ?\DateTime
	{
		return $this->apiTokenExpire;
	}

	public function setApiTokenExpire(\DateTime $apiTokenExpire): void
	{
		$this->apiTokenExpire = $apiTokenExpire;
	}

	public function isApiTokenExpired(): bool
	{
		if (is_null($this->getApiTokenExpire())) {
			return true;
		}

		$date = new \DateTime();

		return $this->getApiTokenExpire() <= $date;
	}

	public function isFriend(): bool
	{
		return $this->friend;
	}

	public function setFriend(bool $friend): void
	{
		$this->friend = $friend;
	}

	static public function generatePasswordHash(string $email, string $nickname, string $password, \DateTime $created): string
	{
		$str = $email . '|' . $nickname . '|' . $password . '|' . $created->format('U');

		return sha1($str);
	}

	static public function generateEmailConfirmToken(string $email, \DateTime $emailConfirmExpire): string
	{
		return md5($email . '|' . $emailConfirmExpire->format('U'));
	}

	static public function generatePassword(): string
	{
		$password = substr(base64_encode(md5('SecretKey' . (string)(new \DateTime())->format('U'))), 2, rand(8, 10));

		return $password;
	}
}