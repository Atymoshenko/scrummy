<?php
namespace App\Entity;

class UserView {
	/** @var int */
	private $userId;
	/** @var int */
	private $userIdView;
	/** @var \DateTime */
	private $watchedAt;

	public function __construct(User $user, User $userView)
	{
		$this->userId = $user->getId();
		$this->userIdView = $userView->getId();
		$this->setWatchedAt();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdView(): int
	{
		return $this->userIdView;
	}

	public function getWatchedAt(): \DateTime
	{
		return $this->watchedAt;
	}

	public function setWatchedAt(): void
	{
		$this->watchedAt = new \DateTime();
	}

}