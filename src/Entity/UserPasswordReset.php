<?php
namespace App\Entity;

use DateTime;

class UserPasswordReset
{
	/** @var int */
	private $userId;
	/** @var DateTime */
	private $expire;
	/** @var string */
	private $code;

	public function __construct(User $user)
	{
		$this->userId = $user->getId();
		$this->setExpire();
		$this->setCode();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getExpire(): DateTime
	{
		return $this->expire;
	}

	public function setExpire(): void
	{
		$this->expire = new DateTime('now + 1 hour');
		$this->setCode();
	}

	public function isExpired(): bool
	{
		return $this->getExpire() <= new DateTime();
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function setCode(): void
	{
		$this->code = self::generateCode($this->getUserId(), $this->getExpire());
	}

	static public function generateCode(int $userId, DateTime $expire): string
	{
		return md5(
			'mySaltUserPasswordReset|' . (string)$userId . '|' . $expire->format('U')
		);
	}
}