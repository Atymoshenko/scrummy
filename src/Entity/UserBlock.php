<?php
namespace App\Entity;

class UserBlock {
	const STATUS_BLOCKED = 'blocked';
	const STATUS_NOT_BLOCKED = 'notBlocked';

	/** @var int */
	private $userId;
	/** @var int */
	private $userIdBlocked;
	/** @var \DateTime */
	private $createdAt;
	/** @var \DateTime|null */
	private $expiredAt;

	public function __construct(User $user, User $userBlocked)
	{
		$this->userId = $user->getId();
		$this->userIdBlocked = $userBlocked->getId();
		$this->createdAt = new \DateTime();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdBlocked(): int
	{
		return $this->userIdBlocked;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function getExpiredAt(): ?\DateTime
	{
		return $this->expiredAt;
	}

	public function setExpiredAt(\DateTime $expiredAt): void
	{
		$this->expiredAt = $expiredAt;
	}
}