<?php
namespace App\Entity;

class UserFriend {
	const STATUS_FRIEND = 'friend';
	const STATUS_NOT_FRIEND = 'notFriend';
	const STATUS_SENT = 'sent';
	const STATUS_NEED_APPROVE = 'needApprove';

	/** @var int */
	private $userId;
	/** @var int */
	private $userIdFriend;
	/** @var \DateTime */
	private $createdAt;
	/** @var \DateTime|null */
	private $approvedAt;

	public function __construct(User $user, User $userFriend)
	{
		$this->userId = $user->getId();
		$this->userIdFriend = $userFriend->getId();
		$this->createdAt = new \DateTime();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdFriend(): int
	{
		return $this->userIdFriend;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function getApprovedAt(): ?\DateTime
	{
		return $this->approvedAt;
	}

	public function setApprovedAt(): void
	{
		$this->approvedAt = new \DateTime();
	}
}