<?php
namespace App\Entity;

class UserProfile {
	/** @var int */
	private $userId;
	/** @var string|null */
	private $locale;
	/** @var string|null */
	private $name;
	/** @var string|null */
	private $surname;
	/** @var string|null */
	private $countryCode;
	/** @var int|null */
	private $cityId;
	/** @var int */
	private $groupCount = 0;
	/** @var int */
	private $viewsCount = 0;
	/** @var int */
	private $likeCount = 0;
	/** @var array */
	private $locales = [];

	public function __construct(User $user)
	{
		$this->userId = $user->getId();
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getLocale(): ?string
	{
		return $this->locale;
	}

	public function setLocale(?string $locale): void
	{
		$this->locale = $locale;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name): void
	{
		$this->name = $name;
	}

	public function getSurname(): ?string
	{
		return $this->surname;
	}

	public function setSurname(?string $surname): void
	{
		$this->surname = $surname;
	}

	public function getCountryCode(): ?string
	{
		return $this->countryCode;
	}

	public function setCountry(?Country $country): void
	{
		if ($country) {
			$this->countryCode = $country->getCode();
		} else {
			$this->countryCode = null;
		}
	}

	public function getCityId(): ?int
	{
		return $this->cityId;
	}

	public function setCity(?City $city): void
	{
		if ($city) {
			$this->cityId = $city->getId();
		} else {
			$this->cityId = null;
		}
	}

	public function getGroupCount(): int
	{
		return $this->groupCount;
	}

	public function setGroupCount(int $groupCount): void
	{
		$this->groupCount = $groupCount;
	}

	public function getViewsCount(): int
	{
		return $this->viewsCount;
	}

	public function incrementViewsCount(): void
	{
		$this->viewsCount++;
	}

	public function getLikeCount(): int
	{
		return $this->likeCount;
	}

	public function incrementLikeCount(): void
	{
		$this->likeCount++;
	}

	public function getLocales(): array
	{
		return $this->locales;
	}

	public function setLocales(array $locales): void
	{
		$this->locales = $locales;
	}

	public function toArray(): array
	{
		return [
			'userId' => $this->getUserId(),
			'locale' => $this->getLocale(),
			'name' => $this->getName(),
			'surname' => $this->getSurname(),
			'countryCode' => $this->getCountryCode(),
			'cityId' => $this->getCityId(),
			'groupCount' => $this->getGroupCount(),
		];
	}
}