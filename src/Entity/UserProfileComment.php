<?php
namespace App\Entity;

class UserProfileComment {
	/** @var int */
	private $id;
	/** @var int */
	private $userId;
	/** @var int */
	private $userIdComment;
	/** @var \DateTime */
	private $createdAt;
	/** @var string */
	private $content;

	public function __construct(User $user, User $userComment, string $content)
	{
		$this->userId = $user->getId();
		$this->userIdComment = $userComment->getId();
		$this->setCreatedAt();
		$this->setContent($content);
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdComment(): int
	{
		return $this->userIdComment;
	}

	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	public function setCreatedAt(): void
	{
		$this->createdAt = new \DateTime();
	}

	public function getContent(): string
	{
		return $this->content;
	}

	public function setContent(string $content): void
	{
		$this->content = $content;
	}
}