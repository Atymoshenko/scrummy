<?php
namespace App\Entity;

class Message extends BaseMessage {
	const FORMAT_DATE_TIME = 'Y-m-d H:i:s';

	/** @var string */
	private $hash;
	/** @var int */
	private $roomId;
	/** @var int */
	private $userId;
	/** @var int|null */
	private $userIdTo;
	/** @var \DateTime */
	private $created;
	/** @var string */
	private $message;

	public function __construct(User $user, string $message, int $roomId)
	{
		$this->roomId = $roomId;
		$this->userId = $user->getId();
		$this->message = $message;
		$this->created = new \DateTime();
	}

	public function getHash(): string
	{
		if (is_null($this->hash)) {
			$this->generateHash();
		}

		return $this->hash;
	}

	public function setHash(): void
	{
		$this->generateHash();
	}

	private function generateHash(): void
	{
		$this->hash = md5(
			$this->userId
			. '|' . (is_null($this->getUserIdTo()) ? '' : $this->userIdTo)
			. '|' . $this->getCreated()->format('U')
			. '|' . $this->getMessage()
		);
	}

	public function getRoomId(): int
	{
		return $this->roomId;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function getUserIdTo(): ?int
	{
		return $this->userIdTo;
	}

	public function setUserTo(User $userTo): void
	{
		$this->userIdTo = $userTo->getId();
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getMessage(): string
	{
		return $this->message;
	}

	public function setMessage(string $message, $clean = true): void
	{
		if ($clean) {
			$message = $this->cleanMessage($message);
		}

		$this->message = $message;
	}

	private function cleanMessage(string $message): string
	{
		$message = trim($message);
		$message = preg_replace("~\s+~s", ' ', $message);

		return $message;
	}

	public function toArray(): array
	{
		return [
			'hash' => $this->getHash(),
			'userId' => $this->getUserId(),
			'userIdTo' => $this->getUserIdTo(),
			'created' => $this->getCreated()->format(self::FORMAT_DATE_TIME),
			'message' => $this->getMessage(),
		];
	}

	public function parseMessage(string $message): array
	{
		preg_match("~^(([^\s]+)(:\s*))?(.*)$~", $message, $matches);

		return [($matches[2] !== '' ? $matches[2] : null), $matches[4]];
	}

	public function preSave(): void
	{
		$this->generateHash();
	}
}