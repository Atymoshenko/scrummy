<?php
namespace App\Entity;

use App\Interfaces\iLocale;

class City implements iLocale {
	/** @var string|null */
	private $locale;
	/** @var int */
	private $id;
	/** @var string */
	private $countryCode;
	/** @var string */
	private $nameEn;
	/** @var string */
	private $nameRu;
	/** @var int */
	private $priority = 100;

	public function setLocale(string $locale) {
		$this->locale = $locale;
	}

	public function __construct(Country $country, string $name)
	{
		$this->countryCode = $country->getCode();
		$this->name = $name;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getCountryCode(): string
	{
		return $this->countryCode;
	}

	public function getNameEn(): string
	{
		return $this->nameEn;
	}

	public function getNameRu(): string
	{
		return $this->nameRu;
	}

	public function setNameRu(string $nameRu): void
	{
		$this->nameRu = $nameRu;
	}

	public function getName(): string
	{
		switch ($this->locale) {
			case 'ru':
				return $this->getNameRu();
			case 'en':
			default:
				return $this->getNameEn();
		}
	}

	public function getPriority(): int
	{
		return $this->priority;
	}

	public function setPriority(int $priority): void
	{
		$this->priority = $priority;
	}
}