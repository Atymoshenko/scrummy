<?php
namespace App\Entity;

use App\Interfaces\iLocale;

class Country implements iLocale {
	/** @var string|null */
	private $locale;
	/** @var string */
	private $code;
	/** @var string */
	private $nameEn;
	/** @var string */
	private $nameRu;
	/** @var int */
	private $priority = 100;

	public function setLocale(string $locale) {
		$this->locale = $locale;
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function getNameEn(): string
	{
		return $this->nameEn;
	}

	public function getNameRu(): string
	{
		return $this->nameRu;
	}

	public function setNameRu(string $nameRu): void
	{
		$this->nameRu = $nameRu;
	}

	public function getName(): string
	{
		switch ($this->locale) {
			case 'ru':
				return $this->getNameRu();
			case 'en':
			default:
				return $this->getNameEn();
		}
	}

	public function getPriority(): int
	{
		return $this->priority;
	}

	public function setPriority(int $priority): void
	{
		$this->priority = $priority;
	}
}