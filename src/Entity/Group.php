<?php
namespace App\Entity;

class Group {
	/** @var int */
	private $id;
	/** @var int */
	private $userId;
	/** @var bool */
	private $private = false;
	/** @var string */
	private $name;
	/** @var string */
	private $description;

	public function __construct(User $user, string $name, string $description)
	{
		$this->setUserId($user->getId());
		$this->setName($name);
		$this->setDescription($description);
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getUserId(): int
	{
		return $this->userId;
	}

	public function setUserId(int $userId): void
	{
		$this->userId = $userId;
	}

	public function isPublic(): bool
	{
		return !$this->isPrivate();
	}

	public function isPrivate(): bool
	{
		return $this->private;
	}

	public function setPrivate(bool $private): void
	{
		$this->private = $private;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}
}