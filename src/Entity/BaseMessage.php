<?php
namespace App\Entity;

class BaseMessage {
	const MAX_MESSAGES_PUBLIC_COUNT = 100;
	const MAX_MESSAGES_PRIVATE_COUNT = 100;
}