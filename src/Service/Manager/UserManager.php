<?php
namespace App\Service\Manager;

use App\Entity\User;
use App\Framework\App;

class UserManager extends AbstractManager {
	private $user;

	public function __construct(App $app, User $user)
	{
		parent::__construct($app);

		$this->user = $user;
	}

	public function getEmailConfirmExpireText(): string
	{
		return $this->getApp()->trans(
			'You must confirm the mail. The confirmation code and the instruction has been sent to the address you entered. You must confirm before {{ date }}, otherwise your account will be deleted. If you did not find the email, look in the spam folder.',
			[
				'{{ date }}' => $this->user->getEmailConfirmExpire() ? $this->user->getEmailConfirmExpire()->format('d.m.Y H:i:s') : '',
			]
		);
	}
}