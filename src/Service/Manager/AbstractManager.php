<?php
namespace App\Service\Manager;

use App\Framework\App;

class AbstractManager {
	private $app;

	public function __construct(App $app)
	{
		$this->app = $app;
	}

	public function getApp(): App
	{
		return $this->app;
	}

}