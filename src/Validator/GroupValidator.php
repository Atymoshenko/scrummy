<?php
namespace App\Validator;

use App\Entity\Group;
use App\Framework\App;

class GroupValidator extends Validator {
	const NAME_LENGTH_MIN = 2;
	const NAME_LENGTH_MAX = 20;
	const DESCRIPTION_LENGTH_MIN = 10;
	const DESCRIPTION_LENGTH_MAX = 255;

	/** @var Group */
	protected $group;

	public function __construct(Group $group, App $app = null)
	{
		parent::__construct($group, $app);
		$this->group = $group;
	}

	public function validate(): void
	{
		$this->validateUser();
		$this->validateName();
		$this->validateDescription();
	}

	public function validateUser(): bool
	{
		if (!$this->group->getUserId()) {
			$this->addError('user', 'User is required.');

			return false;
		}

		$this->addSuccess('user');

		return true;
	}

	public function validateNameRequire(): bool
	{
		if (!$this->group->getName()) {
			$this->addError('name', 'Name is required.');

			return false;
		}

		return true;
	}

	public function validateDescriptionRequire(): bool
	{
		if (!$this->group->getDescription()) {
			$this->addError('description', 'Description is required.');

			return false;
		}

		return true;
	}

	public function validateName(): bool
	{
		if (!$this->validateNameRequire()) {
			return false;
		}

		$length = mb_strlen($this->group->getName());
		if ($length < self::NAME_LENGTH_MIN || $length > self::NAME_LENGTH_MAX) {
			$this->addError('name', 'Name must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::NAME_LENGTH_MIN,
				'max' => self::NAME_LENGTH_MAX,
			]);

			return false;
		}

		$this->addSuccess('name');

		return true;
	}

	public function validateDescription(): bool
	{
		if (!$this->validateDescriptionRequire()) {
			return false;
		}

		$length = mb_strlen($this->group->getDescription());
		if ($length < self::DESCRIPTION_LENGTH_MIN || $length > self::DESCRIPTION_LENGTH_MAX) {
			$this->addError('description', 'Description must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::DESCRIPTION_LENGTH_MIN,
				'max' => self::DESCRIPTION_LENGTH_MAX,
			]);

			return false;
		}

		$this->addSuccess('description');

		return true;
	}
}