<?php
namespace App\Validator;

use App\Entity\UserProfile;
use App\Framework\App;

class UserProfileValidator extends Validator {
	const NAME_LENGTH_MIN = 2;
	const NAME_LENGTH_MAX = 20;
	const SURNAME_LENGTH_MIN = 2;
	const SURNAME_LENGTH_MAX = 20;

	/** @var UserProfile */
	protected $userProfile;

	public function __construct(UserProfile $userProfile, App $app = null)
	{
		parent::__construct($userProfile, $app);
		$this->userProfile = $userProfile;
	}

	public function validate(): void
	{
		$this->validateLocale();
		$this->validateName();
		$this->validateSurname();
	}

	public function validateLocaleRequire(): bool
	{
		if (!$this->userProfile->getLocale()) {
			$this->addError('locale', 'Locale is required.');

			return false;
		}

		return true;
	}

	public function validateLocale(): bool
	{
		if (is_null($this->userProfile->getLocale())) {
			return true;
		}

		if (!array_key_exists($this->userProfile->getLocale(), $this->userProfile->getLocales())) {
			$this->addError('locale', 'Invalid locale.');

			return false;
		}

		if (!$this->userProfile->getLocales()[$this->userProfile->getLocale()]['enabled']) {
			$this->addError('locale', 'Invalid locale.');

			return false;
		}

		$this->addSuccess('locale');

		return true;
	}

	public function validateName(): bool
	{
		if (!$this->userProfile->getName()) {
			return true;
		}

		$result = $this->validateNameString('name', $this->userProfile->getName(), 'Name', self::NAME_LENGTH_MIN, self::NAME_LENGTH_MAX);

		if ($result) {
			$this->addSuccess('name');
		}

		return $result;
	}

	public function validateSurname(): bool
	{
		if (!$this->userProfile->getSurname()) {
			return true;
		}

		$result = $this->validateNameString('surname', $this->userProfile->getSurname(), 'Surname', self::NAME_LENGTH_MIN, self::NAME_LENGTH_MAX);

		if ($result && !$this->getErrors('name')) {
			$value = $this->userProfile->getName() . $this->userProfile->getSurName();
			if (
				(
					preg_match("~[\p{Cyrillic}]~u", $value)
					&& preg_match("~[\p{Latin}]~u", $value)
				)
				||
				(
					preg_match("~[\p{Cyrillic}]~u", $value)
					&& preg_match("~[ґєії]~iu", $value)
					&& preg_match("~[ёъыэ]~iu", $value)
				)
			) {
				$this->addError('surname', 'Surname must be in some language with name.');

				return false;
			}
		}

		if ($result) {
			$this->addSuccess('surname');
		}

		return $result;
	}

	private function validateNameString(string $key, $value, string $prefix, int $lengthMin, int $lengthMax): bool
	{
		$length = mb_strlen($value);
		if ($length < $lengthMin || $length > $lengthMax) {
			$this->addError($key, $prefix . ' must contain from {{ min }} to {{ max }} characters.', [
				'min' => $lengthMin,
				'max' => $lengthMax,
			]);

			return false;
		}

		if (!preg_match("~^[\p{Cyrillic}\p{Latin}]+$~u", $value)) {
			$this->addError($key, $prefix . ' must contain only letters.');

			return false;
		}

		if (
			(
				preg_match("~[\p{Cyrillic}]~u", $value)
				&& preg_match("~[\p{Latin}]~u", $value)
			)
			||
			(
				preg_match("~[\p{Cyrillic}]~u", $value)
				&& preg_match("~[ґєії]~iu", $value)
				&& preg_match("~[ёъыэ]~iu", $value)
			)
		) {
			$this->addError($key, $prefix . ' must be in only one language.');

			return false;
		}

		return true;
	}

	public function validateCountryRequire(): bool
	{
		if (!$this->userProfile->getCountryCode()) {
			$this->addError('country', 'Country is required.');

			return false;
		}

		return true;
	}

	public function validateCityRequire(): bool
	{
		if (!$this->userProfile->getCityId()) {
			$this->addError('city', 'City is required.');

			return false;
		}

		return true;
	}

}