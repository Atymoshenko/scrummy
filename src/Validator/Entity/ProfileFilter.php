<?php
namespace App\Validator\Entity;

class ProfileFilter {
	/** @var string|null */
	private $countryCode;
	/** @var int|null */
	private $cityId;

	public function getCountryCode(): ?string
	{
		return $this->countryCode;
	}

	public function setCountryCode(?string $countryCode): void
	{
		$this->countryCode = $countryCode;
	}

	public function getCityId(): ?int
	{
		return $this->cityId;
	}

	public function setCityId(?int $cityId): void
	{
		$this->cityId = $cityId;
	}
}