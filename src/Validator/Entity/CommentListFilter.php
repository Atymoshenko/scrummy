<?php
namespace App\Validator\Entity;

class CommentListFilter {
	const OBJECT_USER_PROFILE = 'userProfile';

	/** @var string|null */
	private $object;
	/** @var string|null */
	private $objectId;

	static public function getObjects(): array
	{
		return [
			self::OBJECT_USER_PROFILE,
		];
	}

	public function getObject(): ?string
	{
		return $this->object;
	}

	public function setObject(?string $object): void
	{
		$this->object = $object;
	}

	public function getObjectId(): ?string
	{
		return $this->objectId;
	}

	public function setObjectId(?string $objectId): void
	{
		$this->objectId = $objectId;
	}

}