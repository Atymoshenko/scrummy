<?php
namespace App\Validator\Entity;

class GroupFilter {
	/** @var string|null */
	private $countryCode;
	/** @var int|null */
	private $cityId;
	/** @var int|null */
	private $categoryId;

	public function getCountryCode(): ?string
	{
		return $this->countryCode;
	}

	public function setCountryCode(?string $countryCode): void
	{
		$this->countryCode = $countryCode;
	}

	public function getCityId(): ?int
	{
		return $this->cityId;
	}

	public function setCityId(?int $cityId): void
	{
		$this->cityId = $cityId;
	}

	public function getCategoryId(): ?int
	{
		return $this->categoryId;
	}

	public function setCategoryId(?int $categoryId): void
	{
		$this->categoryId = $categoryId;
	}

}