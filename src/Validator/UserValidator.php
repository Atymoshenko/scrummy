<?php
namespace App\Validator;

use App\Entity\User;
use App\Framework\App;

class UserValidator extends Validator {
	const EMAIL_LENGTH_MIN = 5;
	const EMAIL_LENGTH_MAX = 50;
	const NICKNAME_LENGTH_MIN = 2;
	const NICKNAME_LENGTH_MAX = 25;
	const PASSWORD_LENGTH_MIN = 6;
	const PASSWORD_LENGTH_MAX = 30;

	/** @var User */
	protected $user;

	public function __construct(User $user, App $app = null)
	{
		parent::__construct($user, $app);
		$this->user = $user;
	}

	public function validate(): void
	{
		$this->validateEmail();
		$this->validateNickname();
		$this->validatePassword();
		$this->validateSex();
	}

	public function validateEmailRequire(): bool
	{
		if ($this->user->getEmail() == '') {
			$this->addError('email', 'Email is required.');

			return false;
		}

		return true;
	}

	public function validateNicknameRequire(): bool
	{
		if ($this->user->getNickname() == '') {
			$this->addError('nickname', 'Nickname is required.');

			return false;
		}

		return true;
	}

	public function validatePasswordRequire(): bool
	{
		if ($this->user->getPassword() == '') {
			$this->addError('password', 'Password is required.');

			return false;
		}

		return true;
	}

	public function validateEmail(): bool
	{
		if (!$this->validateEmailRequire()) {
			return false;
		}

		$length = mb_strlen($this->user->getEmail());
		if ($length < self::EMAIL_LENGTH_MIN || $length > self::EMAIL_LENGTH_MAX) {
			$this->addError('email', 'Email must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::EMAIL_LENGTH_MIN,
				'max' => self::EMAIL_LENGTH_MAX,
			]);

			return false;
		}

		if (!preg_match("~^[a-z0-9\._]+@[a-z0-9\-]+\.[a-z]{1,4}(\.[a-z]{1,4})?$~su", $this->user->getEmail())) {
			$this->addError('email', 'Email must contain only latin letters (a-z), digits (0-9) and symbols dot and underscore (_.) and @(a-z0-9-.).');

			return false;
		}

		$this->addSuccess('email');


		return true;
	}

	public function validatePassword(): bool
	{
		if (!$this->validatePasswordRequire()) {
			return false;
		}

		$length = mb_strlen($this->user->getPassword());
		if ($length < self::PASSWORD_LENGTH_MIN || $length > self::PASSWORD_LENGTH_MAX) {
			$this->addError('password', 'Password must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::PASSWORD_LENGTH_MIN,
				'max' => self::PASSWORD_LENGTH_MAX,
			]);

			return false;
		}

		$this->addSuccess('password');


		return true;
	}

	public function validatePasswordNew(): bool
	{
		if ($this->user->getPassword() === '') {
			$this->addSuccess('password');

			return true;
		}

		return $this->validatePassword();
	}

	public function validateNickname(): bool
	{
		if (!$this->validateNicknameRequire()) {
			return false;
		}

		$length = mb_strlen($this->user->getNickname());
		if ($length < self::NICKNAME_LENGTH_MIN || $length > self::NICKNAME_LENGTH_MAX) {
			$this->addError('nickname', 'Nickname must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::NICKNAME_LENGTH_MIN,
				'max' => self::NICKNAME_LENGTH_MAX,
			]);

			return false;
		}

		$result = true;

		if (!preg_match("~^[\p{Cyrillic}\p{Latin}0-9_-]+$~u", $this->user->getNickname())) {
			$this->addError('nickname', 'Nickname must contain only letters, digits and symbols: _-.');
			$result = false;
		}

		if (
			(
				preg_match("~[\p{Cyrillic}]~u", $this->user->getNickname())
				&& preg_match("~[\p{Latin}]~u", $this->user->getNickname())
			)
			||
			(
				preg_match("~[\p{Cyrillic}]~u", $this->user->getNickname())
				&& preg_match("~[ґєії]~iu", $this->user->getNickname())
				&& preg_match("~[ёъыэ]~iu", $this->user->getNickname())
			)
		) {
			$this->addError('nickname', 'Nickname must be in only one language.');
			$result = false;
		}

		if (!preg_match("~^[[\p{Cyrillic}\p{Latin}]+~iu", $this->user->getNickname())) {
			$this->addError('nickname', 'Nickname should start with a letter.');
			$result = false;
		}

		if ($result) {
			$this->addSuccess('nickname');
		}

		return $result;
	}

	public function validateSex(): bool
	{
		if (!$this->user->getSex()) {
			$this->addError('sex', 'Gender is required.');

			return false;
		}

		if (!in_array($this->user->getSex(), ['M','F'])) {
			$this->addError('sex', 'Wrong gender.');

			return false;
		}

		$this->addSuccess('sex');

		return true;
	}
}