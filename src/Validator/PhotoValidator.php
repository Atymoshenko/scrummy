<?php
namespace App\Validator;

use App\Entity\Photo;
use App\Framework\App;

class PhotoValidator extends Validator {
	const MAX_FILE_SIZE = 9000000;
	const TYPE_PATTERN = "~^image/(jpe?g|png)$~m";
	const ALLOWED_TYPES = "jpg,png";

	/** @var Photo */
	protected $photo;
	private $filePath;
	private $type;
	private $size;

	public function __construct(Photo $photo, App $app = null, string $filePath, string $type, int $size)
	{
		parent::__construct($photo, $app);
		$this->photo = $photo;
		$this->filePath = $filePath;
		$this->type = $type;
		$this->size = $size;
	}

	public function validate(): void
	{
		$this->validateFile();
	}

	private function validateFile(): bool
	{
		if ($this->size > self::MAX_FILE_SIZE) {
			$this->addError('file', 'File size should not exceed {{ size }} megabytes.', [
				'size' => intval(self::MAX_FILE_SIZE / 1000000),
			]);

			return false;
		}

		if (!preg_match(self::TYPE_PATTERN, $this->type)) {
			$this->addError('file', 'That file type is not allowed. Allowed types: {{ types }}.', [
				'types' => self::ALLOWED_TYPES,
			]);

			return false;
		}

		$this->addSuccess('file');

		return true;
	}
}