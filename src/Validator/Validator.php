<?php
namespace App\Validator;

use App\Framework\App;
use App\Framework\Interfaces\iValidator;

abstract class Validator implements iValidator {
	/** @var App|null */
	private $app;
	private $errors = [];
	private $successes = [];

	public function __construct($object, App $app = null)
	{
		$this->app = $app;
	}

	abstract public function validate(): void;

	public function getErrors(string $key = null): array
	{
		return $key ? ([$this->errors[$key]] ?? []) : $this->errors;
	}

	public function getError(string $key = null): string
	{
		$errors = [];

		foreach ($this->getErrors($key) as $error) {
			$errors[] = is_array($errors) ? implode(' ', $error) : $error;
		}

		return implode(' ', $errors);
	}

	public function getSuccesses(): array
	{
		return $this->successes;
	}

	public function hasErrors(): bool
	{
		return (bool) $this->getErrors();
	}

	public function addError(string $key, string $message, array $params = []): void
	{
		if ($this->app) {
			$message = $this->app->trans($message);
		}
		foreach ($params as $k => $v) {
			$message = preg_replace("~\{\{\s*" . $k . "\s*\}\}~", $v, $message);
		}
		$this->errors[$key][] = $message;
		unset($this->successes[$key]);
	}

	public function addSuccess(string $key): void
	{
		if (!isset($this->errors[$key])) {
			$this->successes[$key] = true;
		}
	}

	public function getNotifications(): array
	{
		return [
			'success' => $this->getSuccesses(),
			'errors' => $this->getErrors(),
		];
	}
}