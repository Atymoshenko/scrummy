<?php
namespace App\Validator;

use App\Entity\Message;
use App\Framework\App;

class MessageValidator extends Validator {
	const MESSAGE_LENGTH_MIN = 1;
	const MESSAGE_LENGTH_MAX = 255;
	const MESSAGE_NO_SPACE_SYMBOLS = 20;

	private $message;

	public function __construct(Message $message, App $app = null)
	{
		parent::__construct($message, $app);
		$this->message = $message;
	}

	public function validate(): void
	{
		$this->validateMessage();
	}

	private function validateMessage(): bool
	{
		if ($this->message->getMessage() == '') {
			$this->addError('message', 'Message is required.');

			return false;
		}

		$length = mb_strlen($this->message->getMessage());
		if ($length < self::MESSAGE_LENGTH_MIN || $length > self::MESSAGE_LENGTH_MAX) {
			$this->addError('message', 'Message must contain from {{ min }} to {{ max }} characters.', [
				'min' => self::MESSAGE_LENGTH_MIN,
				'max' => self::MESSAGE_LENGTH_MAX,
			]);

			return false;
		}

		if (preg_match("~[^\s]{" . self::MESSAGE_NO_SPACE_SYMBOLS . ",}~su", $this->message->getMessage())) {
			$this->addError('message', 'Must be a space between {{ count }} non-whitespace characters.', [
				'count' => self::MESSAGE_NO_SPACE_SYMBOLS,
			]);

			return false;
		}

		return true;
	}
}