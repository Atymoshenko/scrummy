<?php
namespace App\Validator\Filter;

use App\Framework\App;
use App\Validator\Entity\GroupFilter;
use App\Validator\Validator;

class GroupFilterValidator extends Validator {
	/** @var GroupFilter */
	protected $groupFilter;

	public function __construct(GroupFilter $groupFilter, App $app = null)
	{
		parent::__construct($groupFilter, $app);
		$this->groupFilter = $groupFilter;
	}

	public function validate(): void
	{
		$this->validateCountry();
		$this->validateCity();
		$this->validateCategory();
	}

	private function validateCountry(): bool
	{
		if (!$this->groupFilter->getCountryCode()) {
			$this->addError('country', 'Country is required.');

			return false;
		}

		$this->addSuccess('country');

		return true;
	}

	private function validateCity(): bool
	{
		if (!$this->groupFilter->getCityId()) {
			$this->addError('city', 'City is required.');

			return false;
		}

		$this->addSuccess('city');

		return true;
	}

	private function validateCategory(): bool
	{
		if (!$this->groupFilter->getCategoryId()) {
			return true;
			$this->addError('category', 'Category is required.');

			return false;
		}

		$this->addSuccess('category');

		return true;
	}
}