<?php
namespace App\Validator\Filter;

use App\Entity\UserProfileComment;
use App\Framework\App;
use App\Validator\Validator;

class UserProfileCommentValidator extends Validator {
	const CONTENT_MAX_LENGTH = 2000;

	/** @var UserProfileComment */
	protected $userProfileComment;

	public function __construct(UserProfileComment $userProfileComment, App $app = null)
	{
		parent::__construct($userProfileComment, $app);
		$this->userProfileComment = $userProfileComment;
	}

	public function validate(): void
	{
		$this->validateContent();
	}

	private function validateContent(): bool
	{
		if ($this->userProfileComment->getContent() === '') {
			$this->addError('content', 'Content is required.');

			return false;
		}

		if (mb_strlen($this->userProfileComment->getContent()) > self::CONTENT_MAX_LENGTH) {
			$this->addError('content', 'Comment must contain from 1 to {{ max }} characters.', [
				'max' => self::CONTENT_MAX_LENGTH,
			]);

			return false;
		}

		$this->addSuccess('content');

		return true;
	}
}