<?php
namespace App\Validator\Filter;

use App\Framework\App;
use App\Validator\Entity\CommentListFilter;
use App\Validator\Validator;

class CommentListFilterValidator extends Validator {
	/** @var CommentListFilter */
	protected $commentListFilter;

	public function __construct(CommentListFilter $commentListFilter, App $app = null)
	{
		parent::__construct($commentListFilter, $app);
		$this->commentListFilter = $commentListFilter;
	}

	public function validate(): void
	{
		$this->validateObject();
		$this->validateObjectId();
	}

	private function validateObject(): bool
	{
		if (!$this->commentListFilter->getObject()) {
			$this->addError('object', 'Object is required.');

			return false;
		}

		if (!in_array($this->commentListFilter->getObject(), CommentListFilter::getObjects())) {
			$this->addError('object', 'Undefined object ' . htmlspecialchars($this->commentListFilter->getObject()) . '.');

			return false;
		}

		return true;
	}

	private function validateObjectId(): bool
	{
		if (!$this->commentListFilter->getObjectId()) {
			$this->addError('objectId', 'ObjectId is required.');

			return false;
		}

		return true;
	}
}