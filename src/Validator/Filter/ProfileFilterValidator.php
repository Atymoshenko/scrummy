<?php
namespace App\Validator\Filter;

use App\Framework\App;
use App\Validator\Entity\ProfileFilter;
use App\Validator\Validator;

class ProfileFilterValidator extends Validator {
	/** @var ProfileFilter */
	protected $profileFilter;

	public function __construct(ProfileFilter $profileFilter, App $app = null)
	{
		parent::__construct($profileFilter, $app);
		$this->profileFilter = $profileFilter;
	}

	public function validate(): void
	{
		$this->validateCountry();
		$this->validateCity();
	}

	private function validateCountry(): bool
	{
		$this->addSuccess('countryCode');

		return true;
	}

	private function validateCity(): bool
	{
		$this->addSuccess('cityId');

		return true;
	}
}