CREATE TABLE `country` (
    `code` char(2) NOT NULL COLLATE utf8_unicode_ci,
    `name` varchar(50) NOT NULL COLLATE utf8_unicode_ci,
    PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `country` (`code`,`name`) VALUES ('ua','Ukraine'),('ru','Russia');

ALTER TABLE `user_profile`
    ADD `countryCode` char(2) NULL DEFAULT NULL
    , ADD INDEX `countryCode` (`countryCode`)
    , ADD CONSTRAINT `user_profile_ibfk_country` FOREIGN KEY (`countryCode`) REFERENCES `country` (`code`) ON DELETE SET NULL ON UPDATE CASCADE;

CREATE TABLE `user_chat`
(
    `userId`  int(11) unsigned NOT NULL,
    `token`  char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`userId`),
    CONSTRAINT `user_chat_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
