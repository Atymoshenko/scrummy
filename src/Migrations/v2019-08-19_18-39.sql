ALTER TABLE `user_profile` ADD `likeCount` int(11) unsigned not null default 0;
CREATE TABLE `user_profile_like` (
    `userId` int(11) unsigned NOT NULL,
    `userIdLike` int(11) unsigned NOT NULL,
    `createdAt` datetime NOT NULL,
    PRIMARY KEY (`userId`,`userIdLike`),
    CONSTRAINT `upl_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `upl_ibfk_user2` FOREIGN KEY (`userIdLike`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
