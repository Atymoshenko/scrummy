ALTER TABLE `user_profile` ADD `viewsCount` int(11) unsigned not null default 0;
CREATE TABLE `user_view` (
    `userId` int(11) unsigned NOT NULL,
    `userIdView` int(11) unsigned NOT NULL,
    `watchedAt` datetime NOT NULL,
    PRIMARY KEY (`userId`,`userIdView`),
    CONSTRAINT `user_view_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_view_ibfk_user2` FOREIGN KEY (`userIdView`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user_profile_comment` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `userId` int(11) unsigned NOT NULL,
    `userIdComment` int(11) unsigned NOT NULL,
    `createdAt` datetime NOT NULL,
    `content` text NOT NULL,
    KEY (`userId`),
    KEY (`userIdComment`),
    CONSTRAINT `upc_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `upc_ibfk_user2` FOREIGN KEY (`userIdComment`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
