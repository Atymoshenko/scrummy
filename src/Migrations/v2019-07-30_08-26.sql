CREATE TABLE `user_block`
(
    `userId`  int(11) unsigned NOT NULL,
    `userIdBlocked`  int(11) unsigned NOT NULL,
    `createdAt` datetime NOT NULL,
    `expiredAt` datetime DEFAULT NULL,
    PRIMARY KEY (`userId`,`userIdBlocked`),
    CONSTRAINT `user_block_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_block_blocked_ibfk_user` FOREIGN KEY (`userIdBlocked`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE `user_friend`
(
    `userId`  int(11) unsigned NOT NULL,
    `userIdFriend`  int(11) unsigned NOT NULL,
    `createdAt` datetime NOT NULL,
    PRIMARY KEY (`userId`,`userIdFriend`),
    CONSTRAINT `user_friend_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `user_friend_target_ibfk_user` FOREIGN KEY (`userIdFriend`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
ALTER TABLE `user_friend` ADD `approvedAt` datetime null default null;