CREATE TABLE `city` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `countryCode` char(2) NOT NULL COLLATE utf8_unicode_ci,
    `name` varchar(50) NOT NULL  COLLATE utf8_unicode_ci,
    PRIMARY KEY (`id`),
    KEY `countryCode` (`countryCode`),
    UNIQUE KEY `countryCode_name` (`countryCode`,`name`),
    CONSTRAINT `city_ibfk_country` FOREIGN KEY (`countryCode`) REFERENCES `country` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `city` (`countryCode`,`name`) VALUES ('ua','Kiev'),('ua','Bila Tserkva');
INSERT INTO `city` (`countryCode`,`name`) VALUES ('ru','Moscow'),('ru','Saint Petersburg');

ALTER TABLE `user_profile`
    ADD `cityId` int(11) unsigned NULL DEFAULT NULL AFTER `countryCode`
    , ADD KEY (`cityId`)
    , ADD CONSTRAINT `user_profile_ibfk_city` FOREIGN KEY (`cityId`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
;

CREATE TABLE `group` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `userId` int(11) unsigned NOT NULL,
    `name` varchar(50) NOT NULL COLLATE utf8_unicode_ci,
    `description` text NOT NULL COLLATE utf8_unicode_ci,
    PRIMARY KEY (`id`),
    KEY `userId` (`userId`),
    CONSTRAINT `group_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `user_profile`
    ADD `groupCount` smallint(5) unsigned NOT NULL DEFAULT '0'
;
ALTER TABLE `group` ADD `private` tinyint(1) unsigned not null default '0' AFTER `userId`;
