CREATE TABLE `photo` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `userId` int(11) unsigned NOT NULL,
    `enabled` tinyint(1) unsigned default '1',
    `name` varchar(255) NULL DEFAULT NULL,
    `created` datetime NOT NULL,
    `approved` tinyint(1) unsigned default '0',
    `approvedAt` datetime NULL DEFAULT NULL,
    `approvedUserId` int(11) unsigned NULL DEFAULT NULL,
    `hash` char(32) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `userId` (`userId`),
    KEY `approvedUserId` (`approvedUserId`),
    UNIQUE KEY `hash` (`hash`),
    CONSTRAINT `photo_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
    CONSTRAINT `photo-approvedUserId_ibfk_user` FOREIGN KEY (`approvedUserId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `photo_data` (
     `photoId` int(11) unsigned NOT NULL,
     `data` text COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`photoId`),
     CONSTRAINT `photo_data_ibfk_photo` FOREIGN KEY (`photoId`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
