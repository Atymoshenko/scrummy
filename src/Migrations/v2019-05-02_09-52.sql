CREATE TABLE `group_user` (
    `groupId` int(11) unsigned NOT NULL,
    `userId` int(11) unsigned NOT NULL,
    `createdAt` datetime NOT NULL,
    `approved` tinyint(1) unsigned default '0',
    `approvedAt` datetime NULL DEFAULT NULL,
    `approvedUserId` int(11) unsigned NULL DEFAULT NULL,
    PRIMARY KEY (`groupId`,`userId`),
    KEY `approvedUserId` (`approvedUserId`),
    CONSTRAINT `group_user_ibfk_group` FOREIGN KEY (`groupId`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `group_user_ibfk_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `group_user-approvedUserId_ibfk_user` FOREIGN KEY (`approvedUserId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `room`
    ADD `groupId` int(11) unsigned NULL DEFAULT NULL,
    ADD KEY `groupId` (`groupId`),
    ADD CONSTRAINT `room_ibfk_group` FOREIGN KEY (`groupId`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

