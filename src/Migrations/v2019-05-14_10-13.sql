ALTER TABLE `message_private`
    ADD `fromDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `created`
    , ADD `toDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `fromDeleted`;
