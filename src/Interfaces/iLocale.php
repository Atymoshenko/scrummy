<?php
namespace App\Interfaces;

interface iLocale {
	public function setLocale(string $locale);
}