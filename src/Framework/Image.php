<?php
namespace App\Framework;

use mysql_xdevapi\Exception;

class Image {
	const TYPE_PATTERN = "~^image/(jpe?g|png)$~m";

	private $filePath;

	public function __construct(string $filePath)
	{
		if (!is_file($filePath)) {
			throw new \Exception('File not found in: ' . htmlspecialchars($filePath) . '.');
		}

		$this->filePath = $filePath;
	}

	public function getSize(): array
	{
		$this->init();

		$size = getimagesize($this->filePath);

		return [
			'width' => $size[0],
			'height' => $size[1],
		];
	}

	public function resize(int $width, int $height, int $quality, string $filePath, array $exifData): void
	{
		$this->init();
		$size = getimagesize($this->filePath);

		preg_match("/^(.*)\/(gif|jpe?g|png)$/i", $size['mime'], $ext);
		switch (strtolower($ext[2]))
		{
			case 'jpg':
			case 'jpeg':
				$im	= imagecreatefromjpeg($this->filePath);
				break;
			case 'gif':
				$im	= imagecreatefromgif($this->filePath);
				break;
			case 'png':
				$im	= imagecreatefrompng($this->filePath);
				break;
			default:
				throw new \Exception('Unknown image format.');
		}
		if (empty($im)) {
			throw new \Exception('Can not create image.');
		}
		if (isset($exifData['Orientation']) && $exifData['Orientation'] != 1) {
			switch($exifData['Orientation']) {
				case 3:
					$im = imagerotate($im, 180, 0);
					break;
				case 6:
					$im = imagerotate($im, 270, 0);
					break;
				case 8:
					$im = imagerotate($im, 90, 0);
					break;
			}
		}
		$x = imagesx($im);
		$y = imagesy($im);
		if ($x > $width || $y > $height) {
			if (($width / $height) < ($x / $y)) {
				$save = imagecreatetruecolor($x / ($x / $width), $y / ($x / $width));
			} else {
				$save = imagecreatetruecolor($x / ($y / $height), $y / ($y / $height));
			}
		} else {
			$save = imagecreatetruecolor($x, $y);
		}
		imagecopyresampled($save, $im, 0, 0, 0, 0, imagesx($save), imagesy($save), $x, $y);
		imagejpeg($save, $filePath, $quality);
		imagedestroy($im);
		imagedestroy($save);
	}

	private function init(): void
	{
		$fileType = mime_content_type($this->filePath);
		if (!preg_match(self::TYPE_PATTERN, $fileType)) {
			throw new Exception('File is not image.');
		}
	}
}