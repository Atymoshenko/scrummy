<?php
namespace App\Framework\Traits;

trait AccessorTrait
{
	protected function getProperty($data, $key, $default = null)
	{
		$type = gettype($data);

		if ($type === 'array') {
			return $data[ $key ] ?? $default;
		}

		throw new \Exception('Unknown type "' . $type . '".');
	}
}