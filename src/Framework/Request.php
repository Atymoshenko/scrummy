<?php
namespace App\Framework;

class Request {
	public function get(string $key, $default = null)
	{
		return $this->getFromRequest($key, $default);
	}

	public function getFromPost(string $key, $default = null)
	{
		return $_POST[$key] ?? $default;
	}

	public function getFromRequest(string $key, $default = null)
	{
		return $_REQUEST[$key] ?? $default;
	}

	public function getForApi(string $key, $default = null, bool $isDev)
	{
		return $isDev ? $this->get($key, $default) : $this->getFromPost($key, $default);
	}

	public function isHttps(): bool
	{
		return (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) === 'on');
	}
}