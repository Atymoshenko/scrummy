<?php
namespace App\Framework\Interfaces;

use App\Framework\{App, Exception\NotFoundException, Response};

interface iPage {
	public function __construct(App $app);

	/**
	 * @throws NotFoundException
	 * @return Response
	 */
	public function run(): Response;
}