<?php
namespace App\Framework\Interfaces;

use App\Framework\App;

interface iValidator {
	public function validate(): void;
}