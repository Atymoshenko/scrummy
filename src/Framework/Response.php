<?php
namespace App\Framework;

class Response
{
	/** @var string */
	private $contentType = 'text/html';
	/** @var string */
	private $charset = 'UTF-8';
	/** @var int */
	private $statusCode;
	/** @var string|array */
	private $content = '';
	/** @var array */
	private $headers = [];
	/** @var array */
	private $statusCodeText = [
		200 => 'OK',
		401 => 'Unauthorized',
		404 => 'Not Found',
		500 => 'Internal Server Error'
	];

	public function __construct(string $content = '', int $statusCode = 200, array $headers = [])
	{
		$this->content = $content;
		$this->statusCode = $statusCode;
		$this->headers = $headers;
	}

	public function getContentType(): string
	{
		return $this->contentType;
	}

	public function setContentType(string $contentType): void
	{
		$this->contentType = $contentType;
	}

	public function getCharset(): string
	{
		return $this->charset;
	}

	public function setCharset(string $charset): void
	{
		$this->charset = $charset;
	}

	public function getStatusCode(): int
	{
		return $this->statusCode;
	}

	public function setStatusCode(int $statusCode): void
	{
		$this->statusCode = $statusCode;
	}

	public function getHeaders(): array
	{
		return $this->headers;
	}

	public function setHeaders(array $headers): void
	{
		$this->headers = $headers;
	}

	public function addHeader(string $header): void
	{
		$this->headers[] = $header;
	}

	public function getStatusCodeText(): string
	{
		return $this->statusCodeText[$this->getStatusCode()];
	}

	/**
	 * @return array|string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param array|string $content
	 */
	public function setContent($content): void
	{
		$this->content = $content;
	}
}