<?php
namespace App\Framework;

class ConfigEntities {
	/** @var ConfigEntity[] */
	private $entities = [];

	public function __construct(array $config)
	{
		foreach ($config as $entityClass => $entityData) {
			$this->entities[$entityClass] = new ConfigEntity($this, $entityClass, $entityData);
		}
		foreach ($this->entities as $configEntity) {
			$configEntity->validateRelations();
		}
	}

	public function getConfigEntityByClass(string $entityClass): ConfigEntity
	{
		if (!array_key_exists($entityClass, $this->entities)) {
			throw new \Exception("Entity '" . htmlspecialchars($entityClass) . "' not found.");
		}

		return $this->entities[$entityClass];
	}
}