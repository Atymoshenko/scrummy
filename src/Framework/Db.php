<?php
namespace App\Framework;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\Message;
use App\Entity\MessagePrivate;
use App\Entity\Photo;
use App\Entity\PhotoData;
use App\Entity\Room;
use App\Entity\User;
use App\Entity\UserFriend;
use App\Entity\UserGoogle;
use App\Entity\UserPasswordReset;
use App\Entity\UserProfile;
use App\Entity\UserProfileComment;
use App\Interfaces\iLocale;
use App\Validator\Entity\GroupFilter;
use App\Validator\Entity\ProfileFilter;
use DateTime;
use Exception;
use mysqli;
use mysqli_result;
use ReflectionClass;

class Db implements iLocale {
	const DB_FORMAT_DATE_TIME = 'Y-m-d H:i:s';
	// const MAX_LIMIT = 18446744073709551615;

	const DB_TYPE_INTEGER = 'int';
	const DB_TYPE_SMALLINT = 'smallint';
	const DB_TYPE_TINYINT = 'tinyint';
	const DB_TYPE_TEXT = 'text';
	const DB_TYPE_VARCHAR = 'varchar';
	const DB_TYPE_CHAR = 'char';
	const DB_TYPE_ENUM = 'enum';
	const DB_TYPE_DATETIME = 'datetime';
	const DB_TYPE_BOOL = 'tinyint';

	const DB_RELATION_TYPE_ONE_TO_ONE = 'OneToOne';
	const DB_RELATION_TYPE_ONE_TO_MANY = 'OneToMany';
	const DB_RELATION_TYPE_MANY_TO_ONE = 'ManyToOne';
	const DB_RELATION_TYPE_MANY_TO_MANY = 'ManyToMany';

	/** @var string|null */
	private $locale;
	/** @var bool */
	private $connected = false;
	/** @var mysqli */
	private $link;
	/** @var string */
	private $host;
	/** @var string */
	private $name;
	/** @var string */
	private $user;
	/** @var string */
	private $password;
	/** @var \ArrayObject */
	private $loadedEntities;
	/** @var ConfigEntities */
	private $configEntities;

	public function __construct(string $host, string $user, string $password, string $name, array $configEntities)
	{
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->name = $name;
		$this->loadedEntities = new \ArrayObject();
		$this->configEntities = new ConfigEntities($configEntities);
	}

	static public function getDbTypes(): array
	{
		return [
			self::DB_TYPE_BOOL,
			self::DB_TYPE_TINYINT,
			self::DB_TYPE_SMALLINT,
			self::DB_TYPE_INTEGER,
			self::DB_TYPE_CHAR,
			self::DB_TYPE_VARCHAR,
			self::DB_TYPE_TEXT,
			self::DB_TYPE_ENUM,
			self::DB_TYPE_DATETIME,
		];
	}

	static public function getDbRelationTypes(): array
	{
		return [
			self::DB_RELATION_TYPE_ONE_TO_ONE,
			self::DB_RELATION_TYPE_ONE_TO_MANY,
			self::DB_RELATION_TYPE_MANY_TO_ONE,
			self::DB_RELATION_TYPE_MANY_TO_MANY,
		];
	}

	public function setLocale(string $locale): void
	{
		$this->locale = $locale;
	}

	private function removeLoadedEntity(object $entity): void
	{
		$this->loadedEntities->offsetUnset(spl_object_hash($entity));
	}

	public function findOneBy(array $criteria, string $class)
	{
		$object = $this->findBy($criteria, $class, 1);

		return $object ? $object : null;
	}

	public function findOneByOrCreate(array $criteria, string $class, object $newEntity = null): object
	{
		$entity = $this->findOneBy($criteria, $class);
		if ($entity) {
			return $entity;
		}

		if (!$newEntity) {
			throw new Exception('newEntity must be object for create new entity.');
		}

		$this->save($newEntity);

		return $newEntity;
	}

	public function findBy(array $criteria = [], string $class, int $limit = null, bool $keyPk = false, int $offset = 0, array $orders = [])
	{
		$sql = "SELECT * FROM `" . $this->getTableByClass($class) . "` " . $this->getSqlByCriteria($criteria);
		if (count($orders) > 0) {
			$sql.= " ORDER BY";
			foreach ($orders as $field => $sort) {
				$sql.= " `{$field}` " . $sort;
			}
		}
		if ($limit) {
			$sql.= ' LIMIT ' . $offset . ', ' . $limit;
		}

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			if ($keyPk) {
				$result[$this->getColPk($row, $class)] = $this->mapArrayToEntity($row, $class);
			} else {
				$result[] = $this->mapArrayToEntity($row, $class);
			}
		}

		return $limit === 1 ? reset($result) : $result;
	}

	public function getUserProfile(int $userId): UserProfile
	{
		/** @var UserProfile|null $userProfile */
		$userProfile = $this->findOneBy(['userId' => $userId], UserProfile::class);
		if (!$userProfile) {
			throw new \Exception("User profile with id {$userId} not found.");
		}

		return $userProfile;
	}

	public function findMessagesNew(User $user, bool $reload = false, int $maxMessagesCount): array
	{
		$sql = "SELECT * FROM `" . $this->getTableByClass(Message::class) . "`";
		$sql.= " WHERE `roomId`=" . $user->getRoomId();
		if ($user->getLastMessageLoad() && !$reload) {
			$sql.= " AND `created`>='" . $user->getLastMessageLoad()->format(self::DB_FORMAT_DATE_TIME) . "'";
			$sql.= " ORDER BY `created` DESC";
		} else {
			$sql.= " ORDER BY `created` DESC LIMIT " . $maxMessagesCount;
		}

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, Message::class);
		}

		return $result;
	}

	public function findMessagesPrivateNew(User $user, bool $reload = false, int $maxMessagesCount): array
	{
		$sql = "SELECT * FROM `" . $this->getTableByClass(MessagePrivate::class) . "`";
		$sql.= " WHERE ((`userId`=" . $user->getId() . " AND `fromDeleted`=0) OR (`userIdTo`=" . $user->getId() . " AND `toDeleted`=0))";
		if ($user->getLastMessageLoad() && !$reload) {
			$sql.= " AND `created`>='" . $user->getLastMessageLoad()->format(self::DB_FORMAT_DATE_TIME) . "'";
		}
		$sql.= " ORDER BY `created` DESC";
		$sql.= " LIMIT " . $maxMessagesCount;

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, MessagePrivate::class);
		}

		return $result;
	}

	public function getCountries(): array
	{
		$sql = "SELECT * FROM `" . $this->getTableByClass(Country::class) . "`";
		$sql.= " ORDER BY `priority` ASC";
		switch ($this->locale) {
			case 'ru':
				$sql.= ", `nameRu` ASC";
				break;
			case 'en':
			default:
				$sql.= ", `nameEn` ASC";
		}

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, Country::class);
		}

		return $result;
	}

	public function getCities(string $countryCode): array
	{
		$sql = "SELECT * FROM `" . $this->getTableByClass(City::class) . "`";
		$sql.= " WHERE `countryCode`='" . $this->real_escape_string($countryCode) . "'";
		$sql.= " ORDER BY `priority` ASC";
		switch ($this->locale) {
			case 'ru':
				$sql.= ", `nameRu` ASC";
				break;
			case 'en':
			default:
				$sql.= ", `nameEn` ASC";
		}

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, City::class);
		}

		return $result;
	}

	public function getMessagesForHomePage(): array
	{
		$sql = "SELECT `m`.`message`,`m`.`created`";
		$sql.= ",`u`.`id` AS `userId`,`u`.`nickname`,`u`.`lastVisit`";
		$sql.= ",`u`.`id` AS `userIdTo`,`uT`.`nickname` AS `nicknameTo`,`uT`.`lastVisit` AS `lastVisitTo`";
		$sql.= " FROM `" . $this->getTableByClass(Message::class) . "` AS `m`";
		$sql.= " INNER JOIN `" . $this->getTableByClass(User::class) . "` AS `u` ON `u`.`id`=`m`.`userId`";
		$sql.= " LEFT JOIN `" . $this->getTableByClass(User::class) . "` AS `uT` ON `uT`.`id`=`m`.`userIdTo`";
		$sql.= " WHERE `m`.`roomId`=(SELECT `id` FROM `room` WHERE `groupId` IS NULL AND `default`=1 LIMIT 1)";
		$sql.= " ORDER BY `m`.`created` DESC LIMIT 10";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $row;
		}

		$result = array_reverse($result);

		return $result;
	}

	public function removeOldMessagesPublic(User $user, int $maxMessagesCount): void
	{
		$tableMessage = $this->getTableByClass(Message::class);

		/** @var Message $message */
		$message = $this->findBy([
			'roomId' => $user->getRoomId(),
		], Message::class, 1, false, $maxMessagesCount, ['created' => 'DESC']);
		if ($message) {
			$sql = "DELETE FROM `{$tableMessage}`
				WHERE `roomId`={$user->getRoomId()}
				AND `created`<='{$message->getCreated()->format(self::DB_FORMAT_DATE_TIME)}'
			";
			$this->query($sql);
		}
	}

	public function removeOldMessagesPrivate(User $user, int $maxMessagesCount): void
	{
		$tableMessage = $this->getTableByClass(MessagePrivate::class);

		/** @var MessagePrivate $messagePrivate */
		$messagePrivate = $this->findBy([
			'userId' => $user->getId(),
			'fromDeleted' => false,
		], MessagePrivate::class, 1, false, $maxMessagesCount, ['created' => 'DESC']);
		if ($messagePrivate) {
			$sql = "UPDATE `{$tableMessage}`
				SET `fromDeleted`=1
				WHERE `userId`={$user->getId()}
				AND `created`<='{$messagePrivate->getCreated()->format(self::DB_FORMAT_DATE_TIME)}'
				AND `fromDeleted`=0
			";
			$this->query($sql);
		}

		$messagePrivate = $this->findBy([
			'userIdTo' => $user->getId(),
			'toDeleted' => false,
		], MessagePrivate::class, 1, false, $maxMessagesCount, ['created' => 'DESC']);
		if ($messagePrivate) {
			$sql = "UPDATE `{$tableMessage}`
				SET `toDeleted`=1
				WHERE `userIdTo`={$user->getId()}
				AND `created`<='{$messagePrivate->getCreated()->format(self::DB_FORMAT_DATE_TIME)}'
				AND `toDeleted`=0
			";
			$this->query($sql);
		}

		$this->query("DELETE FROM `{$tableMessage}` WHERE `userId`={$user->getId()} AND `fromDeleted`=1 AND `toDeleted`=1");
		$this->query("DELETE FROM `{$tableMessage}` WHERE `userIdTo`={$user->getId()} AND `fromDeleted`=1 AND `toDeleted`=1");
	}

	public function findChatUserList(User $user): array
	{
		$dateLastVisitCondition = new \DateTime('now - 1 minute');
		$sql = "SELECT `u`.* FROM `" . $this->getTableByClass(User::class) . "` AS `u`";
		$sql.= " LEFT JOIN `user_block` AS `ub` ON (`ub`.`userId`={$user->getId()} AND `ub`.`userIdBlocked`=`u`.`id`)";
		$sql.= " WHERE `u`.`lastVisit`>='" . $dateLastVisitCondition->format(self::DB_FORMAT_DATE_TIME) . "'";
		$sql.= " AND `u`.`roomId`=" . $user->getRoomId();
		$sql.= " AND `u`.`apiToken` IS NOT NULL";
		$sql.= " AND `ub`.`userId` IS NULL";

		$result = [];
		$r = $this->query($sql);
		$userIds = [];
		while ($row = $this->fetch_assoc($r)) {
			$result[$row['id']] = $this->mapArrayToEntity($row, User::class);
			$userIds[$row['id']] = $row['id'];
		}

		if (count($userIds)) {
			$sql = "SELECT `userId`,`userIdFriend` FROM `" . $this->configEntities->getConfigEntityByClass(UserFriend::class)->getTableName() . "`";
			$sql.= " WHERE ((`userId`={$user->getId()} AND `userIdFriend` IN(" . implode(',', $userIds) . "))";
			$sql.= " OR (`userId` IN(" . implode(',', $userIds) . ") AND `userIdFriend`={$user->getId()}))";
			$sql.= " AND `approvedAt` IS NOT NULL";
			$r = $this->query($sql);
			while ($row = $this->fetch_assoc($r)) {
				$userId = isset($result[$row['userId']]) ? $row['userId'] : (isset($result[$row['userIdFriend']]) ? $row['userIdFriend'] : null);
				if ($userId) {
					$result[$userId]->setFriend(true);
				}
			}
		}

		return $result;
	}

	public function findChatRoomList(User $user): array
	{
		$sql = "SELECT `r`.* FROM `" . $this->getTableByClass(Room::class) . "` AS `r`
			WHERE `r`.`groupId` IS NULL
				OR `r`.`groupId` IN (SELECT `id` FROM `group` WHERE `userId`={$user->getId()})
			OR `r`.`groupId` IN (
					SELECT `gu`.`groupId`
				FROM
					 `group_user` AS `gu`
				INNER JOIN `group` AS `g`
					ON `g`.`id`=`gu`.`groupId`
				WHERE
					 `gu`.`userId`={$user->getId()} AND (`g`.`private`=0 OR `gu`.`approved`=1)
			)
		";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, Room::class);
		}

		return $result;
	}

	public function findGroupUserRequests(Group $group): array
	{
		$sql = "SELECT `u`.* FROM `" . $this->getTableByClass(User::class) . "` AS `u`";
		$sql.= " INNER JOIN `" . $this->getTableByClass(GroupUser::class) . "` AS `gu`";
		$sql.= " ON `gu`.`userId`=`u`.`id`";
		$sql.= " WHERE `gu`.`groupId`=" . $group->getId();
		$sql.= " AND `gu`.`approved`=0";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, User::class);
		}

		return $result;
	}

	public function save(object $entity): void
	{
		$entityClass = get_class($entity);
		$entityConfig = $this->configEntities->getConfigEntityByClass(get_class($entity));

		$reflector = new \ReflectionClass($entity);

		foreach ($entityConfig->getFields() as $fieldName => $fieldData) {
			if (!$reflector->hasProperty($fieldName)) {
				throw new \Exception('Field ' . htmlspecialchars($fieldName) . ' not found in class ' . $entityClass . '.');
			}
			$property = $reflector->getProperty($fieldName);
			$property->setAccessible(true);
			if ($property->isStatic()) {
				throw new \Exception('Field ' . htmlspecialchars($fieldName) . ' must be not static in class ' . $entityClass . '.');
			}
		}

		$isNew = $this->isNew($entity);
		foreach ($entityConfig->getPrimaryFieldNames() as $primaryFieldName) {
			$property = $reflector->getProperty($primaryFieldName);
			$property->setAccessible(true);
			$value = $property->getValue($entity);
			if (!is_null($value) && (!is_int($value) && !is_string($value))) {
				throw new \Exception("Supported only integer or string primary field. (" . $entityClass . "), " . gettype($value) . " given.");
			}
			if (!$isNew && is_null($value)) {
				throw new \Exception("Incorrectly overwritten one or more of the primary keys in " . $entityClass . '.');
			}
		}

		foreach ($entityConfig->getPreSaveMethodNames() as $preSaveMethodName) {
			if (!method_exists($entity, $preSaveMethodName)) {
				throw new \Exception("Method '" . htmlspecialchars($preSaveMethodName) . "' not exists in '{$entityClass}.");
			}
			$entity->$preSaveMethodName();
		}

		$dataForSave = [];
		/**
		 * @var string $fieldName
		 * @var ConfigEntityField $configEntityField
		 */
		foreach ($entityConfig->getFields() as $fieldName => $configEntityField) {
			$property = $reflector->getProperty($fieldName);
			$property->setAccessible(true);
			$value = $property->getValue($entity);

			if (is_null($value)) {
				if ($isNew && in_array($fieldName, $entityConfig->getPrimaryFieldNames())) {
					continue;
				}
				if (!$configEntityField->isNullable()) {
					throw new \Exception("Field '" . htmlspecialchars($fieldName) . "' can not be null in " . $entityClass . ".");
				}
				$dataForSave[] = "`" . $fieldName . "`=NULL";
			} else {
				switch ($configEntityField->getType()) {
					case Db::DB_TYPE_BOOL:
						$dataForSave[] = "`" . $fieldName . "`=" . ($value ? 1 : 0);
						break;
					case Db::DB_TYPE_SMALLINT:
					case Db::DB_TYPE_TINYINT:
					case Db::DB_TYPE_INTEGER:
						$dataForSave[] = "`" . $fieldName . "`=" . (int)$value;
						break;
					case Db::DB_TYPE_CHAR:
					case Db::DB_TYPE_VARCHAR:
					case Db::DB_TYPE_TEXT:
						$dataForSave[] = "`" . $fieldName . "`='" . $this->real_escape_string($value) . "'";
						break;
					case Db::DB_TYPE_DATETIME:
						$dataForSave[] = "`" . $fieldName . "`='" . $value->format(self::DB_FORMAT_DATE_TIME) . "'";
						break;
					case Db::DB_TYPE_ENUM:
						if (!in_array($value, $configEntityField->getEnumChoices())) {
							throw new \Exception("Unknown choice in enum field " . htmlspecialchars($fieldName) . " in " . $entityClass . ".");
						}
						$dataForSave[] = "`" . $fieldName . "`='" . $this->real_escape_string($value) . "'";
						break;
					default:
						throw new \Exception("Unknown field type '" . htmlspecialchars($configEntityField->getType()) . "' in " . $entityClass . ".");
				}
			}
		}

		if ($isNew) {
			$sql = "INSERT INTO `" . $entityConfig->getTableName() . "` SET ";
			$sql.= implode(', ' , $dataForSave);
			$this->query($sql);
			if ($entityConfig->getAutoIncrementFieldName()) {
				$propertyAutoIncrement = $reflector->getProperty($entityConfig->getAutoIncrementFieldName());
				$propertyAutoIncrement->setAccessible(true);
				$propertyAutoIncrement->setValue($entity, $this->insert_id());
			}
			$this->loadedEntities->offsetSet(spl_object_hash($entity), ['entity' => $entity]);
		} else {
			$sqlUpdateConditions = [];
			foreach ($entityConfig->getPrimaryFieldNames() as $primaryFieldName) {
				$propertyPrimary = $reflector->getProperty($primaryFieldName);
				$propertyPrimary->setAccessible(true);
				$valuePrimary = $propertyPrimary->getValue($entity);
				$sqlUpdateConditions[] = "`{$primaryFieldName}`=" . (is_int($valuePrimary) ? $valuePrimary : ("'" . $this->real_escape_string($valuePrimary) . "'"));
			}
			$sql = "UPDATE `" . $entityConfig->getTableName() . "` SET ";
			$sql.= implode(', ' , $dataForSave);
			$sql.= " WHERE " . implode(' AND ' , $sqlUpdateConditions);
			$this->query($sql);
		}
	}

	public function remove(object $entity): void
	{
		$entityClass = get_class($entity);
		$entityConfig = $this->configEntities->getConfigEntityByClass(get_class($entity));

		$reflector = new \ReflectionClass($entity);

		if ($this->isNew($entity)) {
			throw new \Exception("Can't remove new entity {$entityClass}.");
		}
		foreach ($entityConfig->getPrimaryFieldNames() as $primaryFieldName) {
			$property = $reflector->getProperty($primaryFieldName);
			$property->setAccessible(true);
			$value = $property->getValue($entity);
			if (is_null($value)) {
				throw new \Exception("Can't remove entity, when primary key is null in {$entityClass}.");
			}
		}

		$sqlDeleteConditions = [];
		foreach ($entityConfig->getPrimaryFieldNames() as $primaryFieldName) {
			$propertyPrimary = $reflector->getProperty($primaryFieldName);
			$propertyPrimary->setAccessible(true);
			$valuePrimary = $propertyPrimary->getValue($entity);
			if (is_null($valuePrimary)) {
				throw new \Exception("Can't remove entity, when primary key {$primaryFieldName} is null in {$entityClass}.");
			}
			$sqlDeleteConditions[] = "`{$primaryFieldName}`=" . (is_int($valuePrimary) ? $valuePrimary : ("'" . $this->real_escape_string($valuePrimary) . "'"));
		}
		$sql = "DELETE FROM `" . $entityConfig->getTableName() . "` WHERE " . implode(' AND ' , $sqlDeleteConditions);
		$this->query($sql);
		$this->removeLoadedEntity($entity);
	}

	public function beginTransaction(): bool
	{
		$this->connect();

		return mysqli_begin_transaction($this->link);
	}

	public function commit(): bool
	{
		$this->connect();

		return mysqli_commit($this->link);
	}

	public function rollback(): bool
	{
		$this->connect();

		return mysqli_rollback($this->link);
	}

	public function lockTable(string $class, string $mode): void
	{
		if (!in_array(strtoupper($mode), ['READ','READ LOCAL','LOW PRIORITY WRITE','WRITE'])) {
			throw new \Exception('Wrong mode ' . htmlspecialchars($mode) . '.');
		}

		$this->connect();

		$table = $this->getTableByClass($class);
		$this->query('LOCK TABLE `' . $table . '` ' . $mode);
	}

	public function unlockTables(): void
	{
		$this->connect();

		$this->query('UNLOCK TABLES');
	}

	public function isNew(object $entity): bool
	{
		return !$this->loadedEntities->offsetExists(spl_object_hash($entity));
	}

	private function connect(): void
	{
		if ($this->connected) {
			return;
		}

		$this->link = @mysqli_connect($this->host, $this->user, $this->password, $this->name);
		if (mysqli_connect_error()) {
			throw new Exception('DB connect error');
		}
		$this->connected = true;
		$this->query('set names `utf8`');
	}

	private function query(string $sql)
	{
		$this->connect();
		$r = mysqli_query($this->link, $sql);
		if (!$r) {
			throw new Exception(mysqli_error($this->link) . " SQL: " . htmlspecialchars($sql));
		}

		return $r;
	}

	private function insert_id() {
		$this->connect();

		return mysqli_insert_id($this->link);
	}

	private function fetch_assoc(mysqli_result $r):? array
	{
		return mysqli_fetch_assoc($r);
	}

	private function real_escape_string(string $data): string
	{
		$this->connect();
		return mysqli_real_escape_string($this->link, $data);
	}

	private function getTableByClass(string $class): string
	{
		return $this->configEntities->getConfigEntityByClass($class)->getTableName();
	}

	private function getSqlByCriteria(array $criteria): string
	{
		$sql = '';
		$number = 0;
		foreach ($criteria as $field => $value) {
			$number++;
			$sql.= ($number > 1 ? ' AND' : '') . ' `' . $field . '`';
			if (is_integer($value)) {
				$sql.= '=' . (string)$value;
			} elseif (is_string($value)) {
				$sql.= "='" . $this->real_escape_string($value) . "'";
			} elseif (is_bool($value)) {
				$sql.= "=" . ($value ? 1 : 0);
			} elseif (is_array($value)) {
				$sql.= " IN(" . implode(',', $this->quoteArray($value)) . ")";
			} elseif (is_null($value)) {
				$sql.= " IS NULL";
			} else {
				throw new Exception('Unknown criteria type ' . gettype($value) . '.');
			}
		}

		return $number > 0 ? 'WHERE' . $sql : $sql;
	}

	private function quoteArray(array $data): array
	{
		foreach ($data as $key => $val) {
			$type = gettype($val);
			switch ($type) {
				case 'string':
					$data[$key] = "'" . $this->real_escape_string($val) . "'";
					break;
				case 'integer':
					break;
				default:
					throw new Exception('Type ' . $type . ' can not be quoted.');
			}
		}

		return $data;
	}

	private function mapArrayToEntity(array $data, string $class): object
	{
		$configEntity = $this->configEntities->getConfigEntityByClass($class);
		$reflector = new ReflectionClass($class);
		$entity = $reflector->newInstanceWithoutConstructor();

		/**
		 * @var string $fieldName
		 * @var ConfigEntityField $configEntityField
		 */
		foreach ($configEntity->getFields() as $fieldName => $configEntityField) {
			if (!array_key_exists($fieldName, $data)) {
				throw new \Exception("No value for field '{$fieldName}' of entity {$class}.");
			}
			if (!$reflector->hasProperty($fieldName)) {
				throw new \Exception('Field ' . htmlspecialchars($fieldName) . ' not found in class ' . $class . '.');
			}
			$property = $reflector->getProperty($fieldName);
			if ($property->isStatic()) {
				throw new \Exception('Field ' . htmlspecialchars($fieldName) . ' must be not static in class ' . $class . '.');
			}
			$property->setAccessible(true);

			if (is_null($data[$fieldName])) {
				$property->setValue($entity, null);
			} else {
				switch ($configEntityField->getType()) {
					case Db::DB_TYPE_BOOL:
						$property->setValue($entity, (bool) $data[$fieldName]);
						break;
					case Db::DB_TYPE_SMALLINT:
					case Db::DB_TYPE_TINYINT:
					case Db::DB_TYPE_INTEGER:
						$property->setValue($entity, (int) $data[$fieldName]);
						break;
					case Db::DB_TYPE_CHAR:
					case Db::DB_TYPE_VARCHAR:
					case Db::DB_TYPE_TEXT:
					case Db::DB_TYPE_ENUM:
						$property->setValue($entity, (string) $data[$fieldName]);
						break;
					case Db::DB_TYPE_DATETIME:
						$property->setValue($entity, DateTime::createFromFormat(self::DB_FORMAT_DATE_TIME, $data[$fieldName]));
						break;
					default:
						throw new \Exception("Unknown field type '" . $configEntityField->getType() . "' in " . $class . ".");
				}
			}
			// @TODO: BEGIN Refactor this:
			if (in_array($configEntity->getTableName(), ['country','city'])) {
				$entity->setLocale($this->locale);
			}
			// @TODO: ENDRefactor this.
		}

		$this->loadedEntities->offsetSet(spl_object_hash($entity), ['entity' => $entity]);

		return $entity;
	}

	private function getColPk(array $data, string $class)
	{
		switch ($class) {
			case User::class:
			case Room::class:
			case City::class:
			case Group::class:
			case Photo::class:
				return $data['id'];
				break;
			case UserProfile::class:
			case UserPasswordReset::class:
			case UserGoogle::class:
				return $data['userId'];
			case Message::class:
			case MessagePrivate::class:
				return $data['hash'];
				break;
			case Country::class:
				return $data['code'];
				break;
			case PhotoData::class:
				return $data['photoId'];
				break;
			default:
				throw new Exception('Unknown class: ' . htmlspecialchars($class) . '.');
		}
	}

	public function findUsers(ProfileFilter $profileFilter, int $offset, int $limit): array
	{
		$sql = "SELECT `u`.`id`,`u`.`nickname`,`u`.`sex`,`up`.`name`,`up`.`surname`,`p`.`id` AS `photoId` FROM " . $this->configEntities->getConfigEntityByClass(User::class)->getTableName() . " AS `u`";
		$sql.= " INNER JOIN `" . $this->configEntities->getConfigEntityByClass(UserProfile::class)->getTableName() . "` AS `up` ON `u`.`id`=`up`.`userId`";
		$sql.= " LEFT JOIN `" . $this->configEntities->getConfigEntityByClass(Photo::class)->getTableName() . "` AS `p` ON `p`.`userId`=`u`.`id`";
		if ($profileFilter->getCountryCode() || $profileFilter->getCityId()) {
			$sql.= " WHERE ";
			$sql.= " `up`.`countryCode`='" . $this->real_escape_string($profileFilter->getCountryCode()) . "'";
			$sql.= " AND `up`.`cityId`=" . $profileFilter->getCityId();
		}
		$sql.= " GROUP BY `u`.`id` LIMIT {$offset}, {$limit}";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $row;
		}

		return $result;
	}

	public function getUserProfileComments(int $userId, int $offset, int $limit): array
	{
		$sql = "SELECT `upc`.`id`,`upc`.`userId`,`upc`.`userIdComment`,`upc`.`createdAt`,`upc`.`content`,`u`.`nickname`,`p`.`id` AS `photoId`,`u`.`sex` FROM " . $this->configEntities->getConfigEntityByClass(UserProfileComment::class)->getTableName() . " AS `upc`";
		$sql.= " INNER JOIN `" . $this->configEntities->getConfigEntityByClass(User::class)->getTableName() . "` AS `u` ON `upc`.`userIdComment`=`u`.`id`";
		$sql.= " INNER JOIN `" . $this->configEntities->getConfigEntityByClass(UserProfile::class)->getTableName() . "` AS `up` ON `upc`.`userIdComment`=`up`.`userId`";
		$sql.= " LEFT JOIN `" . $this->configEntities->getConfigEntityByClass(Photo::class)->getTableName() . "` AS `p` ON `p`.`userId`=`up`.`userId`";
		$sql.= " WHERE ";
		$sql.= " `upc`.`userId`=" . $userId;
		$sql.= " ORDER BY `upc`.`id` DESC";
		$sql.= " LIMIT {$offset}, {$limit}";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $row;
		}

		return $result;
	}

	public function findGroups(GroupFilter $groupFilter): array
	{
		$sql = "SELECT `g`.* FROM `group` AS `g`";
		$sql.= " INNER JOIN `user_profile` AS `up` ON `g`.`userId`=`up`.`userId`";
		$sql.= " WHERE ";
		$sql.= " `up`.`countryCode`='" . $this->real_escape_string($groupFilter->getCountryCode()) . "'";
		$sql.= " AND `up`.`cityId`=" . $groupFilter->getCityId();

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, Group::class);
		}

		return $result;
	}

	public function getTopGroups(): array
	{
		$sql = "SELECT `gu`.`groupId`, COUNT(`gu`.`groupId`) AS `count`, `g`.* FROM `group_user` AS `gu`";
		$sql.= " INNER JOIN `group` AS `g` ON `g`.`id`=`gu`.`groupId`";
		$sql.= " GROUP BY `gu`.`groupId` ORDER BY `count` DESC LIMIT 10";

		$result = [];
		$r = $this->query($sql);
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $this->mapArrayToEntity($row, Group::class);
		}

		return $result;
	}

	public function getUserFriend(User $user, User $user2): ?UserFriend
	{
		$sql = "SELECT * FROM `" . $this->configEntities->getConfigEntityByClass(UserFriend::class)->getTableName() . "`
			WHERE (`userId`={$user->getId()} AND `userIdFriend`={$user2->getId()})
			OR (`userId`={$user2->getId()} AND `userIdFriend`={$user->getId()})";
		$this->query($sql);

		$r = $this->query($sql);
		$row = $this->fetch_assoc($r);
		if (!$row) {
			return null;
		}
		/** @var UserFriend|null $userFriend */
		$userFriend = $this->mapArrayToEntity($row, UserFriend::class);

		return $userFriend;
	}

	public function removeUserFriend(UserFriend $userFriend): void
	{
		$sql = "DELETE FROM `" . $this->configEntities->getConfigEntityByClass(UserFriend::class)->getTableName() . "`
			WHERE (`userId`={$userFriend->getUserId()} AND `userIdFriend`={$userFriend->getUserIdFriend()})
			OR (`userId`={$userFriend->getUserIdFriend()} AND `userIdFriend`={$userFriend->getUserId()})";
		$this->query($sql);

		$this->removeLoadedEntity($userFriend);
	}

	public function incrementUserProfileLike(int $userId): void
	{
		$sql = "UPDATE `" . $this->configEntities->getConfigEntityByClass(UserProfile::class)->getTableName() . "` SET `likeCount`=`likeCount`+1 WHERE `userId`=" . $userId;
		$this->query($sql);
	}

	public function decrementUserProfileLike(int $userId): void
	{
		$sql = "UPDATE `" . $this->configEntities->getConfigEntityByClass(UserProfile::class)->getTableName() . "` SET `likeCount`=`likeCount`-1 WHERE `userId`=" . $userId;
		$this->query($sql);
	}
}