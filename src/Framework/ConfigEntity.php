<?php
namespace App\Framework;

class ConfigEntity {
	/** @var ConfigEntities */
	private $configEntities;
	/** @var string */
	private $entityclass;
	/** @var string */
	private $tableName;
	/** @var ConfigEntityField[] */
	private $fields = [];
	/** @var array */
	private $primaryFieldNames = [];
	/** @var string|null */
	private $autoIncrementFieldName;
	/** @var array */
	private $keys = [];
	/** @var array */
	private $uniqueKeys = [];
	/** @var string[] */
	private $preSaveMethodNames = [];

	public function __construct(ConfigEntities $configEntities, string $entityClass, array $config)
	{
		$this->configEntities = $configEntities;

		if (!class_exists($entityClass)) {
			throw new \Exception('Class ' . htmlspecialchars($entityClass) . ' not found.');
		}
		$this->entityclass = $entityClass;

		if (!isset($config['tableName'])) {
			throw new \Exception('No table name in ' . $entityClass . '.');
		}
		if (!preg_match("~^[a-z\-_0-9]{1,64}$~", $config['tableName'])) {
			throw new \Exception('Invalid table name in ' . $entityClass . '.');
		}
		$this->tableName = $config['tableName'];

		if (!isset($config['field'])) {
			throw new \Exception('Table must contain at least one field in ' . $entityClass . '.');
		}
		if (!is_array($config['field'])) {
			throw new \Exception('Array key field must be array in ' . $entityClass . ', ' . gettype($config['field']) . ' given.');
		}

		foreach ($config['field'] as $fieldName => $fieldData) {
			$this->fields[$fieldName] = new ConfigEntityField($this, $fieldName, $fieldData);
		}

		if (isset($config['primary'])) {
			if (!is_array($config['primary'])) {
				throw new \Exception("Property primary must be array, " . gettype($config['primary']) . " given in {$entityClass}.");
			}
			foreach ($config['primary'] as $primaryFieldName) {
				if (!is_string($primaryFieldName)) {
					throw new \Exception("Primary fields list must have type string, " . gettype($primaryFieldName) . " given in {$entityClass}.");
				}
				if (!array_key_exists($primaryFieldName, $this->getFields())) {
					throw new \Exception("No field '" . htmlspecialchars($primaryFieldName) . "' in {$entityClass}.");
				}
			}
			$this->primaryFieldNames = $config['primary'];
		}

		if (isset($config['autoIncrement'])) {
			$this->setAutoIncrementFieldName($config['autoIncrement']);
			if (!array_key_exists($config['autoIncrement'], $this->getFields())) {
				throw new \Exception("No field '" . htmlspecialchars($this->getAutoIncrementFieldName()) . "' in {$entityClass}.");
			}
		}

		if (isset($config['key'])) {
			if (!is_array($config['key'])) {
				throw new \Exception("Property key must be array, " . gettype($config['key']) . " given in {$entityClass}.");
			}
			foreach ($config['key'] as $keyName => $keyFieldNames) {
				if (!is_array($keyFieldNames)) {
					throw new \Exception("Field list for key must have type array, " . gettype($keyFieldNames) . " given in {$entityClass}.");
				}
				foreach ($keyFieldNames as $keyFieldName) {
					if (!is_string($keyFieldName)) {
						throw new \Exception("Field name in keys list must have type string, " . gettype($keyFieldName) . " given in {$entityClass}.");
					}
					if (!array_key_exists($keyFieldName, $this->getFields())) {
						throw new \Exception("No field '" . htmlspecialchars($keyFieldName) . "' in {$entityClass}.");
					}
				}
			}
			$this->keys = $config['key'];
		}

		if (isset($config['unique'])) {
			if (!is_array($config['unique'])) {
				throw new \Exception("Property unique must be array, " . gettype($config['unique']) . " given in {$entityClass}.");
			}
			foreach ($config['unique'] as $uniqueName => $uniqueFieldNames) {
				if (!is_array($uniqueFieldNames)) {
					throw new \Exception("Field list for unique key must have type array, " . gettype($uniqueFieldNames) . " given in {$entityClass}.");
				}
				foreach ($uniqueFieldNames as $uniqueFieldName) {
					if (!is_string($uniqueFieldName)) {
						throw new \Exception("Field name in unique keys list must have type string, " . gettype($uniqueFieldName) . " given in {$entityClass}.");
					}
					if (!array_key_exists($uniqueFieldName, $this->getFields())) {
						throw new \Exception("No field '" . htmlspecialchars($uniqueFieldName) . "' in {$entityClass}.");
					}
				}
			}
			$this->uniqueKeys = $config['unique'];
		}

		if (isset($config['preSaveMethodNames'])) {
			if (!is_array($config['preSaveMethodNames'])) {
				throw new \Exception("Property preSaveMethodNames must be array, " . gettype($config['preSaveMethodNames']) . " given in {$entityClass}.");
			}
			foreach ($config['preSaveMethodNames'] as $preSaveMethodName) {
				if (!is_string($preSaveMethodName)) {
					throw new \Exception("Method for pre save must be type string, " . gettype($preSaveMethodName) . " given in {$entityClass}.");
				}
				$this->preSaveMethodNames[] = $preSaveMethodName;
			}
		}
	}

	public function getConfigEntities(): ConfigEntities
	{
		return $this->configEntities;
	}

	public function getTableName(): string
	{
		return $this->tableName;
	}

	public function getFields(): array
	{
		return $this->fields;
	}

	public function getPrimaryFieldNames(): array
	{
		return $this->primaryFieldNames;
	}

	public function getAutoIncrementFieldName(): ?string
	{
		return $this->autoIncrementFieldName;
	}

	public function setAutoIncrementFieldName(string $autoIncrementFieldName): void
	{
		$this->autoIncrementFieldName = $autoIncrementFieldName;
	}

	public function getPreSaveMethodNames(): array
	{
		return $this->preSaveMethodNames;
	}

	public function validateRelations(): void
	{
		foreach ($this->getFields() as $configEntityField) {
			$configEntityField->validateRelations();
		}
	}
}