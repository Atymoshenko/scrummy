<?php
namespace App\Framework;

use App\Api\Api;
use App\Api\AuthApi;
use App\Api\ChatApi;
use App\Api\CommentApi;
use App\Api\GroupApi;
use App\Api\LikeApi;
use App\Api\ProfileApi;
use App\Api\UserApi;
use App\Framework\ {
	Traits\AccessorTrait
};
use App\Page\{IndexPage,
	ChatPage,
	PasswordForgotConfirmPage,
	PasswordForgotPage,
	Profile\ProfileEditPhotoPage,
	Profile\ProfilesPage,
	ProfilePage,
	SignupPage,
	SigninPage,
	ProfileEditConfirmPage,
	ProfileEditProfilePage,
	ProfileEditPasswordPage,
	StaticPage};
use App\Page\Group\{
	GroupChatPage,
	GroupPage,
	GroupsPage,
	GroupAddPage,
	GroupEditPage
};

class Route
{
	use AccessorTrait;

	/** @var string */
	private $protocol = 'http';
	private $request;
	private $routes = [
		'' => ['path' => '', 'class' => IndexPage::class, 'enable' => true],
		'apiSignUp' => ['path' => Api::PATH_PREFIX . 'signup', 'method' => 'methodSignUp', 'class' => AuthApi::class, 'enable' => true],
		'apiSignIn' => ['path' => Api::PATH_PREFIX . 'signin', 'method' => 'methodSignIn', 'class' => AuthApi::class, 'enable' => true],
		'apiSignInGoogle' => ['path' => Api::PATH_PREFIX . 'signin/google', 'method' => 'methodSignInGoogle', 'class' => AuthApi::class, 'enable' => true],
		'apiSignOut' => ['path' => Api::PATH_PREFIX . 'signout', 'method' => 'methodSignOut', 'class' => AuthApi::class, 'enable' => true],
		'apiSignUpConfirm' => ['path' => Api::PATH_PREFIX . 'signup/confirm', 'method' => 'methodSignUpConfirm', 'class' => AuthApi::class, 'enable' => true],
		'apiCheckUser' => ['path' => Api::PATH_PREFIX . 'checkUser', 'method' => 'methodCheckUser', 'class' => Api::class, 'enable' => true],
		'apiUserFriendAdd' => ['path' => Api::PATH_PREFIX . 'user/{userId}/friend/add', 'method' => 'methodFriendAdd', 'class' => UserApi::class, 'enable' => true, 'auth' => true],
		'apiUserFriendRemove' => ['path' => Api::PATH_PREFIX . 'user/{userId}/friend/remove', 'method' => 'methodFriendRemove', 'class' => UserApi::class, 'enable' => true, 'auth' => true],
		'apiUserBlockAdd' => ['path' => Api::PATH_PREFIX . 'user/{userId}/block/add', 'method' => 'methodBlockAdd', 'class' => UserApi::class, 'enable' => true, 'auth' => true],
		'apiUserBlockRemove' => ['path' => Api::PATH_PREFIX . 'user/{userId}/block/remove', 'method' => 'methodBlockRemove', 'class' => UserApi::class, 'enable' => true, 'auth' => true],
		'apiProfileEditProfile' => ['path' => Api::PATH_PREFIX . 'profile/edit/profile', 'method' => 'methodProfileEditProfile', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiProfileEditPassword' => ['path' => Api::PATH_PREFIX . 'profile/edit/password', 'method' => 'methodProfileEditPassword', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiProfilePhotos' => ['path' => Api::PATH_PREFIX . 'profile/photos', 'method' => 'methodProfilePhotos', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiProfilePhotoUpload' => ['path' => Api::PATH_PREFIX . 'profile/photo/upload', 'method' => 'methodProfilePhotoUpload', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiProfilePhotoDelete' => ['path' => Api::PATH_PREFIX . 'profile/photo/{photoId}/delete', 'method' => 'methodProfilePhotoDelete', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiProfilePasswordForgot' => ['path' => Api::PATH_PREFIX . 'profile/password/forgot', 'method' => 'methodProfilePasswordForgot', 'class' => ProfileApi::class, 'enable' => true],
		'apiProfilePasswordReset' => ['path' => Api::PATH_PREFIX . 'profile/password/reset', 'method' => 'methodProfilePasswordReset', 'class' => ProfileApi::class, 'enable' => true],
		'apiProfiles' => ['path' => Api::PATH_PREFIX . 'profiles', 'method' => 'methodProfiles', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiComments' => ['path' => Api::PATH_PREFIX . 'comments', 'method' => 'methodComments', 'class' => CommentApi::class, 'enable' => true, 'auth' => true],
		'apiCommentCreate' => ['path' => Api::PATH_PREFIX . 'comment/create', 'method' => 'methodCommentCreate', 'class' => CommentApi::class, 'enable' => true, 'auth' => true],
		'apiCommentRemove' => ['path' => Api::PATH_PREFIX . 'comment/{commentId}/remove', 'method' => 'methodCommentRemove', 'class' => CommentApi::class, 'enable' => true, 'auth' => true],
		'apiCommentUpdate' => ['path' => Api::PATH_PREFIX . 'comment/{commentId}/update', 'method' => 'methodCommentUpdate', 'class' => CommentApi::class, 'enable' => true, 'auth' => true],
		'apiLikesCount' => ['path' => Api::PATH_PREFIX . 'likes/count', 'method' => 'methodLikesCount', 'class' => LikeApi::class, 'enable' => true, 'auth' => true],
		'apiLikesCreate' => ['path' => Api::PATH_PREFIX . 'like/create', 'method' => 'methodLikeCreate', 'class' => LikeApi::class, 'enable' => true, 'auth' => true],
		'apiLikesRemove' => ['path' => Api::PATH_PREFIX . 'like/remove', 'method' => 'methodLikeRemove', 'class' => LikeApi::class, 'enable' => true, 'auth' => true],
		'apiUserPhotos' => ['path' => Api::PATH_PREFIX . 'user/{id}/photos', 'method' => 'methodPhotos', 'class' => ProfileApi::class, 'enable' => true, 'auth' => true],
		'apiCities' => ['path' => Api::PATH_PREFIX . 'cities', 'method' => 'methodCities', 'class' => Api::class, 'enable' => true],
		'apiChatUsers' => ['path' => Api::PATH_PREFIX . 'chat/users', 'method' => 'methodChatUsers', 'class' => ChatApi::class, 'enable' => true, 'auth' => true],
		'apiChatMessages' => ['path' => Api::PATH_PREFIX . 'chat/messages', 'method' => 'methodChatMessages', 'class' => ChatApi::class, 'enable' => true, 'auth' => true],
		'apiChatMessage' => ['path' => Api::PATH_PREFIX . 'chat/message', 'method' => 'methodChatMessage', 'class' => ChatApi::class, 'enable' => true, 'auth' => true],
		'apiChatRooms' => ['path' => Api::PATH_PREFIX . 'chat/rooms', 'method' => 'methodChatRooms', 'class' => ChatApi::class, 'enable' => true, 'auth' => true],
		'apiChatRoomSwitch' => ['path' => Api::PATH_PREFIX . 'chat/room/switch', 'method' => 'methodChatRoomSwitch', 'class' => ChatApi::class, 'enable' => true, 'auth' => true],
		'apiGroups' => ['path' => Api::PATH_PREFIX . 'groups', 'method' => 'methodGroups', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupsMy' => ['path' => Api::PATH_PREFIX . 'groups/my', 'method' => 'methodGroupsMy', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupCreateForm' => ['path' => Api::PATH_PREFIX . 'group/create/form', 'method' => 'methodGroupCreateForm', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupCreate' => ['path' => Api::PATH_PREFIX . 'group/create', 'method' => 'methodGroupCreate', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupEdit' => ['path' => Api::PATH_PREFIX . 'group/edit', 'method' => 'methodGroupEdit', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupJoin' => ['path' => Api::PATH_PREFIX . 'group/join', 'method' => 'methodGroupJoin', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupLeave' => ['path' => Api::PATH_PREFIX . 'group/leave', 'method' => 'methodGroupLeave', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupRequestAccept' => ['path' => Api::PATH_PREFIX . 'group/request/accept', 'method' => 'methodGroupRequestAccept', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'apiGroupRequestDecline' => ['path' => Api::PATH_PREFIX . 'group/request/decline', 'method' => 'methodGroupRequestDecline', 'class' => GroupApi::class, 'enable' => true, 'auth' => true],
		'chat' => ['path' => 'chat', 'class' => ChatPage::class, 'enable' => true, 'auth' => true],
		'profile' => ['path' => 'p/{userId}', 'class' => ProfilePage::class, 'enable' => true],
		'profiles' => ['path' => 'profiles', 'class' => ProfilesPage::class, 'enable' => true],
		'profile_edit_confirm' => ['path' => 'pedit/confirm', 'class' => ProfileEditConfirmPage::class, 'enable' => true, 'auth' => true],
		'profile_edit_profile' => ['path' => 'pedit/profile', 'class' => ProfileEditProfilePage::class, 'enable' => true, 'auth' => true],
		'profile_edit_password' => ['path' => 'pedit/password', 'class' => ProfileEditPasswordPage::class, 'enable' => true, 'auth' => true],
		'profile_edit_photo' => ['path' => 'pedit/photo', 'class' => ProfileEditPhotoPage::class, 'enable' => true, 'auth' => true],
		'policy' => ['path' => 'privacyPolicy.html', 'class' => StaticPage::class, 'enable' => true],
		'rules' => ['path' => 'rules.html', 'class' => StaticPage::class, 'enable' => true],
		'signup' => ['path' => 'signup', 'class' => SignupPage::class, 'enable' => true],
		'signin' => ['path' => 'signin', 'class' => SigninPage::class, 'enable' => true],
		'signup_confirm' => ['path' => 'signup', 'class' => SignupPage::class, 'enable' => true],
		'password_forgot' => ['path' => 'password_forgot', 'class' => PasswordForgotPage::class, 'enable' => true],
		'password_forgot_confirm' => ['path' => 'password_forgot_confirm', 'class' => PasswordForgotConfirmPage::class, 'enable' => true],
		'groups' => ['path' => 'groups', 'class' => GroupsPage::class, 'enable' => true, 'auth' => true],
		'group_create' => ['path' => 'group/add', 'class' => GroupAddPage::class, 'enable' => true, 'auth' => true],
		'group_edit' => ['path' => 'group/edit/{groupId}', 'class' => GroupEditPage::class, 'enable' => true, 'auth' => true],
		'group' => ['path' => 'g/{groupId}', 'class' => GroupPage::class, 'enable' => true, 'auth' => true],
		'group_chat' => ['path' => 'g/{groupId}/chat', 'class' => GroupChatPage::class, 'enable' => true, 'auth' => true],
	];
	/** @var string|null */
	private $class;
	/** @var string|null */
	private $method;
	/** @var string|null */
	private $path;
	/** @var string|null */
	private $routeName;
	/** @var bool */
	private $isAuth = false;
	/** @var string */
	private $urlRoot;
	private $routeParams = [];

	public function __construct(Request $request)
	{
		if ($request->isHttps()) {
			$this->protocol = 'https';
		}
		$this->urlRoot = $this->getProtocol() . '://' . $_SERVER['HTTP_HOST'] . '/';

		$this->request = $request;

		$this->path = substr($this->getProperty($_SERVER, 'REQUEST_URI'), 1);
		if (substr($this->path, mb_strlen($this->path) - 1, 1) === '/') {
			$this->path = mb_substr($this->path, 0, mb_strlen($this->path) - 1);
		}
		$this->path = preg_replace("~\?.*?$~", '', $this->path);

		foreach ($this->routes as $routeName => $route) {
			$this->routes[$routeName]['active'] = false;
			if ($route['enable'] && /*$route['path'] === $this->getPath()*/$this->isRoutePath($route['path'])) {
				$this->routeName = $routeName;
				$this->class = $route['class'];
				$this->method = $route['method'] ?? null;
				$this->routes[$routeName]['active'] = true;
				$this->isAuth = $route['auth'] ?? false;
			}
			if (!isset($route['auth'])) {
				$this->routes[$routeName]['auth'] = false;
			}
		}
	}

	public function getProtocol(): string
	{
		return $this->protocol;
	}

	public function getUrlRoot(): string
	{
		return $this->urlRoot;
	}

	public function getRouteClass(): ?string
	{
		return $this->class;
	}

	public function getMethod(): ?string
	{
		return $this->method;
	}

	public function getPath(): string
	{
		return $this->path;
	}

	public function getRouteName(): ?string
	{
		return $this->routeName;
	}

	public function isAuth(): bool
	{
		return $this->isAuth;
	}

	/**
	 * @param string|null $key
	 *
	 * @return array|string|null
	 * @throws \Exception
	 */
	public function getRouteParams(string $key = null)
	{
		if (!is_null($key)) {
			return $this->getProperty($this->routeParams, $key);
		}

		return $this->routeParams;
	}

	public function renderUrl(string $routeName, array $params = []): string
	{
		if (!isset($this->routes[$routeName])) {
			throw new \Exception("Route name '" . htmlspecialchars($routeName) . "' not found.");
		}

		$path = $this->routes[$routeName]['path'];

		foreach ($params as $key => $val) {
			$path = str_replace('{'.$key.'}', $val, $path);
			//$params[$key] = $key . '=' . urlencode($val);
		}

		//if ($params) {
			//$url.= '?' . implode('&amp;', $params);
		//}

		$url = $this->getUrlRoot() . $path;

		return $url;
	}

	public function getRoutesForJsAsArray(): array
	{
		$result = [];

		foreach ($this->routes as $routeName => $route) {
			if (!$route['enable']) {
				continue;
			}
			if (!$route['auth'] && !in_array($routeName, ['','apiCheckUser','apiSignUpConfirm','apiCities','signin'])) {
				continue;
			}
			$result[$routeName] = [
				'path' => $route['path'],
				'auth' => $route['auth'],
			];
		}

		return $result;
	}

	public function getRoutesForJs(): string
	{
		return json_encode($this->getRoutesForJsAsArray());
	}

	private function isRoutePath(string $path): bool
	{
		$paths = explode('/', $path);
		$pathsCurrent = explode('/', $this->getPath());
		if (count($paths) !== count($pathsCurrent)) {
			return false;
		}
		foreach ($paths as $key => $pathItem) {
			if ($pathItem === $pathsCurrent[$key]) {
				continue;
			}
			preg_match("~^\{(.+)\}$~m", $pathItem, $matches);
			if (!isset($matches[1])) {
				return false;
			}
			$this->routeParams[$matches[1]] = $pathsCurrent[$key];
		}

		return true;
	}
}