<?php
namespace App\Framework;

class ConfigEntityRelation {
	/** @var ConfigEntityField */
	private $configEntityField;
	/** @var string */
	private $type;
	/** @var string */
	private $targetEntity;
	/** @var string */
	private $referencedFieldName;

	public function __construct(ConfigEntityField $configEntityField, array $config)
	{
		$this->configEntityField = $configEntityField;

		if (!array_key_exists('type', $config)) {
			throw new \Exception("Property type is required for relation.");
		}
		$this->setType($config['type']);
		if (!in_array($this->getType(), Db::getDbRelationTypes())) {
			throw new \Exception("Invalid relation type '" . htmlspecialchars($this->getType()) . "'.");
		}

		if (!array_key_exists('targetEntity', $config)) {
			throw new \Exception("Property targetEntity is required for relation.");
		}
		$this->setTargetEntity($config['targetEntity']);

		if (!array_key_exists('referencedFieldName', $config)) {
			throw new \Exception("Property referencedFieldName is required for relation.");
		}
		$this->setReferencedFieldName($config['referencedFieldName']);
	}

	public function getConfigEntityField(): ConfigEntityField
	{
		return $this->configEntityField;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}

	public function getTargetEntity(): string
	{
		return $this->targetEntity;
	}

	public function setTargetEntity(string $targetEntity): void
	{
		$this->targetEntity = $targetEntity;
	}

	public function getReferencedFieldName(): string
	{
		return $this->referencedFieldName;
	}

	public function setReferencedFieldName(string $referencedFieldName): void
	{
		$this->referencedFieldName = $referencedFieldName;
	}

	public function validate(): void
	{
		$targetConfigEntity = $this->getConfigEntityField()->getConfigEntity()->getConfigEntities()->getConfigEntityByClass($this->getTargetEntity());
		if (!array_key_exists($this->getReferencedFieldName(), $targetConfigEntity->getFields())) {
			throw new \Exception("No exists referencedFieldName '" . htmlspecialchars($this->getReferencedFieldName()) . "' in '{$this->getTargetEntity()}'.");
		}
	}
}