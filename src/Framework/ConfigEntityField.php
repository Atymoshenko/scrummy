<?php
namespace App\Framework;

class ConfigEntityField {
	/** @var ConfigEntity */
	private $configEntity;
	/** @var string */
	private $name;
	/** @var string */
	private $type;
	/** @var int|null */
	private $length;
	/** @var array */
	private $enumChoices = [];
	/** @var bool */
	private $unsigned = false;
	/** @var bool */
	private $nullable = true;
	/** @var mixed */
	private $default = null;
	/** @var ConfigEntityRelation|null */
	private $relation;

	public function __construct(ConfigEntity $configEntity, string $name, array $config)
	{
		$this->configEntity = $configEntity;

		if (!preg_match("~^[a-zA-Z_0-9\-]{1,50}$~", $name)) {
			throw new \Exception("Invalid field name '" . htmlspecialchars($name) . "'.");
		}
		$this->name = $name;

		if (!array_key_exists('type', $config)) {
			throw new \Exception("Property type is required for field '{$this->name}'");
		}
		if (!in_array($config['type'], Db::getDbTypes())) {
			throw new \Exception("Db property type '" . htmlspecialchars($config['type']) . "' is not supported.");
		}
		$this->type = $config['type'];
		if ($this->getType() === Db::DB_TYPE_ENUM) {
			if (!array_key_exists('enumChoices', $config)) {
				throw new \Exception("Field type enum must have array property enumChoices in field {$name}.");
			}
			if (!is_array($config['enumChoices'])) {
				throw new \Exception("Field property enumChoices must be array, " . gettype($config['enumChoices']) . " given in field {$name}.");
			}
			$enumChoicesCount = count($config['enumChoices']);
			if ($enumChoicesCount < 1) {
				throw new \Exception("Enum choices must contain at least 1 choice in field {$name}.");
			}
			foreach ($config['enumChoices'] as $enumChoice) {
				if (!is_int($enumChoice) && !is_string($enumChoice)) {
					throw new \Exception("Enum choices must be type integer or string, " . gettype($enumChoice) . " given in field {$name}.");
				}
				$this->enumChoices[] = $enumChoice;
			}
		}

		if (isset($config['length'])) {
			if (!is_int($config['length'])) {
				throw new \Exception("Property length must have type integer, " . gettype($config['length']) . " given in field " . $name . ".");
			}
			if ($config['length'] < 1) {
				throw new \Exception("Property length must be from 1 in field " . $name . ".");
			}
			$this->length = $config['length'];
		}

		if (isset($config['unsigned'])) {
			if (!is_bool($config['unsigned'])) {
				throw new \Exception("Property unsigned must have type boolean, " . gettype($config['unsigned']) . " given in field " . $name . ".");
			}
			$this->unsigned = $config['unsigned'];
		}

		if (isset($config['nullable'])) {
			if (!is_bool($config['nullable'])) {
				throw new \Exception("Property nullable must have type boolean, " . gettype($config['nullable']) . " given in field " . $name . ".");
			}
			$this->nullable = $config['nullable'];
		}

		if (isset($config['default'])) {
			if (is_null($config['default']) && !$this->isNullable()) {
				throw new \Exception("Property default can not be null, because property nullable is false in field {$name}.");
			}
			$this->default = $config['default'];
		}

		if (isset($config['relation'])) {
			$this->relation = new ConfigEntityRelation($this, $config['relation']);
		}
	}

	public function getConfigEntity(): ConfigEntity
	{
		return $this->configEntity;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function getEnumChoices(): array
	{
		return $this->enumChoices;
	}

	public function isNullable(): bool
	{
		return $this->nullable;
	}

	public function getRelation(): ?ConfigEntityRelation
	{
		return $this->relation;
	}

	public function validateRelations(): void
	{
		if ($this->getRelation()) {
			$this->getRelation()->validate();
		}
	}
}