<?php
namespace App\Framework;

use App\Api\Api;
use App\Entity\User;
use App\Entity\UserProfile;
use App\Exception\NeedSignInException;
use App\Framework\Exception\NotFoundException;
use App\Framework\Traits\AccessorTrait;
use App\Mail\Mail;
use App\Page\Page;

/**
 * @method App trans(mixed $data, array $params = [], string|null $category = null)
 * @method App path(string $routeName, array $params = [])
 */
class App {
	use AccessorTrait;

	/** @var array */
	public $config;
	/** @var Request */
	public $request;
	/** @var Db */
	public $db;
	/** @var Response */
	private $response;
	/** @var string|null */
	private $templateLayout;
	private $javascripts = [];
	private $stylesheets = [];
	private $templateParams = [];
	private $route;
	private $pathRoot;
	private $pathApp;
	private $pathStatic;
	private $pathUsersPhotos;
	/** @var Mail */
	private $mail;
	/** @var User|null */
	private $user;
	/** @var UserProfile|null */
	private $userProfile;
	private $locales = [];
	private $locale;
	private $staticVersion = 24;
	private $translations;

	public function __construct()
	{
		mb_internal_encoding("UTF-8");
		date_default_timezone_set('UTC');

		$this->pathRoot = realpath(__DIR__ . '/../../') . '/';
		$this->pathApp = $this->getPathRoot() . 'src/';
		$this->pathStatic = $this->getPathRoot() . 'public/';
		$this->pathUsersPhotos = $this->getPathStatic() . 'photos/';
		$file = $this->getPathApp() . 'config.php';
		$this->config = require_once $file;
		$this->locales = [
			'en' => [
				'name' => 'English',
				'enabled' => true,
			],
			'ru' => [
				'name' => 'Русский',
				'enabled' => true,
			],
		];
		$this->request = new Request();
		$this->route = new Route($this->request);
		$this->initLocale();
		$this->db = new Db($this->config['db_host'], $this->config['db_user'], $this->config['db_password'], $this->config['db_name'], $this->config['entity']);
		$this->db->setLocale($this->locale);
	}

	public function run()
	{
		$this->response = new Response();

		try {
			$routeClass = $this->route->getRouteClass();
			if (!$routeClass) {
				throw new NotFoundException();
			}
			/** @var Page $pageClass */
			$pageClass = new $routeClass($this, $this->response);
			if ($this->route->getMethod()) {
				/** @var Api $pageClass */
				$pageClass->{$this->route->getMethod()}();
				$this->response = $pageClass->getResponse();
			} else {
				$this->response = $pageClass->run();
			}
		} catch (NeedSignInException $e) {
			// $this->response = $pageClass->getResponse();
			$this->response->setStatusCode(200);
			$this->response->setContent(json_encode([
				'needAuth' => true,
				'message' => $e->getMessage(),
			]));
		} catch (NotFoundException $e) {
			$this->response = new Response($this->_translate('Page not found.'), 404);
		} catch (\Throwable $e) {
			$this->response = new Response($this->_translate('Technical error.') , 500);
			if ($this->isDev()) {
				$this->response->setContent('File: ' . $e->getFile() . '(' . $e->getLine() . ') Message: ' . $e->getMessage());
			} else {
				$this->response->setContent($this->_translate('Technical error.'));
			}
		} finally {
			$this->printResponse();
		}
	}

	public function getPathRoot(): string
	{
		return $this->pathRoot;
	}

	public function getPathApp(): string
	{
		return $this->pathApp;
	}

	public function getPathStatic(): string
	{
		return $this->pathStatic;
	}

	public function getPathUserPhotosRelative(int $userId): string
	{
		$folder = $this->getPathStatic();

		$folderSuffix = 'photos/';
		if (!is_dir($folder . $folderSuffix) && !mkdir($folder . $folderSuffix, 0777)) {
			throw new \Exception("Can't create folder " . $folder . $folderSuffix);
		}

		$folderSuffix.= ceil($userId / 1000000) . '/';

		if (!is_dir($folder . $folderSuffix) && !mkdir($folder . $folderSuffix, 0777)) {
			throw new \Exception("Can't create folder " . $folder . $folderSuffix);
		}

		$folderSuffix.= ceil($userId / 1000) . '/';
		if (!is_dir($folder . $folderSuffix) && !mkdir($folder . $folderSuffix, 0777)) {
			throw new \Exception("Can't create folder " . $folder . $folderSuffix);
		}

		$folderSuffix.= $userId . '/';
		if (!is_dir($folder . $folderSuffix) && !mkdir($folder . $folderSuffix, 0777)) {
			throw new \Exception("Can't create folder " . $folder . $folderSuffix);
		}

		return $folderSuffix;
	}

	public function getConfig(): array
	{
		return $this->config;
	}

	public function getLocales(): array
	{
		return $this->locales;
	}

	public function getLocaleDefault(): string
	{
		return $this->config['localeDefault'];
	}

	private function getLocale(): string
	{
		if (is_null($this->locale)) {
			$this->initLocale();
		}

		return $this->locale;
	}

	private function initLocale(): void
	{
		$this->locale = $this->getConfig()['localeDefault'];
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$locale = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
			if (array_key_exists($locale, $this->getLocales())) {
				$this->locale = $locale;
			} else {
				if ($locale === 'uk') {
					$this->locale = 'ru';
				}
			}
		}
		if (isset($_COOKIE['locale']) && array_key_exists($_COOKIE['locale'], $this->getLocales())) {
			$this->locale = $_COOKIE['locale'];
		}
		if (isset($_REQUEST['locale']) && array_key_exists($_REQUEST['locale'], $this->getLocales())) {
			$this->locale = $_REQUEST['locale'];
		}
	}

	public function getStaticVersion(): int
	{
		return $this->staticVersion;
	}

	public function getRequest(): Request
	{
		return $this->request;
	}

	public function getDb(): Db
	{
		return $this->db;
	}

	public function getMail(): Mail
	{
		if (is_null($this->mail)) {
			$this->mail = new Mail($this->getConfig()['mailer']);
		}

		return $this->mail;
	}

	public function getUser(): ?User
	{
		return $this->user;
	}

	public function setUser(User $user): void
	{
		$this->user = $user;
	}

	public function removeUser(): void
	{
		$this->user = null;
	}

	public function getUserProfile(): ?UserProfile
	{
		return $this->userProfile;
	}

	public function setUserProfile(UserProfile $userProfile): void
	{
		$this->userProfile = $userProfile;
	}

	public function getRoute(): Route
	{
		return $this->route;
	}

	public function isAjax(): bool
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' ? true : false;
	}

	public function isDev(): bool
	{
		return $this->getConfig()['env'] === 'dev';
	}

	public function renderAjaxOrHtml(string $view, array $params = [], Response $response = null): Response
	{
		if (is_null($response)) {
			$response = new Response();
		}

		$response->setContent($this->renderView($view, $params));

		return $response;
	}

	public function renderView(string $view, array $params = []): string
	{
		return $this->render($view, $params);
	}

	private function render(): string
	{
		extract(func_get_arg(1));
		ob_start();
		require $this->getPathApp() . 'template/' . $this->getConfig()['templateDefault'] . '/' . func_get_arg(0);

		return ob_get_clean();
	}

	public function __call(string $name, $arguments)
	{
		switch($name) {
			case 'extend':
				$this->_extend(reset($arguments));
				break;
			case 'javascripts':
				$this->_javascripts();
				break;
			case 'stylesheets':
				$this->_stylesheets();
				break;
			case 'path':
				return $this->_path($arguments[0], $arguments[1] ?? []);
				break;
			case 'include':
				$this->_include($arguments[0], $arguments[1] ?? []);
				break;
			case 'getRoutesForJsAsArray':
				return $this->_getRoutesForJsAsArray();
				break;
			case 'getRoutesForJs':
				return $this->_getRoutesForJs();
				break;
			case 'trans':
				return $this->_translate($arguments[0] ?? '', $arguments[1] ?? [], $arguments[2] ?? null);
				break;
			default:
				throw new \Exception("Undefined function: " . htmlspecialchars($name));
		}

		return null;
	}

	private function _extend(array $params)
	{
		if (isset($params['file'])) {
			$this->templateLayout = (string)$params['file'];
		}
		if (isset($params['javascripts'])) {
			$this->javascripts = array_merge($this->javascripts, $params['javascripts']);
		}
		if (isset($params['stylesheets'])) {
			$this->stylesheets = array_merge($this->stylesheets, $params['stylesheets']);
		}
		if (isset($params['params']) && is_array($params['params'])) {
			$this->templateParams = array_merge($this->templateParams, $params['params']);
		}
	}

	private function _javascripts(): void
	{
		foreach ($this->javascripts as $javascript) {
			echo "<script src=\"" . $javascript . "?v" . $this->getStaticVersion() . "\"></script>\n";
		}
	}

	private function _stylesheets(): void
	{
		foreach ($this->stylesheets as $stylesheet) {
			echo "<link rel=\"stylesheet\" href=\"" . $stylesheet . "?v" . $this->getStaticVersion() . "\">\n";
		}
	}

	private function _path(string $routeName, array $params = []): string
	{
		return $this->getRoute()->renderUrl($routeName, $params);
	}

	private function _include(string $file, array $params = []): void
	{
		extract($params);
		require $this->getPathApp() . 'template/' . $this->getConfig()['templateDefault'] . '/' . $file;
	}

	private function _translate($data, array $params = [], ?string $category = null): string
	{
		if (is_null($category)) {
			$category = 'message';
		}
		if (!in_array($category, ['message','table-room'])) {
			throw new \Exception('Unknown translation category.');
		}

		if (!isset($this->translations[$category])) {
			$file = $this->getPathApp() . 'Translations/' . $category . '.' . $this->getLocale() . '.php';
			$this->translations[$category] = require $file;
		}

		if (!isset($this->translations[$category][$data])) {
			return $data;
		}

		return str_replace(array_keys($params), array_values($params), $this->translations[$category][$data]);
	}

	private function _getRoutesForJsAsArray(): array
	{
		return $this->getRoute()->getRoutesForJsAsArray();
	}

	private function _getRoutesForJs(): string
	{
		return $this->getRoute()->getRoutesForJs();
	}

	private function printResponse(): void
	{
		header('HTTP/1.0 '.$this->response->getStatusCode().' ' . $this->response->getStatusCodeText());
		header('Content-Type: ' . $this->response->getContentType() . '; charset=' . $this->response->getCharset());

		foreach ($this->response->getHeaders() as $header) {
			header($header);
		}

		if (!$this->isAjax() && $this->templateLayout) {
			$this->response = $this->renderAjaxOrHtml($this->templateLayout, array_merge($this->templateParams, [
				'env' => $this->config['env'],
				'content' => $this->response->getContent(),
				'baseHref' => $this->getRoute()->getUrlRoot(),
				'lang' => $this->getLocale(),
				'googleClientId' => $this->getConfig()['googleClientId'],
				'staticVersion' => $this->getStaticVersion(),
				'route' => $this->getRoute()->getRouteName(),
			]), $this->response);
		}

		echo $this->response->getContent();
	}
}