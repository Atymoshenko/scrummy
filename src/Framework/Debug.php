<?php

trait SingletonTrait
{
	static private $instance = null;

	private function __construct()
	{
	}

	private function __clone()
	{
	}

	private function __wakeup()
	{
	}

	static public function getInstance()
	{
		return self::$instance === null ? self::$instance = new static() : self::$instance;
	}
}

class Debug
{
	use SingletonTrait;

	/** @var string */
	private $type;

	public function getType(): string
	{
		return $this->type;
	}

	public function dump($data = null)
	{
		$this->type = gettype($data);

		$this->printHeader();

		$db = debug_backtrace(false);
		$db = $db[1];
		echo $db['file'] . ' (' . $db['line'] . ')' . '<br />';

		switch (strtolower($this->getType())) {
			case 'null':
				$this->printType('999');
				break;
			case 'string':
				$this->printType('f90');
				echo htmlspecialchars($data);
				break;
			case 'integer':
				$this->printType('3f0');
				echo $data;
				break;
			case 'array':
			case 'object':
				$this->printType('39f');
				print_r($data);
				break;
			case 'boolean':
				$this->printType('3f0');
				echo $data ? 'true' : 'false';
				break;
			default:
				$this->printType();
				print_r($data);
		}

		$this->printFooter();
	}

	private function printHeader(): void
	{
		echo '<div style="position:relative;z-index:5000;margin:1px;padding:3px;border:0;background-color:#000;color:#fff;font:normal normal normal 12px/14px Verdana;">';
		echo '<pre style="margin:0;padding:0;font:normal normal normal 12px/14px Verdana;color:#fff;">';
	}

	private function printFooter(): void
	{
		echo '</pre>';
		echo '</div>';
	}

	private function printType(string $color = 'fff'): void
	{
		echo '<span style="color:#' . $color . '">';
		echo $this->getType() . '</span>';
		echo '<br />';
	}
}

function dump($data = null): void
{
	$debug = Debug::getInstance();
	$debug->dump($data);
}