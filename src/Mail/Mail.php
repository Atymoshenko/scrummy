<?php
namespace App\Mail;

use App\Entity\User;
use App\Entity\UserPasswordReset;

class Mail {
	private $config;
	private $mailer;
	public function __construct(array $config)
	{
		$this->config = $config;
	}

	public function getMailer(): \Swift_Mailer
	{
		if (is_null($this->mailer)) {
			$transport = new \Swift_SmtpTransport($this->config['host'], $this->config['port']);
			$transport->setUsername($this->config['user']);
			$transport->setPassword($this->config['password']);
			;

			$this->mailer = new \Swift_Mailer($transport);
		}

		return $this->mailer;
	}

	public function sendUserSignUp(User $user, string $urlConfirm): int
	{
		$message = new \Swift_Message('Registration on Scrummy');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Welcome to the Scrummy</h1>
			<p>To activate your account, follow the link <a href="' . $urlConfirm . '" target="_blank">' . $urlConfirm . '</a> or enter the activation code on your profile page.</p>
			<p>Activation code: <b>' . $user->getEmailConfirmToken() . '</b></p>
			' . ($user->getEmailConfirmExpire() ? '<p>The link expires before ' . $user->getEmailConfirmExpire()->format('d.m.Y H:i') . ', otherwise your account will be deleted.</p>' : '') . '
			<p>Do not reply to this email, as it was sent by a robot. The answer we can not read.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}

	public function sendUserDeletedByEmailConfirmExpired(User $user): int
	{
		$message = new \Swift_Message('Your accound has been deleted from Scrummy');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Your accound has been deleted from Scrummy</h1>
			<p>You did not confirm mail for a long time, the confirmation period has expired.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}

	public function sendUserPasswordReset(User $user, UserPasswordReset $userPasswordReset, string $urlReset): int
	{
		$message = new \Swift_Message('Password recovery on Scrummy');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Password recovery on Scrummy</h1>
			<p>Below is a password recovery guide.</p>
			<p>If you didn’t request password recovery on Scrummy website or you think that this letter was received by mistake, then you don’t need to do anything.</p>
			<p>To reset a password, follow the link <a href="' . $urlReset . '" target="_blank">' . $urlReset . '</a>.</p>
			<p>The link expires before ' . $userPasswordReset->getExpire()->format('d.m.Y H:i') . '.</p>
			<p>Do not reply to this email, as it was sent by a robot. The answer we can not read.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}

	public function sendFriendApprove(User $user, User $userFrom, string $userProfileUrl): int
	{
		$message = new \Swift_Message('Friend request');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Friend request on the Scrummy</h1>
			<p>User ' . htmlspecialchars($userFrom->getNickname()) . ' sent you a friend request, go to the <a href="' . $userProfileUrl . '" target="_blank">user profile page</a> to accept or reject the request.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}

	public function createdUserProfileComment(User $user, User $userFrom, string $userProfileUrl): int
	{
		$message = new \Swift_Message('New comment');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Hi ' . htmlspecialchars($user->getNickname()) . '</h1>
			<p>On your profile page, user ' . htmlspecialchars($userFrom->getNickname()) . ', wrote a new comment, go to your <a href="' . $userProfileUrl . '" target="_blank">profile page</a> to read it.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}

	public function createdUserProfileLike(User $user, User $userFrom, string $userProfileUrl): int
	{
		$message = new \Swift_Message('Someone likes you');
		$message->setFrom([$this->config['from_email'] => $this->config['from_name']]);
		$message->setTo([$user->getEmail() => $user->getNickname()]);
		$message->setBody('<html lang="en"><body>
			<h1>Hi ' . htmlspecialchars($user->getNickname()) . '</h1>
			<p>On your profile page, user ' . htmlspecialchars($userFrom->getNickname()) . ', like you, go to your <a href="' . $userProfileUrl . '" target="_blank">profile page</a> to view all likes.</p>
		</body></html>', 'text/html');

		return $this->getMailer()->send($message);
	}
}