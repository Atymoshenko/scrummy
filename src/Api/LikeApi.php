<?php
namespace App\Api;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Entity\UserProfileLike;
use App\Validator\Entity\CommentListFilter;

class LikeApi extends Api {
	public function methodLikeCreate(): void
	{
		$this->ajaxParams['success'] = false;

		list($object, $objectId) = $this->getObjectAndObjectId();

		switch ($object) {
			case CommentListFilter::OBJECT_USER_PROFILE:
				/** @var UserProfile|null $userProfile */
				$userProfile = $this->getApp()->getDb()->findOneBy(['userId' => $objectId], UserProfile::class);
				if (!$userProfile) {
					throw new \Exception("User with id {$objectId} not found.");
				}
				/** @var UserProfileLike|null $userProfileLike */
				$userProfileLike = $this->getApp()->getDb()->findOneBy(['userId' => $objectId, 'userIdLike' => $this->getApp()->getUser()->getId()], UserProfileLike::class);
				if ($userProfileLike) {
					throw new \Exception("User id {$this->getApp()->getUser()->getId()} repeatedly tried to like user id {$objectId}.");
				}
				/** @var User $user */
				$user = $this->getApp()->getDb()->findOneBy(['id' => $objectId], User::class);
				$userProfileLike = new UserProfileLike($user, $this->getApp()->getUser());
				$this->getApp()->getDb()->beginTransaction();
				$this->getApp()->getDb()->save($userProfileLike);
				$this->getApp()->getDb()->incrementUserProfileLike($userProfile->getUserId());
				$this->getApp()->getDb()->commit();
				try {
					$this->getApp()->getMail()->createdUserProfileLike($user, $this->getApp()->getUser(), $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $user->getId()]));
				} catch (\Throwable $e) {

				}
				$this->ajaxParams['success'] = true;
				$this->ajaxParams['likeCount'] = $userProfile->getLikeCount() + 1;

				break;
		}
	}

	public function methodLikesCount(): void
	{
		$this->ajaxParams['success'] = false;

		list($object, $objectId) = $this->getObjectAndObjectId();

		switch ($object) {
			case CommentListFilter::OBJECT_USER_PROFILE:
				/** @var UserProfile|null $userProfile */
				$userProfile = $this->getApp()->getDb()->findOneBy(['userId' => $objectId], UserProfile::class);
				if (!$userProfile) {
					throw new \Exception("User with id {$objectId} not found.");
				}
				/** @var UserProfileLike|null $userProfileLike */
				$userProfileLike = $this->getApp()->getDb()->findOneBy(['userId' => $objectId, 'userIdLike' => $this->getApp()->getUser()->getId()], UserProfileLike::class);
				$this->ajaxParams['success'] = true;
				$this->ajaxParams['likeCount'] = $userProfile->getLikeCount();
				$this->ajaxParams['iLiked'] = (bool)$userProfileLike;
				break;
		}

	}

	public function methodLikeRemove(): void
	{
		$this->ajaxParams['success'] = false;

		list($object, $objectId) = $this->getObjectAndObjectId();

		switch ($object) {
			case CommentListFilter::OBJECT_USER_PROFILE:
				/** @var UserProfile|null $userProfile */
				$userProfile = $this->getApp()->getDb()->findOneBy(['userId' => $objectId], UserProfile::class);
				if (!$userProfile) {
					throw new \Exception("User with id {$objectId} not found.");
				}
				/** @var UserProfileLike|null $userProfileLike */
				$userProfileLike = $this->getApp()->getDb()->findOneBy(['userId' => $objectId, 'userIdLike' => $this->getApp()->getUser()->getId()], UserProfileLike::class);
				if (!$userProfileLike) {
					throw new \Exception("User id {$this->getApp()->getUser()->getId()} repeatedly tried to remove like user id {$objectId}.");
				}
				$this->getApp()->getDb()->beginTransaction();
				$this->getApp()->getDb()->remove($userProfileLike);
				$this->getApp()->getDb()->decrementUserProfileLike($userProfile->getUserId());
				$this->getApp()->getDb()->commit();
				$this->ajaxParams['success'] = true;
				$this->ajaxParams['likeCount'] = $userProfile->getLikeCount() - 1;

				break;
		}
	}

	private function getObjectAndObjectId(): array
	{
		$result = [
			0 => (string)$this->getApp()->getRequest()->getForApi('object', null, $this->getApp()->isDev()),
			1 => (string)$this->getApp()->getRequest()->getForApi('objectId', null, $this->getApp()->isDev()),
		];

		if (!$result[0]) {
			throw new \Exception('No object.');
		}
		if (!in_array($result[0], CommentListFilter::getObjects())) {
			throw new \Exception("Undefined object " . $result[0] . ".");
		}
		if (!$result[1]) {
			throw new \Exception('No objectId.');
		}

		return $result;
	}
}