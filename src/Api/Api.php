<?php
namespace App\Api;

use App\Entity\City;
use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\Room;
use App\Entity\User;
use App\Entity\UserFriend;
use App\Entity\UserBlock;
use App\Entity\UserProfile;
use App\Entity\UserView;
use App\Exception\NeedSignInException;
use App\Framework\App;
use App\Framework\Db;
use App\Framework\Response;
use App\Service\Manager\UserManager;

class Api {
	const PATH_PREFIX = 'api/';

	private $app;
	protected $ajaxParams = ['apiToken' => null];
	private $response;

	public function __construct(App $app, Response $response = null)
	{
		$this->app = $app;
		$this->response = $response ?? new Response();

		if ($this->getApp()->isAjax()) {
			$this->response->setContentType('application/json');
		}

		if ($this->app->getRoute()->isAuth()) {
			$this->methodCheckUser();
		}
	}

	public function getApp(): App
	{
		return $this->app;
	}

	public function getResponse(): Response
	{
		$this->response->setContent(json_encode($this->ajaxParams));

		return $this->response;
	}

	public function methodCheckUser(): void
	{
		$apiToken = $this->app->getRequest()->getForApi('apiToken', null, $this->app->isDev());
		if (!$apiToken) {
			throw new NeedSignInException('apiToken is required.');
		}

		preg_match("~^([^.]+)\.([^.]+)\.(M|F)$~", $apiToken, $matches);
		if (!isset($matches[3])) {
			throw new NeedSignInException('Wrong apiToken.');
		}

		$userId = base64_decode($matches[1], true);
		if (!preg_match("~^\d{1,11}$~", $userId)) {
			throw new NeedSignInException('Wrong or expired apiToken.');
		}

		/** @var User $user */
		$user = $this->app->getDb()->findOneBy(['id' => $userId], User::class);
		if (!$user) {
			throw new NeedSignInException('Fake or expired apiToken.');
		}
		$this->app->setUser($user);
		/** @var UserProfile $userProfile */
		$userProfile = $this->app->getDb()->findOneBy(['userId' => $this->app->getUser()->getId()], UserProfile::class);
		$this->app->setUserProfile($userProfile);
		$this->ajaxParams['locale'] = $userProfile->getLocale();

		if ($this->app->getUser()->isEmailConfirmExpired()) {
			$this->app->getDb()->remove($this->app->getUser());
			$this->app->getMail()->sendUserDeletedByEmailConfirmExpired($this->app->getUser());
			$this->ajaxParams['emailConfirmExpired'] = true;
			throw new NeedSignInException('Fake or expired apiToken.');
		}

		if ($this->app->getUser()->getApiToken() !== $apiToken) {
			throw new NeedSignInException('Fake apiToken.');
		}

		if ($this->app->getUser()->isApiTokenExpired()) {
			throw new NeedSignInException('apiToken is expired.');
		}

		$this->app->getUser()->setLastVisit();
		$this->app->getDb()->save($this->app->getUser());

		if (!$this->app->getUser()->isEmailConfirmed()) {
			$this->ajaxParams['redirect'] = $this->app->getRoute()->renderUrl('profile_edit_confirm');
		}

		$this->ajaxParams['apiToken'] = $this->app->getUser()->getApiToken();

		$tasks = $this->app->getRequest()->getForApi('tasks', null, $this->app->isDev());
		$tasks = json_decode($tasks);
		if (is_array($tasks)) {
			foreach ($tasks as $task) {
				switch ($task) {
					case 'group':
						$this->ajaxParams[$task] = null;
						$groupId = (int)$this->app->getRequest()->getForApi('groupId', null, $this->app->isDev());
						/** @var Group $group */
						$group = $this->app->getDb()->findOneBy(['id' => $groupId, 'userId' => $this->app->getUser()->getId()], Group::class);
						if ($group) {
							$this->ajaxParams[$task] = [
								'id' => $group->getId(),
								'name' => htmlspecialchars($group->getName()),
								'description' => htmlspecialchars($group->getDescription()),
								'private' => $group->isPrivate(),
							];
						}
						break;
					case 'group_details':
						$this->ajaxParams[$task] = [];
						$this->ajaxParams[$task]['groupUser'] = null;
						$this->ajaxParams[$task]['group'] = null;
						$this->ajaxParams[$task]['isOwner'] = false;
						$groupId = (int)$this->app->getRequest()->getForApi('groupId', null, $this->app->isDev());
						/** @var Group $group */
						$group = $this->app->getDb()->findOneBy(['id' => $groupId], Group::class);
						if ($group) {
							if ($group->getUserId() === $user->getId()) {
								$this->ajaxParams[$task]['isOwner'] = true;
							}
							/** @var GroupUser|null $groupUser */
							$groupUser = $this->app->getDb()->findOneBy([
								'groupId' => $group->getId(),
								'userId' => $user->getId(),
							], GroupUser::class);
							$this->ajaxParams[$task]['group'] = [
								'id' => $group->getId(),
								'private' => $group->isPrivate(),
							];
							if ($groupUser) {
								$this->ajaxParams[$task]['groupUser'] = [
									'approved' => $groupUser->isApproved(),
								];
							}
							if ($group->isPrivate() && $this->ajaxParams[$task]['isOwner']) {
								/** @var User[] $groupUserList */
								$groupUserList = $this->getApp()->getDb()->findGroupUserRequests($group);
								$this->ajaxParams[$task]['userRequests'] = [];
								foreach ($groupUserList as $requestUser) {
									$this->ajaxParams[$task]['userRequests'][] = [
										'id' => $requestUser->getId(),
										'nickname' => $requestUser->getNickname(),
										'urlProfile' => $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $requestUser->getId()]),
									];
								}
							}
						}
						break;
					case 'user':
						$userManager = new UserManager($this->getApp(), $user);
						$this->ajaxParams[$task]['nickname'] = $user->getNickname();
						$this->ajaxParams[$task]['email'] = $user->getEmail();
						$this->ajaxParams[$task]['emailConfirmed'] = $user->isEmailConfirmed();
						$this->ajaxParams[$task]['emailConfirmExpireText'] = $userManager->getEmailConfirmExpireText();
						break;
					case 'profile':
						$this->ajaxParams[$task] = $userProfile->toArray();
						break;
					case 'profileView':
						$userId = (int)$this->app->getRequest()->getForApi('userId', null, $this->app->isDev());
						$isFriend = $isBlocked = $isFriendRequestSent = $isFriendNeedApprove = null;
						$iBlocked = false;
						$this->ajaxParams[$task]['user'] = [];
						$this->ajaxParams[$task]['profile'] = [];

						/** @var User|null $userTarget */
						$userTarget = $this->getApp()->getDb()->findOneBy(['id' => $userId], User::class);
						if (!$userTarget) {
							throw new \Exception("User with userId {$userTarget->getId()} not found.");
						}
						/** @var UserProfile $userProfileTarget */
						$userProfileTarget = $this->getApp()->getDb()->findOneBy(['userId' => $userTarget->getId()], UserProfile::class);
						if (!$userProfileTarget) {
							throw new \Exception("User profile with userId {$userTarget->getId()} not found.");
						}
						$this->getApp()->getDb()->lockTable(UserProfile::class, 'WRITE');
						$userProfileTarget->incrementViewsCount();
						$this->getApp()->getDb()->save($userProfileTarget);
						$this->getApp()->getDb()->unlockTables();
						/** @var UserView $userView */
						$userView = $this->getApp()->getDb()->findOneBy(['userId' => $this->getApp()->getUser()->getId(), 'userIdView' => $userTarget->getId()], UserView::class);
						if (!$userView) {
							$userView = new UserView($this->getApp()->getUser(), $userTarget);
						}
						$userView->setWatchedAt();
						$this->getApp()->getDb()->save($userView);
						$this->ajaxParams[$task]['profile']['viewsCount'] = $userProfileTarget->getViewsCount();
						$this->ajaxParams[$task]['user']['lastVisit'] = $userTarget->getLastVisit()->format('d.m.Y H:i');

						if ($userId !== $this->getApp()->getUser()->getId()) {
							$iBlocked = (bool)$this->app->getDb()->findOneBy(['userId' => $userId, 'userIdBlocked' => $this->app->getUser()->getId()], UserBlock::class);
							/** @var UserFriend $userFriend */
							$userFriend = $this->app->getDb()->findOneBy(['userId' => $this->app->getUser()->getId(), 'userIdFriend' => $userId], UserFriend::class);
							if (!$userFriend) {
								$userFriend = $this->app->getDb()->findOneBy(['userId' => $userId, 'userIdFriend' => $this->app->getUser()->getId()], UserFriend::class);
							}
							$isFriend = $isFriendRequestSent = $isFriendNeedApprove = false;
							if ($userFriend) {
								if ($userFriend->getApprovedAt()) {
									$isFriend = true;
								} else {
									if ($userFriend->getUserId() === $this->app->getUser()->getId()) {
										$isFriendRequestSent = true;
									} else {
										$isFriendNeedApprove = true;
									}
								}
							}

							$userBlock = $this->app->getDb()->findOneBy(['userId' => $this->app->getUser()->getId(), 'userIdBlocked' => $userId], UserBlock::class);
							$isBlocked = $userBlock ? true : false;
						}

						$this->ajaxParams[$task]['isFriend'] = $isFriend;
						$this->ajaxParams[$task]['isFriendRequestSent'] = $isFriendRequestSent;
						$this->ajaxParams[$task]['isFriendNeedApprove'] = $isFriendNeedApprove;
						$this->ajaxParams[$task]['iBlocked'] = $iBlocked;
						$this->ajaxParams[$task]['isBlocked'] = $isBlocked;
						break;
				}
			}
		}
	}

	public function methodCities(): void
	{
		$countryCode = (string)$this->app->getRequest()->getForApi('countryCode', '', $this->app->isDev());
		/** @var City[] $cities */
		$cities = [];
		if ($countryCode && strlen($countryCode) === 2) {
			$cities = $this->app->getDb()->getCities($countryCode);
			foreach ($cities as $key => $city) {
				$cities[$key] = [
					'id' => $city->getId(),
					'name' => htmlspecialchars($city->getName()),
				];
			}
		}

		$this->ajaxParams['cities'] = $cities;
	}

	protected function generateToken(User $user): string
	{
		$date = new \DateTime();
		$token = base64_encode($user->getId()) . '.' . sha1($user->getNickname() . '|' . $date->format('U') . '|' . $user->getPasswordHash()) . '.' . $user->getSex();

		return $token;
	}

	protected function getOrCreateDefaultRoom(): Room
	{
		$this->getApp()->getDb()->lockTable(Room::class, 'WRITE');
		/** @var Room $room */
		$room = $this->getApp()->getDb()->findOneBy(['groupId' => null, 'default' => true], Room::class);
		if (!$room) {
			$room = $this->getApp()->getDb()->findOneBy(['groupId' => null], Room::class);
			if (!$room) {
				$room = new Room('General');
			}
			$room->setDefault(true);
			$this->getApp()->getDb()->save($room);
		}
		$this->getApp()->getDb()->unlockTables();

		return $room;
	}
}
