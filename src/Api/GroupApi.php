<?php
namespace App\Api;

use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\Room;
use App\Entity\UserBlock;
use App\Framework\App;
use App\Validator\Entity\GroupFilter;
use App\Validator\Filter\GroupFilterValidator;
use App\Validator\GroupValidator;
use App\Validator\UserProfileValidator;

class GroupApi extends Api {
	public function methodGroupCreateForm(): void
	{
		$this->ajaxParams['html'] = $this->getApp()->renderView('Group/form.html.php');
	}

	public function methodGroupCreate(): void
	{
		$this->ajaxParams['success'] = false;
		$user = $this->getApp()->getUser();
		$userProfile = $this->getApp()->getUserProfile();
		$userProfileValidator = new UserProfileValidator($userProfile);
		$userProfileValidator->validateCountryRequire();
		$userProfileValidator->validateCityRequire();
		if ($userProfileValidator->hasErrors()) {
			$this->ajaxParams['errors']['validator'] = $userProfileValidator->getErrors();
			return;
		}

		$name = trim($this->getApp()->getRequest()->getForApi('name', '', $this->getApp()->isDev()));
		$description = trim($this->getApp()->getRequest()->getForApi('description', '', $this->getApp()->isDev()));
		$private = (bool)$this->getApp()->getRequest()->getForApi('private', false, $this->getApp()->isDev());
		$group = new Group($user, $name, $description);
		$group->setPrivate($private);
		$groupValidator = new GroupValidator($group, $this->getApp());
		$groupValidator->validate();
		if ($userProfile->getGroupCount() >= 2) {
			$groupValidator->addError('groupCount', 'You cannot create more than 2 groups.');
		}
		if (!$groupValidator->hasErrors()) {
			$this->getApp()->getDb()->beginTransaction();
			$this->getApp()->getDb()->save($group);
			$userProfile->setGroupCount($userProfile->getGroupCount() + 1);
			$this->getApp()->getDb()->save($userProfile);

			$room = new Room($group->getName());
			$room->setGroup($group);
			$room->setDefault(true);
			$this->getApp()->getDb()->save($room);

			$this->getApp()->getDb()->commit();
			$this->ajaxParams['success'] = true;
			$this->ajaxParams['group'] = [
				'id' => $group->getId(),
				'name' => $group->getName(),
				'description' => $group->getDescription(),
				'private' => $group->isPrivate(),
			];
		}

		$this->ajaxParams['notifications'] = $groupValidator->getNotifications();
	}

	public function methodGroupEdit(): void
	{
		$this->ajaxParams['success'] = false;
		$user = $this->getApp()->getUser();

		$groupId = (int)$this->getApp()->getRequest()->getForApi('id', null, $this->getApp()->isDev());
		$name = trim($this->getApp()->getRequest()->getForApi('name', '', $this->getApp()->isDev()));
		$description = trim($this->getApp()->getRequest()->getForApi('description', '', $this->getApp()->isDev()));
		$private = (bool)$this->getApp()->getRequest()->getForApi('private', false, $this->getApp()->isDev());
		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId, 'userId' => $user->getId()], Group::class);
		if (!$group) {
			$this->ajaxParams['notifications']['errors'] = 'Group not found.';
			return;
		}
		$group->setName($name);
		$group->setDescription($description);
		$group->setPrivate($private);
		$groupValidator = new GroupValidator($group, $this->getApp());
		$groupValidator->validate();
		if (!$groupValidator->hasErrors()) {
			$this->getApp()->getDb()->save($group);
			$this->ajaxParams['success'] = true;
		}

		$this->ajaxParams['notifications'] = $groupValidator->getNotifications();
	}

	public function methodGroupsMy(): void
	{
		/** @var Group[] $groups */
		$groups = $this->getApp()->getDb()->findBy(['userId' => $this->getApp()->getUser()->getId()], Group::class);

		foreach ($groups as $key => $group) {
			$groups[$key] = [
				'id' => $group->getId(),
				'name' => htmlspecialchars($group->getName()),
				'description' => htmlspecialchars($group->getDescription()),
				'private' => $group->isPrivate(),
				'urlEdit' => $this->getApp()->getRoute()->renderUrl('group_edit', ['groupId' => $group->getId()]),
				'url' => $this->getApp()->getRoute()->renderUrl('group', ['groupId' => $group->getId()]),
			];
		}

		$this->ajaxParams['myGroups'] = $groups;
	}

	public function methodGroups(): void
	{
		$this->ajaxParams['success'] = false;
		$this->ajaxParams['groups'] = [];

		$countryCode = $this->getApp()->getRequest()->getForApi('country', null, $this->getApp()->isDev());
		$cityId = (int)$this->getApp()->getRequest()->getForApi('city', null, $this->getApp()->isDev());
		$ccategoryId = (int)$this->getApp()->getRequest()->getForApi('category', null, $this->getApp()->isDev());

		$groupFilter = new GroupFilter();
		$groupFilter->setCountryCode($countryCode);
		$groupFilter->setCityId($cityId);
		$groupFilter->setCategoryId($ccategoryId);
		$groupFilterValidator = new GroupFilterValidator($groupFilter, $this->getApp());
		$groupFilterValidator->validate();
		if (!$groupFilterValidator->hasErrors()) {
			$this->ajaxParams['success'] = true;
			/** @var Group[] $groups */
			$groups = $this->getApp()->getDb()->findGroups($groupFilter);

			foreach ($groups as $key => $group) {
				$groups[$key] = [
					'id' => $group->getId(),
					'name' => htmlspecialchars($group->getName()),
					'description' => htmlspecialchars($group->getDescription()),
					'private' => $group->isPrivate(),
					'url' => $this->getApp()->getRoute()->renderUrl('group', ['groupId' => $group->getId()])
				];

				$this->ajaxParams['groups'] = $groups;
			}
		}

		$this->ajaxParams['notifications'] = $groupFilterValidator->getNotifications();
	}

	public function methodGroupJoin(): void
	{
		$this->ajaxParams['success'] = false;

		$groupId = (int)$this->getApp()->getRequest()->getForApi('groupId', null, $this->getApp()->isDev());
		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new \Exception('Group by id ' . $groupId . ' not found.');
		}

		$user = $this->getApp()->getUser();
		if ($user->getId() === $group->getUserId()) {
			return;
		}

		$groupValidator = new GroupValidator($group, $this->getApp());
		$iBlocked = (bool)$this->getApp()->getDb()->findOneBy(['userId' => $group->getUserId(), 'userIdBlocked' => $user->getId()], UserBlock::class);
		if ($iBlocked) {
			$groupValidator->addError('group', "You can't join this group. You are blocked for this group.");
		} else {
			/** @var GroupUser $groupUser */
			$groupUser = $this->getApp()->getDb()->findOneBy([
				'groupId' => $group->getId(),
				'userId' => $user->getId(),
			], GroupUser::class);

			if ($groupUser) {
				return;
			}

			$groupUser = new GroupUser($group, $user);
			$this->getApp()->getDb()->save($groupUser);
			$this->ajaxParams['success'] = true;
		}

		$this->ajaxParams['notifications'] = $groupValidator->getNotifications();
	}

	public function methodGroupLeave(): void
	{
		$this->ajaxParams['success'] = false;

		$groupId = (int)$this->getApp()->getRequest()->getForApi('groupId', null, $this->getApp()->isDev());
		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new \Exception('Group by id ' . $groupId . ' not found.');
		}

		$user = $this->getApp()->getUser();
		if ($user->getId() === $group->getUserId()) {
			return;
		}

		/** @var GroupUser $groupUser */
		$groupUser = $this->getApp()->getDb()->findOneBy([
			'groupId' => $group->getId(),
			'userId' => $user->getId(),
		], GroupUser::class);

		if (!$groupUser) {
			return;
		}

		$this->getApp()->getDb()->remove($groupUser);
		$this->ajaxParams['success'] = true;
	}

	public function methodGroupRequestAccept(): void
	{
		$this->ajaxParams['success'] = false;

		$groupId = (int)$this->getApp()->getRequest()->getForApi('groupId', null, $this->getApp()->isDev());
		$userId = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());

		$user = $this->getApp()->getUser();

		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new \Exception('Group by id ' . $groupId . ' not found.');
		}
		if ($group->getUserId() !== $user->getId()) {
			throw new \Exception('Access denied for accept.');
		}

		/** @var GroupUser $groupUser */
		$groupUser = $this->getApp()->getDb()->findOneBy([
			'groupId' => $group->getId(),
			'userId' => $userId,
			'approved' => false,
		], GroupUser::class);

		if (!$groupUser) {
			return;
		}

		$groupUser->setApproved(true);
		$groupUser->setApprovedUserId($user);
		$groupUser->setApprovedAt();

		$this->getApp()->getDb()->save($groupUser);
		$this->ajaxParams['success'] = true;
	}

	public function methodGroupRequestDecline(): void
	{
		$this->ajaxParams['success'] = false;

		$groupId = (int)$this->getApp()->getRequest()->getForApi('groupId', null, $this->getApp()->isDev());
		$userId = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());

		$user = $this->getApp()->getUser();

		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new \Exception('Group by id ' . $groupId . ' not found.');
		}
		if ($group->getUserId() !== $user->getId()) {
			throw new \Exception('Access denied for accept.');
		}

		/** @var GroupUser $groupUser */
		$groupUser = $this->getApp()->getDb()->findOneBy([
			'groupId' => $group->getId(),
			'userId' => $userId,
			'approved' => false,
		], GroupUser::class);

		if (!$groupUser) {
			return;
		}

		$this->getApp()->getDb()->remove($groupUser);
		$this->ajaxParams['success'] = true;
	}
}