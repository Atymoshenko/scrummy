<?php
namespace App\Api;

use App\Entity\User;
use App\Entity\UserFriend;
use App\Entity\UserBlock;
use App\Validator\UserValidator;

class UserApi extends Api {
	public function methodFriendAdd(): void
	{
		$this->ajaxParams['status'] = null;

		$userIdFriend = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());
		$user = $this->getApp()->getUser();
		if ($user->getId() === $userIdFriend) {
			throw new \Exception("You can't add yourself as a friend.");
		}

		/** @var User $userForFriend */
		$userForFriend = $this->getApp()->getDb()->findOneBy(['id' => $userIdFriend], User::class);
		if (!$userForFriend) {
			throw new \Exception('User by id ' . $userIdFriend . ' not found.');
		}

		$userValidator = new UserValidator($user, $this->getApp());
		$iBlocked = (bool)$this->getApp()->getDb()->findOneBy(['userIdBlocked' => $user->getId(), 'userId' => $userForFriend->getId()], UserBlock::class);
		if ($iBlocked) {
			$userValidator->addError('friend', "You can't add this user as friend. You are blocked.");
		}

		if (!$userValidator->hasErrors()) {
			/** @var UserFriend $userFriend */
			$userFriend = $this->getApp()->getDb()->getUserFriend($user, $userForFriend);
			if ($userFriend) {
				if ($userFriend->getApprovedAt()) {
					throw new \Exception("User {$userForFriend->getId()} already friend for {$user->getId()}.");
				}
				if ($userFriend->getUserId() === $user->getId()) {
					throw new \Exception("Request for user {$user->getId()} add friend {$userForFriend->getId()} already sent.");
				}
				$userFriend->setApprovedAt();
				$this->ajaxParams['status'] = UserFriend::STATUS_FRIEND;
			} else {
				$userFriend = new UserFriend($user, $userForFriend);
				$this->ajaxParams['status'] = UserFriend::STATUS_SENT;
				try {
					$this->getApp()->getMail()->sendFriendApprove($userForFriend, $user, $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $user->getId()]));
				} catch (\Throwable $e) {

				}
			}
			$this->getApp()->getDb()->save($userFriend);
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodFriendRemove(): void
	{
		$this->ajaxParams['status'] = null;

		$userIdFriend = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());
		$user = $this->getApp()->getUser();
		if ($user->getId() === $userIdFriend) {
			throw new \Exception("You can't add yourself as a friend.");
		}

		/** @var User $userForFriend */
		$userForFriend = $this->getApp()->getDb()->findOneBy(['id' => $userIdFriend], User::class);
		if (!$userForFriend) {
			throw new \Exception('User by id ' . $userIdFriend . ' not found.');
		}

		/** @var UserFriend $userFriend */
		$userFriend = $this->getApp()->getDb()->findOneBy([
			'userId' => $user->getId(),
			'userIdFriend' => $userForFriend->getId(),
		], UserFriend::class);
		if (!$userFriend) {
			$userFriend = $this->getApp()->getDb()->findOneBy([
				'userId' => $userForFriend->getId(),
				'userIdFriend' => $user->getId(),
			], UserFriend::class);
		}
		if (!$userFriend) {
			throw new \Exception("User {$userForFriend->getId()} is not friend for {$user->getId()}.");
		}

		$this->getApp()->getDb()->removeUserFriend($userFriend);

		$this->ajaxParams['status'] = UserFriend::STATUS_NOT_FRIEND;
	}

	public function methodBlockAdd(): void
	{
		$this->ajaxParams['status'] = null;

		$userIdBlock = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());
		$user = $this->getApp()->getUser();
		if ($user->getId() === $userIdBlock) {
			throw new \Exception("You can't block yourself.");
		}

		/** @var User $userForBlock */
		$userForBlock = $this->getApp()->getDb()->findOneBy(['id' => $userIdBlock], User::class);
		if (!$userForBlock) {
			throw new \Exception('User by id ' . $userIdBlock . ' not found.');
		}

		/** @var UserBlock $userBlock */
		$userBlock = $this->getApp()->getDb()->findOneBy([
			'userId' => $user->getId(),
			'userIdBlocked' => $userForBlock->getId(),
		], UserBlock::class);
		if ($userBlock) {
			throw new \Exception("User {$userForBlock->getId()} already blocked.");
		}

		$userBlock = new UserBlock($user, $userForBlock);
		$this->getApp()->getDb()->save($userBlock);
		$this->ajaxParams['status'] = UserBlock::STATUS_BLOCKED;
	}

	public function methodBlockRemove(): void
	{
		$this->ajaxParams['status'] = null;

		$userIdBlock = (int)$this->getApp()->getRequest()->getForApi('userId', null, $this->getApp()->isDev());
		$user = $this->getApp()->getUser();
		if ($user->getId() === $userIdBlock) {
			throw new \Exception("You can't unblock yourself.");
		}

		/** @var User $userForBlock */
		$userForBlock = $this->getApp()->getDb()->findOneBy(['id' => $userIdBlock], User::class);
		if (!$userForBlock) {
			throw new \Exception('User by id ' . $userIdBlock . ' not found.');
		}

		/** @var UserBlock $userBlock */
		$userBlock = $this->getApp()->getDb()->findOneBy([
			'userId' => $user->getId(),
			'userIdBlocked' => $userForBlock->getId(),
		], UserBlock::class);
		if (!$userBlock) {
			throw new \Exception("User {$userForBlock->getId()} is not blocked for {$user->getId()}.");
		}

		$this->getApp()->getDb()->remove($userBlock);

		$this->ajaxParams['status'] = UserBlock::STATUS_NOT_BLOCKED;
	}
}