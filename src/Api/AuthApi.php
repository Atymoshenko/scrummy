<?php
namespace App\Api;

use App\Entity\User;
use App\Entity\UserGoogle;
use App\Entity\UserProfile;
use App\Framework\App;
use App\Service\Manager\UserManager;
use App\Validator\UserProfileValidator;
use App\Validator\UserValidator;
use Firebase\JWT\JWT;

class AuthApi extends Api {
	public function methodSignup(): void
	{
		$this->ajaxParams['formSaved'] = false;
		$email = trim($this->getApp()->getRequest()->getForApi('email', '', $this->getApp()->isDev()));
		$nickname = trim($this->getApp()->getRequest()->getForApi('nickname', '', $this->getApp()->isDev()));
		$password = trim($this->getApp()->getRequest()->getForApi('password', '', $this->getApp()->isDev()));
		$sex = trim($this->getApp()->getRequest()->getForApi('sex', '', $this->getApp()->isDev()));
		$agree = trim($this->getApp()->getRequest()->getForApi('agree', 'N', $this->getApp()->isDev()));
		$hostByAddr = (string)gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$user = new User($email, $nickname, $password, $sex, $_SERVER['REMOTE_ADDR'], $hostByAddr, $_SERVER['HTTP_USER_AGENT'], $_SERVER['HTTP_ACCEPT_LANGUAGE']);
		$userValidator = new UserValidator($user, $this->getApp());
		$userValidator->validate();
		if ($agree === 'Y') {
			$userValidator->addSuccess('agree');
		} else {
			$userValidator->addError('agree', $this->getApp()->trans('To sign up, you must accept the privacy policy and rules.'));
		}
		if (!$userValidator->hasErrors()) {
			$user->setRoomId($this->getOrCreateDefaultRoom()->getId());
			try {
				$this->getApp()->getDb()->save($user);
				$userProfile = new UserProfile($user);
				$this->getApp()->getDb()->save($userProfile);
			} catch (\Throwable $e) {
				preg_match("~^Duplicate entry '(.+?)' for key '(.+?)'~su", $e->getMessage(), $matches);
				if (isset($matches[2])) {
					switch ($matches[2]) {
						case 'email':
							$userValidator->addError('email', $this->getApp()->trans('That email is taken, try another.'));
							break;
						case 'nickname':
							$userValidator->addError('nickname', $this->getApp()->trans('That nickname is taken, try another.'));
							break;
						default:
							throw $e;
					}
				} else {
					throw $e;
				}
			}
			if (!$userValidator->hasErrors()) {
				$this->ajaxParams['apiToken'] = $this->generateToken($user);
				$user->setApiToken($this->ajaxParams['apiToken']);
				$user->setApiTokenExpire(new \DateTime('now + 1 month'));
				$this->getApp()->getDb()->save($user);
				$this->ajaxParams['formSaved'] = true;
				$userManager = new UserManager($this->getApp(), $user);
				$this->ajaxParams['emailConfirmExpireText'] = $userManager->getEmailConfirmExpireText();
				try {
					$sent = $this->getApp()->getMail()->sendUserSignUp(
						$user,
						$this->getApp()->getRoute()->renderUrl('signup_confirm') . '?id=' . $user->getId() . '&code=' . $user->getEmailConfirmToken()
					);
					$user->setEmailConfirmSended($sent > 0 ? true : false);
					$this->getApp()->getDb()->save($user);
				} catch (\Throwable $e) {

				}
			}
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodSignUpConfirm(): void
	{
		$this->ajaxParams['emailConfirmSuccess'] = false;
		$id = trim($this->getApp()->getRequest()->getForApi('id', null, $this->getApp()->isDev()));
		$emailConfirmToken = trim($this->getApp()->getRequest()->getForApi('code', null, $this->getApp()->isDev()));
		$user = new User('', '', '', '', '', '', '', '');
		$userValidator = new UserValidator($user);
		if (!$id) {
			$userValidator->addError('code', $this->getApp()->trans('0-Wrong confirm url.'));
		}
		if (!$userValidator->hasErrors()) {
			/** @var User $user */
			$user = $this->getApp()->getDb()->findOneBy(['id' => $id], User::class);
			if (!$user) {
				$userValidator->addError('code', $this->getApp()->trans('1-Wrong confirm url.'));
			} else {
				$this->ajaxParams['apiToken'] = $user->getApiToken();
				if ($user->isEmailConfirmed()) {
					$userValidator->addError('code', $this->getApp()->trans('User email already confirmed.'));
				} elseif ($user->isEmailConfirmExpired()) {
					$this->getApp()->getDb()->remove($user);
					$this->getApp()->getMail()->sendUserDeletedByEmailConfirmExpired($user);
					$userValidator->addError('code', $this->getApp()->trans('Confirm code is expired.'));
					unset($user);
				}
			}
		}
		if (isset($user) && !$userValidator->hasErrors() && $user->getEmailConfirmToken() !== $emailConfirmToken) {
			$userValidator->addError('code', $this->getApp()->trans('Wrong email confirm code.'));
		}
		if (isset($user) && !$userValidator->hasErrors() && $user->getEmailConfirmToken() === $emailConfirmToken) {
			$user->setRoomId($this->getOrCreateDefaultRoom()->getId());
			$this->ajaxParams['apiToken'] = $this->generateToken($user);
			$user->setApiToken($this->ajaxParams['apiToken']);
			$user->setApiTokenExpire(new \DateTime('now + 1 month'));
			$user->setEmailConfirmed(true);
			$user->removeEmailConfirmToken();
			$user->removeEmailConfirmExpire();
			$this->getApp()->getDb()->save($user);
			$this->ajaxParams['emailConfirmSuccess'] = true;
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodSignin(): void
	{
		$login = trim($this->getApp()->getRequest()->getForApi('login', '', $this->getApp()->isDev()));
		$password = trim($this->getApp()->getRequest()->getForApi('password', '', $this->getApp()->isDev()));
		$newUser = new User('', '', $password, '', '', '', '', '');
		$userValidator = new UserValidator($newUser, $this->getApp());
		if (!$login) {
			$userValidator->addError('login', $this->getApp()->trans('Email or nickname is required.'));
		}
		$userValidator->validatePasswordRequire();
		if (!$userValidator->hasErrors()) {
			/** @var User $user */
			$user = $this->getApp()->getDb()->findOneBy(['email' => $login], User::class);
			if ($user) {
				if (User::generatePasswordHash($user->getEmail(), $user->getNickname(), $password, $user->getCreated()) !== $user->getPasswordHash()) {
					$userValidator->addError('login', $this->getApp()->trans('Wrong email or password.'));
				}
			} else {
				$userValidator->addError('login', $this->getApp()->trans('Wrong email or password.'));
			}
			if (!$userValidator->hasErrors()) {
				$user->setRoomId($this->getOrCreateDefaultRoom()->getId());
				$this->ajaxParams['apiToken'] = $this->generateToken($user);
				$user->setApiToken($this->ajaxParams['apiToken']);
				$user->setApiTokenExpire(new \DateTime('now + 1 month'));
				if (!$user->isEmailConfirmed() && !$user->isEmailConfirmSended()) {
					try {
						$user->setEmailConfirmExpire();
						$user->setEmailConfirmToken();
						$sent = $this->getApp()->getMail()->sendUserSignUp($user, $this->getApp()->getRoute()->renderUrl('signup_confirm', ['id' => $user->getId(), 'code' => $user->getEmailConfirmToken()]));
						$user->setEmailConfirmSended($sent > 0 ? true : false);
					} catch (\Throwable $e) {

					}
				}
				$this->getApp()->getDb()->save($user);
				$userProfileNew = new UserProfile($user);
				/** @var UserProfile $userProfile */
				$userProfile = $this->getApp()->getDb()->findOneByOrCreate(['userId' => $user->getId()], UserProfile::class, $userProfileNew);
				$this->ajaxParams['locale'] = $userProfile->getLocale();
			}
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodSigninGoogle(): void
	{
		$idToken = $this->getApp()->getRequest()->getFromPost('idToken', null);
		$nickname = $this->getApp()->getRequest()->getFromPost('nickname', '');
		$sex = $this->getApp()->getRequest()->getFromPost('sex', '');
		$agree = trim($this->getApp()->getRequest()->getForApi('agree', 'N', $this->getApp()->isDev()));

		$errors = [];

		JWT::$leeway+= 10;
		$client = new \Google_Client(['client_id' => $this->getApp()->getConfig()['googleClientId']]);  // Specify the CLIENT_ID of the app that accesses the backend
		$payload = $client->verifyIdToken($idToken);
		if (!$payload) {
			if ($this->getApp()->isDev()) {
				$errors[] = 'Invalid ID token';
			}
			// Invalid ID token
		} elseif ($payload['aud'] !== $this->getApp()->getConfig()['googleClientId']) {
			$errors[] = 'Invalid client id.';
		} elseif (!$payload['email']) {
			$errors[] = 'Google accound have not email.';
		}
		if (!$errors) {
			// If request specified a G Suite domain:
			//$domain = $payload['hd'];
			/** @var UserGoogle $userGoogle */
			$userGoogle = $this->getApp()->getDb()->findOneBy(['googleId' => $payload['sub']], UserGoogle::class);
			/** @var User $user */
			$user = $this->getApp()->getDb()->findOneBy(['email' => $payload['email']], User::class);
			if (!$user) {
				$this->ajaxParams['requireNickname'] = 1;
				$hostByAddr = (string)gethostbyaddr($_SERVER['REMOTE_ADDR']);
				$user = new User($payload['email'], $nickname, User::generatePassword(), $sex, $_SERVER['REMOTE_ADDR'], $hostByAddr, $_SERVER['HTTP_USER_AGENT'], $_SERVER['HTTP_ACCEPT_LANGUAGE']);
				if ($payload['email_verified']) {
					$user->setEmailConfirmed(true);
					$user->removeEmailConfirmToken();
					$user->removeEmailConfirmExpire();
				}
			}
			$userValidator = new UserValidator($user);
			$userValidator->validateNickname();
			$userValidator->validateSex();
			if (isset($this->ajaxParams['requireNickname']) && $this->ajaxParams['requireNickname']) {
				if ($agree === 'Y') {
					$userValidator->addSuccess('agree');
				} else {
					$userValidator->addError('agree', $this->getApp()->trans('To sign up, you must accept the privacy policy and rules.'));
				}
			}
			if (!$userValidator->hasErrors()) {
				if ($this->getApp()->getDb()->beginTransaction()) {
					if ($this->getApp()->getDb()->isNew($user)) {
						$user->setRoomId($this->getOrCreateDefaultRoom()->getId());
					}
					try {
						$this->getApp()->getDb()->save($user);
						/* BEGIN Save profile: */
						/** @var UserProfile $userProfile */
						$userProfile = $this->getApp()->getDb()->findOneBy(['userId' => $user->getId()], UserProfile::class);
						if (!$userProfile) {
							$userProfile = new UserProfile($user);
						}
						$userProfile->setLocales($this->getApp()->getLocales());
						$userProfileValidator = new UserProfileValidator($userProfile, $this->getApp());
						if (is_null($userProfile->getLocale()) && isset($payload['locale']) && $payload['locale']) {
							$userProfile->setLocale($payload['locale']);
							if (!$userProfileValidator->validateLocale()) {
								$userProfile->setLocale(null);
							} else {
								$this->ajaxParams['locale'] = $userProfile->getLocale();
							}
						}
						if (is_null($userProfile->getName()) && isset($payload['name']) && $payload['name']) {
							$userProfile->setName($payload['name']);
							if (!$userProfileValidator->validateName()) {
								$userProfile->setName(null);
							}
						}
						if (is_null($userProfile->getSurname()) && isset($payload['family_name']) && $payload['family_name']) {
							$userProfile->setSurname($payload['family_name']);
							if (!$userProfileValidator->validateSurname()) {
								$userProfile->setSurname(null);
							}
						}
						if (is_null($userProfile->getName()) && isset($payload['given_name']) && $payload['given_name']) {
							$userProfile->setName($payload['given_name']);
							if (!$userProfileValidator->validateName()) {
								$userProfile->setName(null);
							}
						}
						$this->getApp()->getDb()->save($userProfile);
						/* END Save profile. */
						if (!$userGoogle) {
							$userGoogle = new UserGoogle($user, $payload['sub'], json_encode($payload));
							$this->getApp()->getDb()->save($userGoogle);
						}
						$this->ajaxParams['apiToken'] = $this->generateToken($user);
						$user->setApiToken($this->ajaxParams['apiToken']);
						$user->setApiTokenExpire(new \DateTime('now + 1 month'));
						$user->setLastVisit();
						$this->getApp()->getDb()->save($user);
						$this->getApp()->getDb()->commit();
						if (!$user->isEmailConfirmed()) {
							try {
								$user->setEmailConfirmExpire();
								$user->setEmailConfirmToken();
								$sent = $this->getApp()->getMail()->sendUserSignUp($user, $this->getApp()->getRoute()->renderUrl('signup_confirm', ['id' => $user->getId(), 'code' => $user->getEmailConfirmToken()]));
								$user->setEmailConfirmSended($sent > 0 ? true : false);
								$this->getApp()->getDb()->save($user);
							} catch (\Throwable $e) {

							}
						}
					} catch (\Throwable $e) {
						unset($this->ajaxParams['apiToken']);
						$this->getApp()->getDb()->rollback();
						preg_match("~^Duplicate entry '(.+?)' for key '(.+?)'~su", $e->getMessage(), $matches);
						if (isset($matches[2])) {
							switch ($matches[2]) {
								case 'email':
									$userValidator->addError('email', $this->getApp()->trans('That email is taken, try another.'));
									break;
								case 'nickname':
									$userValidator->addError('nickname', $this->getApp()->trans('That nickname is taken, try another.'));
									break;
								default:
									throw $e;
							}
						} else {
							throw $e;
						}
					}
				} else {
					throw new \Exception('Error begin DB Transaction.');
				}
			}
		}

		$this->ajaxParams['notifications'] = isset($userValidator) ? $userValidator->getNotifications() : [];
	}

	public function methodSignout(): void
	{
		if ($this->getApp()->getUser()) {
			$this->getApp()->getUser()->removeApiToken();
			$this->getApp()->getUser()->removeLastMessageLoad();
			$this->getApp()->getDb()->save($this->getApp()->getUser());

			$this->ajaxParams['apiToken'] = null;
		}
	}
}