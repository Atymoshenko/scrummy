<?php
namespace App\Api;

use App\Entity\BaseMessage;
use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\Message;
use App\Entity\MessagePrivate;
use App\Entity\Room;
use App\Entity\User;
use App\Entity\UserBlock;
use App\Entity\UserFriend;
use App\Framework\App;
use App\Framework\Response;
use App\Validator\MessageValidator;

class ChatApi extends Api {
	private const MODE_CHAT = 'chat';
	private const MODE_GROUP = 'group';

	/** @var string */
	private $mode = 'chat';
	/** @var Group|null */
	private $group = null;

	public function __construct(App $app, Response $response = null)
	{
		parent::__construct($app, $response);

		$this->mode = $this->getApp()->getRequest()->getForApi('chatMode', self::MODE_CHAT, $this->getApp()->isDev());
		$groupId = (int)$this->getApp()->getRequest()->getForApi('groupId', null, $this->getApp()->isDev());

		if (!in_array($this->mode, [
			self::MODE_CHAT,
			self::MODE_GROUP,
		])) {
			throw new \Exception('Unknown chatMode.');
		}

		if ($this->isModeGroup()) {
			if (!$groupId) {
				throw new \Exception('groupId is required.');
			}
			$this->group = $this->getApp()->getDb()->findOneBy([
				'id' => $groupId,
			], Group::class);
			if (!$this->group) {
				throw new \Exception('Group with id ' . $groupId . ' not found.');
			}
			if ($this->group->getUserId() !== $this->getApp()->getUser()->getId()) {
				/** @var GroupUser|null $groupUser */
				$groupUser = $this->getApp()->getDb()->findOneBy([
					'groupId' => $this->group->getId(),
					'userId' => $this->getApp()->getUser()->getId(),
				], GroupUser::class);

				if (!$groupUser || ($this->group->isPrivate() && !$groupUser->isApproved())) {
					throw new \Exception('Access denied for userId ' . $this->getApp()->getUser()->getId() . ' to groupId ' . $this->group->getId() . '.');
				}
			}

			/** @var Room $room */
			$room = $this->getApp()->getDb()->findOneBy(['groupId' => $this->group->getId(),'default' => true], Room::class);
			if (!$room) { // @TODO: Remove this:
				$room = $this->getApp()->getDb()->findOneBy(['groupId' => $this->group->getId()], Room::class);
			}

			$this->getApp()->getUser()->setRoomId($room->getId());
			$this->getApp()->getDb()->save($this->getApp()->getUser());
		}
	}

	public function methodChatUsers(): void
	{
		/** @var User[] $users */
		$users = $this->getApp()->getDb()->findChatUserList($this->getApp()->getUser());
		$userIds = [];
		foreach ($users as $key => $user) {
			$userIds[$key] = $user->getId();
			$users[$key] = [
				'nickname' => $user->getNickname(),
				'sex' => $user->getSex(),
				'roomId' => $user->getRoomId(),
				'lastVisit' => $user->getLastVisit() ? $user->getLastVisit()->format('U') : null,
				'urlProfile' => $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $user->getId()]),
				'isFriend' => $user->isFriend(),
			];
		}

		$this->ajaxParams['users'] = $users;
	}

	public function methodChatMessages(): void
	{
		$user = $this->getApp()->getUser();

		$this->deleteOldMessages($user);

		$reload = (bool)$this->getApp()->getRequest()->getForApi('reload', false, $this->getApp()->isDev());
		$dateBeforeLoadMessage = new \DateTime();
		/** @var Message[] $messages */
		$messages = $this->getApp()->getDb()->findMessagesNew($user, $reload, BaseMessage::MAX_MESSAGES_PUBLIC_COUNT);
		$messages = array_reverse($messages);
		$userIds = [];
		foreach ($messages as $key => $message) {
			$userIds[$message->getUserId()] = $message->getUserId();
			if ($message->getUserIdTo()) {
				$userIds[$message->getUserIdTo()] = $message->getUserIdTo();
			}
			$messages[$key] = $message->toArray();
			$messages[$key]['message'] = htmlspecialchars($messages[$key]['message']);
			$messages[$key]['created'] = $message->getCreated()->format('U');
		}
		if (count($userIds) > 0) {
			/** @var User[] $users */
			$users = $this->getApp()->getDb()->findBy(['id' => $userIds], User::class, null, true);
			foreach ($messages as $key => $message) {
				$messages[$key]['nickname'] = $users[$message['userId']]->getNickname();
				if (isset($users[$message['userIdTo']])) {
					$messages[$key]['nicknameTo'] = $users[$message['userIdTo']]->getNickname();
				}
			}
		}

		/** @var MessagePrivate[] $messagesPrivate */
		$messagesPrivate = $this->getApp()->getDb()->findMessagesPrivateNew($user, $reload, BaseMessage::MAX_MESSAGES_PRIVATE_COUNT);
		$messagesPrivate = array_reverse($messagesPrivate);
		$userIds = [];
		foreach ($messagesPrivate as $key => $messagePrivate) {
			$userIds[$messagePrivate->getUserId()] = $messagePrivate->getUserId();
			$userIds[$messagePrivate->getUserIdTo()] = $messagePrivate->getUserIdTo();
			$messagesPrivate[$key] = $messagePrivate->toArray();
			$messagesPrivate[$key]['message'] = htmlspecialchars($messagesPrivate[$key]['message']);
			$messagesPrivate[$key]['created'] = $messagePrivate->getCreated()->format('U');
		}
		if (count($userIds) > 0) {
			/** @var User[] $users */
			$users = $this->getApp()->getDb()->findBy(['id' => $userIds], User::class, null, true);
			foreach ($messagesPrivate as $key => $messagePrivate) {
				$messagesPrivate[$key]['nickname'] = $users[$messagePrivate['userId']]->getNickname();
				$messagesPrivate[$key]['nicknameTo'] = $users[$messagePrivate['userIdTo']]->getNickname();
			}
		}

		if ($user->getLastMessageLoad()) {
			$user->setLifetimeFrom($user->getLastMessageLoad());
		}
		$user->setLastMessageLoad($dateBeforeLoadMessage);
		$this->getApp()->getDb()->save($user);

		$this->ajaxParams['messages'] = $messages;
		$this->ajaxParams['messagesPrivate'] = $messagesPrivate ?? [];
	}

	public function methodChatRooms(): void
	{
		/** @var Room[] $rooms */
		$rooms = $this->getApp()->getDb()->findChatRoomList($this->getApp()->getUser());
		foreach ($rooms as $key => $room) {
			$rooms[$key] = [
				'id' => $room->getId(),
				'name' => $this->getApp()->trans($room->getName(), [], 'table-room'),
				'active' => $this->getApp()->getUser()->getRoomId() === $room->getId(),
			];
		}

		$this->ajaxParams['rooms'] = $rooms;
	}

	public function methodChatMessage(): void
	{
		$user = $this->getApp()->getUser();

		$messageText = trim($this->getApp()->getRequest()->getForApi('message', '', $this->getApp()->isDev()));
		$sendPrivate = (bool) $this->getApp()->getRequest()->getForApi('sendPrivate', false, $this->getApp()->isDev());
		$message = new Message($user, $messageText, $user->getRoomId());
		$messageValidator = new MessageValidator($message, $this->getApp());
		list($userNicknameTo, $messageText) = $message->parseMessage($messageText);
		/** @var User $userTo */
		$userTo = null;
		if ($userNicknameTo) {
			$message->setMessage($messageText);
			$userTo = $this->getApp()->getDb()->findOneBy(['nickname' => $userNicknameTo], User::class);
			if ($userTo) {
				$message->setUserTo($userTo);
			} else {
				$messageValidator->addError('message', $this->getApp()->trans('User {{ nickname }} not found.', ['{{ nickname }}' => htmlspecialchars($userNicknameTo)]));
			}
		}
		if ($sendPrivate && !$userNicknameTo) {
			$messageValidator->addError('message', $this->getApp()->trans('Who to send?'));
		}

		$messageValidator->validate();

		if (!$messageValidator->hasErrors()) {
			if ($userTo) {
				$iBlocked = (bool)$this->getApp()->getDb()->findOneBy(['userId' => $userTo->getId(), 'userIdBlocked' => $user->getId()], UserBlock::class);
				if ($iBlocked) {
					$messageValidator->addError('message', "You can't send a message to this user. You are blocked.");
				}
			}
		}

		if (!$messageValidator->hasErrors()) {
			try {
				if ($sendPrivate) {
					$messagePrivate = new MessagePrivate($user, $messageText, $userTo);
					$this->getApp()->getDb()->save($messagePrivate);
				} else {
					$this->getApp()->getDb()->save($message);
				}
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		$this->ajaxParams['notifications'] = $messageValidator->getNotifications();
	}

	public function methodChatRoomSwitch(): void
	{
		$user = $this->getApp()->getUser();

		$roomId = (int)$this->getApp()->getRequest()->getForApi('roomId', null, $this->getApp()->isDev());
		if ($roomId) {
			/** @var Room $room */
			$room = $this->getApp()->getDb()->findOneBy(['id' => $roomId], Room::class);
			if ($room) {
				$user->setRoomId($room->getId());
				$this->getApp()->getDb()->save($user);
				$this->ajaxParams['roomId'] = $room->getId();
				$this->ajaxParams['mode'] = $room->getGroupId() ? self::MODE_GROUP : self::MODE_CHAT;
				$this->ajaxParams['groupId'] = $room->getGroupId() ?? null;
			} else {
				$this->ajaxParams['errors'][] = 'Room with id ' . $roomId . ' not found.';
			}
		} else {
			$this->ajaxParams['errors'][] = 'roomId required.';
		}
	}

	private function isModeGroup(): bool
	{
		return $this->mode === self::MODE_GROUP;
	}

	private function deleteOldMessages(User $user): void
	{
		$this->getApp()->getDb()->removeOldMessagesPublic($user, BaseMessage::MAX_MESSAGES_PUBLIC_COUNT);
		$this->getApp()->getDb()->removeOldMessagesPrivate($user, BaseMessage::MAX_MESSAGES_PRIVATE_COUNT);
	}
}