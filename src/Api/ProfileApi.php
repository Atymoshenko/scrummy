<?php
namespace App\Api;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Photo;
use App\Entity\PhotoData;
use App\Entity\User;
use App\Entity\UserPasswordReset;
use App\Entity\UserProfile;
use App\Framework\App;
use App\Framework\Image;
use App\Service\Manager\UserManager;
use App\Validator\Entity\ProfileFilter;
use App\Validator\Filter\ProfileFilterValidator;
use App\Validator\ForgotPasswordValidator;
use App\Validator\PhotoValidator;
use App\Validator\UserProfileValidator;
use App\Validator\UserValidator;

class ProfileApi extends Api {
	public function methodProfileEditProfile(): void
	{
		$user = $this->getApp()->getUser();

		$this->ajaxParams['formSaved'] = false;
		$name = trim($this->getApp()->getRequest()->getForApi('name', null, $this->getApp()->isDev()));
		$surname = trim($this->getApp()->getRequest()->getForApi('surname', null, $this->getApp()->isDev()));
		$countryCode = trim($this->getApp()->getRequest()->getForApi('countryCode', null, $this->getApp()->isDev()));
		$cityId = (int)($this->getApp()->getRequest()->getForApi('city', null, $this->getApp()->isDev()));
		$locale = trim($this->getApp()->getRequest()->getForApi('locale', null, $this->getApp()->isDev()));
		/** @var UserProfile $userProfile */
		$userProfile = $this->getApp()->getDb()->findOneBy(['userId' => $user->getId()], UserProfile::class);
		$profileValidator = new UserProfileValidator($userProfile, $this->getApp());
		$userProfile->setName($name);
		$userProfile->setSurname($surname);
		$userProfile->setLocales($this->getApp()->getLocales());
		$userProfile->setLocale($locale);

		if ($userProfile->getGroupCount() === 0) {
			$country = null;
			$city = null;
			/** @var Country $country */
			$country = $this->getApp()->getDb()->findOneBy(['code' => $countryCode], Country::class);
			if ($country) {
				$profileValidator->addSuccess('countryCode');
				/** @var City $city */
				$city = $this->getApp()->getDb()->findOneBy(['countryCode' => $country->getCode(), 'id' => $cityId], City::class);
				if ($city) {
					$profileValidator->addSuccess('city');
				}
			}
			$userProfile->setCountry($country);
			$userProfile->setCity($city);
		}

		$profileValidator->validateName();
		$profileValidator->validateSurname();
		if ($profileValidator->validateLocaleRequire()) {
			$profileValidator->validateLocale();
		}
		if (!$profileValidator->hasErrors()) {
			$this->ajaxParams['locale'] = $userProfile->getLocale();
			$this->ajaxParams['profile']['cityId'] = $userProfile->getCityId();
			$this->getApp()->getDb()->save($userProfile);
			$this->ajaxParams['formSaved'] = true;
		}

		$this->ajaxParams['notifications'] = $profileValidator->getNotifications();
	}

	public function methodProfileEditPassword(): void
	{
		$user = $this->getApp()->getUser();

		$this->ajaxParams['formSaved'] = false;
		$passwordNew = trim($this->getApp()->getRequest()->getForApi('password', '', $this->getApp()->isDev()));
		$needSave = false;
		$userValidator = new UserValidator($user);
		if ($passwordNew !== '') {
			$needSave = true;
			$user->setPassword($passwordNew);
			$user->setPasswordHash();
			$userValidator->validatePasswordNew();
		}

		if ($needSave) {
			if (!$userValidator->hasErrors()) {
				$this->getApp()->getDb()->save($user);
				$this->ajaxParams['formSaved'] = true;
			}
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodProfilePhotoUpload(): void
	{
		foreach ($_FILES as $fieldName => $fieldData) {
			if (is_array($fieldData['name'])) {
				foreach ($fieldData['name'] as $fileIndex => $value) {
					$fileName = $fieldData['name'][$fileIndex];
					$fileType = $fieldData['type'][$fileIndex];
					$fileTmpName = $fieldData['tmp_name'][$fileIndex];
					$fileError = $fieldData['error'][$fileIndex];
					$fileSize = $fieldData['size'][$fileIndex];
					$this->_processUploadPhoto($fileName, $fileType, $fileTmpName, $fileError, $fileSize, $this->getApp()->getConfig()['userPhoto']);
				}
			} else {
				$fileName = $fieldData['name'];
				$fileType = $fieldData['type'];
				$fileTmpName = $fieldData['tmp_name'];
				$fileError = $fieldData['error'];
				$fileSize = $fieldData['size'];
				$this->_processUploadPhoto($fileName, $fileType, $fileTmpName, $fileError, $fileSize, $this->getApp()->getConfig()['userPhoto']);
			}
		}
	}

	public function methodProfilePhotoDelete(): void
	{
		$this->ajaxParams['success'] = false;

		$user = $this->getApp()->getUser();
		$photoId = $this->getApp()->getRoute()->getRouteParams('photoId');
		/** @var Photo $photo */
		$photo = $this->getApp()->getDb()->findOneBy(['id' => $photoId], Photo::class);
		if (!$photo || $photo->getUserId() !== $user->getId()) {
			return;
		}

		$folderSuffix = $this->getApp()->getPathUserPhotosRelative($user->getId());
		$pathFile = $folderSuffix . $photo->getId() . '.jpg';
		$pathFileThumb = $folderSuffix . 'thumb_' . $photo->getId() . '.jpg';
		$this->getApp()->getDb()->beginTransaction();
		$this->getApp()->getDb()->remove($photo);
		unlink($pathFile);
		unlink($pathFileThumb);
		$this->getApp()->getDb()->commit();

		$this->ajaxParams['success'] = true;
	}

	private function _processUploadPhoto(
		string $fileName,
		string $fileType,
		string $fileTmpName,
		string $fileError,
		string $fileSize,
		array $config
	)
	{
		$this->ajaxParams['files'] = [];

		$user = $this->getApp()->getUser();
		$photo = new Photo($user, '');
		/** @var Photo[] $photos */
		$photos = $this->getApp()->getDb()->findBy(['userId' => $user->getId()], Photo::class);
		$photoValidator = new PhotoValidator($photo, $this->getApp(), $fileTmpName, $fileType, $fileSize);
		$photosCount = count($photos);
		if ($photosCount >= $config['maxCount']) {
			$photoValidator->addError('file', 'Maximum number of files exceeded.');
		}
		if (!$photoValidator->hasErrors()) {
			$photo->setHash(md5_file($fileTmpName));

			if ($fileError) {
				switch ($fileError) {
					case '4':
						$photoValidator->addError('file', $this->getApp()->trans('No file selected.'));
						break;
					default:
						$photoValidator->addError('file', $this->getApp()->trans('Unknown file upload error.'));
				}
			} else {
				$photoValidator->validate();
			}
		}

		if (!$photoValidator->hasErrors()) {
			$folderSuffix = $this->getApp()->getPathUserPhotosRelative($user->getId());
			$folder = $this->getApp()->getPathStatic() . $folderSuffix;
			$this->getApp()->getDb()->beginTransaction();
			try {
				$image = new Image($fileTmpName);
				$size = $image->getSize();
				if ($size['width'] < $config['minWidth'] || $size['height'] < $config['minWidth']) {
					$photoValidator->addError('file', 'Minimum image resolution should be {{ width }}x{{ height }} pixels.', [
						'width' => $config['minWidth'],
						'height' => $config['minHeight'],
					]);
				}
				if (!$photoValidator->hasErrors()) {
					$exifData = exif_read_data($fileTmpName);
					$pathTmpFile = $folder . basename($fileTmpName);
					$pathTmpFileThumb = $folder . 'thumb_' . basename($fileTmpName);
					$image->resize($config['maxWidth'], $config['maxHeight'], $config['quality'], $pathTmpFile, $exifData);
					$image->resize($config['thumbMaxWidth'], $config['thumbMaxHeight'], $config['thumbQuality'], $pathTmpFileThumb, $exifData);
					$this->getApp()->getDb()->save($photo);
					$pathFile = $folder . $photo->getId() . '.jpg';
					$pathFileThumb = $folder . 'thumb_' . $photo->getId() . '.jpg';
					if (!rename($pathTmpFile, $pathFile)) {
						unlink($pathTmpFile);
						throw new \Exception("Can't rename file: " . $pathTmpFile . ' to: ' . $pathFile);
					}
					if (!rename($pathTmpFileThumb, $pathFileThumb)) {
						unlink($pathTmpFileThumb);
						unlink($pathFile);
						throw new \Exception("Can't rename file: " . $pathTmpFileThumb . ' to: ' . $pathFileThumb);
					}
					$image = new Image($pathFile);
					$imageThumb = new Image($pathFileThumb);
					$size = $image->getSize();
					$sizeThumb = $imageThumb->getSize();
					$photoData = new PhotoData($photo, json_encode([
						'filename' => $fileName,
						'sizeAfterResize' => $size,
						'sizeThumb' => $sizeThumb,
						'exifData' => $exifData,
					]));
					$this->getApp()->getDb()->save($photoData);
					$this->getApp()->getDb()->commit();

					$this->ajaxParams['files'][] = [
						'name' => htmlspecialchars($photo->getName()),
						'size' => $fileSize,
						'url' => $folderSuffix . basename($pathFile),
						'thumbnailUrl' => $folderSuffix . basename($pathFileThumb),
						'deleteUrl' => $this->getApp()->getRoute()->renderUrl('apiProfilePhotoDelete', ['photoId' => $photo->getId()]),
						'deleteType' => "DELETE",
					];
				}
			} catch (\Exception $e) {
				$this->getApp()->getDb()->rollback();
				if (isset($pathTmpFile) && is_file($pathTmpFile)) {
					unlink($pathTmpFile);
				}
				if (isset($pathTmpFileThumb) && is_file($pathTmpFileThumb)) {
					unlink($pathTmpFileThumb);
				}
				if (isset($pathFile) && is_file($pathFile)) {
					unlink($pathFile);
				}
				if (isset($pathFileThumb) && is_file($pathFileThumb)) {
					unlink($pathFileThumb);
				}
				if ($this->getApp()->isDev()) {
					$photoValidator->addError('file', $e->getMessage());
				} else {
					$photoValidator->addError('file', "Can't upload this file.");
				}
			}
		}
		if ($photoValidator->hasErrors()) {
			$this->ajaxParams['files'][] = [
				'name' => $fileName,
				'size' => $fileSize,
				'error' => $photoValidator->getError(),
			];
		}

		$this->ajaxParams['notifications'] = array_merge($this->ajaxParams['notifications'] ?? [], $photoValidator->getNotifications());
	}

	public function methodProfilePhotos(): void
	{
		$user = $this->getApp()->getUser();
		$folderSuffix = $this->getApp()->getPathUserPhotosRelative($user->getId());
		$this->ajaxParams['files'] = [];

		/** @var Photo[] $photos */
		$photos = $this->getApp()->getDb()->findBy(['userId' => $user->getId()], Photo::class);
		foreach ($photos as $photo) {
			$pathFile = $folderSuffix . $photo->getId() . '.jpg';
			$pathFileThumb = $folderSuffix . 'thumb_' . $photo->getId() . '.jpg';

			$this->ajaxParams['files'][] = [
				'name' => htmlspecialchars($photo->getName()),
				'size' => '',
				'url' => $pathFile,
				'thumbnailUrl' => $pathFileThumb,
				'deleteUrl' => $this->getApp()->getRoute()->renderUrl('apiProfilePhotoDelete', ['photoId' => $photo->getId()]),
				'deleteType' => "POST",
			];
		}
	}

	public function methodLoadProfile(): void
	{
		$user = $this->getApp()->getUser();
		/** @var UserProfile $profile */
		$profile = $this->getApp()->getDb()->findOneByOrCreate(['userId' => $user->getId()], UserProfile::class, new UserProfile($user));

		$this->ajaxParams['user']['nickname'] = $user->getNickname();
		$this->ajaxParams['user']['sex'] = $user->getSex();
		$this->ajaxParams['user']['roomId'] = $user->getRoomId();
		$this->ajaxParams['user']['lastMessageLoad'] = $user->getLastMessageLoad() ? $user->getLastMessageLoad()->format('U') : null;
		$this->ajaxParams['user']['created'] = $user->getCreated()->format('U');
		$this->ajaxParams['user']['email'] = $user->getEmail();
		$this->ajaxParams['user']['emailConfirmed'] = $user->isEmailConfirmed();
		$userManager = new UserManager($this->getApp(), $user);
		$this->ajaxParams['user']['emailConfirmExpireText'] = $userManager->getEmailConfirmExpireText();
		$this->ajaxParams['profile']['locale'] = $profile->getLocale();
		$this->ajaxParams['profile']['name'] = $profile->getName();
		$this->ajaxParams['profile']['surname'] = $profile->getSurname();
		$this->ajaxParams['profile']['countryCode'] = $profile->getCountryCode();
	}

	public function methodProfilePasswordForgot(): void
	{
		$this->ajaxParams['success'] = false;
		$login = trim($this->getApp()->getRequest()->getForApi('login', null, $this->getApp()->isDev()));
		$user = new User('', '', '', '', '', '', '', '');
		$forgotPasswordValidator = new ForgotPasswordValidator($user, $this->getApp());
		if (!$login) {
			$forgotPasswordValidator->addError('login', $this->getApp()->trans('Email or nickname is required.'));
		}
		if (!$forgotPasswordValidator->hasErrors()) {
			if (strstr($login, '@')) {
				/** @var User $user */
				$user = $this->getApp()->getDb()->findOneBy(['email' => $login], User::class);
			} else {
				/** @var User $user */
				$user = $this->getApp()->getDb()->findOneBy(['nickname' => $login], User::class);
			}
			if (!$user) {
				$forgotPasswordValidator->addError('login', 'User not found.');
			} else {
				$forgotPasswordValidator->addSuccess('login');
				/** @var UserPasswordReset $userPasswordReset */
				$userPasswordReset = $this->getApp()->getDb()->findOneBy(['userId' => $user->getId()], UserPasswordReset::class);
				if (!$userPasswordReset) {
					$userPasswordReset = new UserPasswordReset($user);
				}
				$userPasswordReset->setCode();
				try {
					$this->getApp()->getDb()->save($userPasswordReset);
					$this->getApp()->getMail()->sendUserPasswordReset(
						$user,
						$userPasswordReset,
						$this->getApp()->getRoute()->renderUrl('password_forgot_confirm') . '?id=' . $userPasswordReset->getUserId() . '&code=' . $userPasswordReset->getCode()
					);
					$this->ajaxParams['success'] = true;
				} catch (\Throwable $e) {
					$forgotPasswordValidator->addError('login', $this->getApp()->trans('Failed to send instructions, try in a few minutes.'));
					if ($this->getApp()->isDev()) {
						$forgotPasswordValidator->addError('login', $e->getFile() . '(' . $e->getLine() . ') ' . $e->getMessage());
					}
				}
			}
		}

		$this->ajaxParams['notifications'] = $forgotPasswordValidator->getNotifications();
	}

	public function methodProfilePasswordReset(): void
	{
		$this->ajaxParams['success'] = false;
		$userId = trim($this->getApp()->getRequest()->getForApi('id', null, $this->getApp()->isDev()));
		$code = trim($this->getApp()->getRequest()->getForApi('code', null, $this->getApp()->isDev()));
		$password = trim($this->getApp()->getRequest()->getForApi('password', null, $this->getApp()->isDev()));
		$user = new User('', '', $password, '', '', '', '', '');
		$userValidator = new UserValidator($user);
		if (!$userId || !$code) {
			$userValidator->addError('password', 'Wrong url.');
		} else {
			/** @var UserPasswordReset $userPasswordReset */
			$userPasswordReset = $this->getApp()->getDb()->findOneBy(['userId' => $userId], UserPasswordReset::class);
			if (!$userPasswordReset) {
				$userValidator->addError('password', $this->getApp()->trans('Wrong url or expired.'));
			}
			if ($userPasswordReset && $userPasswordReset->isExpired()) {
				$this->getApp()->getDb()->remove($userPasswordReset);
				$userPasswordReset = null;
				$userValidator->addError('password', $this->getApp()->trans('Url is expired.'));
			}
			if (!$userValidator->hasErrors()) {
				/** @var User $user */
				$user = $this->getApp()->getDb()->findOneBy(['id' => $userId], User::class);
				if (!$user) {
					$userValidator->addError('password', $this->getApp()->trans('User not found.'));
				} else {
					$userValidator->validatePassword();
				}
			}
			if (!$userValidator->hasErrors()) {
				$user->setPassword($password);
				$user->setPasswordHash();
				try {
					$this->getApp()->getDb()->save($user);
					$this->ajaxParams['success'] = true;
					$this->getApp()->getDb()->remove($userPasswordReset);
				} catch (\Throwable $e) {
					$userValidator->addError('password', $this->getApp()->trans('Technical error.'));
				}
			}
		}

		$this->ajaxParams['notifications'] = $userValidator->getNotifications();
	}

	public function methodProfiles(): void
	{
		$this->ajaxParams['success'] = false;
		$this->ajaxParams['profiles'] = [];

		$countryCode = (string)$this->getApp()->getRequest()->getForApi('countryCode', null, $this->getApp()->isDev());
		$cityId = (int)$this->getApp()->getRequest()->getForApi('cityId', null, $this->getApp()->isDev());
		$offset = (int)$this->getApp()->getRequest()->getForApi('offset', 0, $this->getApp()->isDev());
		$limit = (int)$this->getApp()->getRequest()->getForApi('limit', 10, $this->getApp()->isDev());

		$profileFilter = new ProfileFilter();
		$profileFilter->setCountryCode($countryCode);
		$profileFilter->setCityId($cityId);
		$profileFilterValidator = new ProfileFilterValidator($profileFilter, $this->getApp());
		$profileFilterValidator->validate();
		$users = [];
		if (!$profileFilterValidator->hasErrors()) {
			$this->ajaxParams['success'] = true;
			$users = $this->getApp()->getDb()->findUsers($profileFilter, $offset, $limit);

			foreach ($users as $key => $user) {
				$thumbUrl = 'img/u_medium_' . $user['sex'] . '.jpg';
				if ($user['photoId']) {
					$thumbUrl = $this->getApp()->getPathUserPhotosRelative($user['id']) . 'thumb_' . $user['photoId'] . '.jpg';
				}
				$users[$key] = [
					'nickname' => htmlspecialchars($user['nickname']),
					'url' => $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $user['id']]),
					'name' => htmlspecialchars($user['name']),
					'surname' => htmlspecialchars($user['surname']),
					'thumbUrl' => $thumbUrl,
				];

			}
		}

		$this->ajaxParams['users'] = $users;
		$this->ajaxParams['notifications'] = $profileFilterValidator->getNotifications();
	}

	public function methodPhotos(): void
	{
		$this->ajaxParams['success'] = false;
		$this->ajaxParams['photoList'] = [];

		$userId = $this->getApp()->getRoute()->getRouteParams('id');
		$userProfile = $this->getApp()->getDb()->getUserProfile($userId);
		/** @var Photo[] $photos */
		$photos = $this->getApp()->getDb()->findBy(['userId' => $userProfile->getUserId()], Photo::class);
		$folderSuffix = $this->getApp()->getPathUserPhotosRelative($userProfile->getUserId());

		foreach ($photos as $photo) {
			$pathFile = $folderSuffix . $photo->getId() . '.jpg';
			$pathFileThumb = $folderSuffix . 'thumb_' . $photo->getId() . '.jpg';

			$this->ajaxParams['photoList'][] = [
				'id' => $photo->getId(),
				'name' => htmlspecialchars($photo->getName()),
				'url' => $pathFile,
				'thumbnailUrl' => $pathFileThumb,
			];
		}

		$this->ajaxParams['success'] = true;
	}
}