<?php
namespace App\Api;

use App\Entity\Photo;
use App\Entity\User;
use App\Entity\UserProfileComment;
use App\Framework\Db;
use App\Validator\Entity\CommentListFilter;
use App\Validator\Filter\CommentListFilterValidator;
use App\Validator\Filter\UserProfileCommentValidator;

class CommentApi extends Api {
	public function methodCommentCreate(): void
	{
		$this->ajaxParams['success'] = false;
		$this->ajaxParams['comment'] = null;

		$object = (string)$this->getApp()->getRequest()->getForApi('object', null, $this->getApp()->isDev());
		$objectId = (string)$this->getApp()->getRequest()->getForApi('objectId', null, $this->getApp()->isDev());
		$commentListFilter = new CommentListFilter();
		$commentListFilter->setObject($object);
		$commentListFilter->setObjectId($objectId);
		$commentListFilterValidator = new CommentListFilterValidator($commentListFilter, $this->getApp());
		$commentListFilterValidator->validate();
		if ($commentListFilterValidator->hasErrors()) {
			$this->ajaxParams['notifications'] = $commentListFilterValidator->getNotifications();
		} else {
			$content = trim($this->getApp()->getRequest()->getForApi('content', '', $this->getApp()->isDev()));
			$content = $this->normalizeContent($content);
			switch ($commentListFilter->getObject()) {
				case CommentListFilter::OBJECT_USER_PROFILE:
					/** @var User|null $userComment */
					$userComment = $this->getApp()->getDb()->findOneBy(['id' => $objectId], User::class);
					if (!$userComment) {
						throw new \Exception("User with id {$objectId} not found.");
					}

					$userProfileComment = new UserProfileComment($userComment, $this->getApp()->getUser(), $content);
					$commentValidator = new UserProfileCommentValidator($userProfileComment, $this->getApp());
					$commentValidator->validate();
					if (!$commentValidator->hasErrors()) {
						$this->getApp()->getDb()->save($userProfileComment);
						try {
							$this->getApp()->getMail()->createdUserProfileComment($userComment, $this->getApp()->getUser(), $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $userComment->getId()]));
						} catch (\Throwable $e) {

						}
						/** @var Photo|null $photo */
						$photo = $this->getApp()->getDb()->findOneBy(['userId' => $this->getApp()->getUser()->getId()], Photo::class);
						$thumbUrl = 'img/u_medium_' . $this->getApp()->getUser()->getSex() . '.jpg';
						if ($photo && $photo->getId()) {
							$thumbUrl = $this->getApp()->getPathUserPhotosRelative($this->getApp()->getUser()->getId()) . 'thumb_' . $photo->getId() . '.jpg';
						}
						$this->ajaxParams['comment'] = [
							'thumbUrl' => $thumbUrl,
							'createdAt' => $userProfileComment->getCreatedAt()->format('U'),
							'content' => $this->prepareContent($userProfileComment->getContent()),
							'nickname' => htmlspecialchars($this->getApp()->getUser()->getNickname()),
							'urlProfile' => $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $this->getApp()->getUser()->getId()]),
							'btnEdit' => '<button type="button" class="btn btn-warning btn-sm ml-2" data-action="edit" data-url="' . $this->getApp()->getRoute()->renderUrl('apiCommentUpdate', ['commentId' => $userProfileComment->getId()]) . '">' . $this->getApp()->trans('Edit') . '</button>',
							'btnRemove' => '<button type="button" class="btn btn-danger btn-sm ml-2" data-action="remove" data-url="' . $this->getApp()->getRoute()->renderUrl('apiCommentRemove', ['commentId' => $userProfileComment->getId()]) . '">' . $this->getApp()->trans('Remove') . '</button>',
						];

						$this->ajaxParams['success'] = true;
					}
					$this->ajaxParams['notifications'] = $commentValidator->getNotifications();
					break;
			}
		}
	}

	public function methodComments(): void
	{
		$this->ajaxParams['success'] = false;

		$object = (string)$this->getApp()->getRequest()->getForApi('object', null, $this->getApp()->isDev());
		$objectId = (string)$this->getApp()->getRequest()->getForApi('objectId', null, $this->getApp()->isDev());
		$offset = (int)$this->getApp()->getRequest()->getForApi('offset', 0, $this->getApp()->isDev());
		$limit = (int)$this->getApp()->getRequest()->getForApi('limit', 10, $this->getApp()->isDev());

		$commentListFilter = new CommentListFilter();
		$commentListFilter->setObject($object);
		$commentListFilter->setObjectId($objectId);
		$commentListFilterValidator = new CommentListFilterValidator($commentListFilter, $this->getApp());
		$commentListFilterValidator->validate();
		$commentList = [];
		if (!$commentListFilterValidator->hasErrors()) {
			$this->ajaxParams['success'] = true;
			switch ($commentListFilter->getObject()) {
				case CommentListFilter::OBJECT_USER_PROFILE:
					$commentList = $this->getApp()->getDb()->getUserProfileComments((int)$commentListFilter->getObjectId(), $offset, $limit);
					foreach ($commentList as $key => $comment) {
						$thumbUrl = 'img/u_medium_' . $comment['sex'] . '.jpg';
						if ($comment['photoId']) {
							$thumbUrl = $this->getApp()->getPathUserPhotosRelative($comment['userIdComment']) . 'thumb_' . $comment['photoId'] . '.jpg';
						}
						$createdAt = \DateTime::createFromFormat(Db::DB_FORMAT_DATE_TIME, $comment['createdAt']);

						$btnEdit = $btnRemove = '';
						if ((int)$comment['userIdComment'] === $this->getApp()->getUser()->getId()) {
							$btnEdit = '<button type="button" class="btn btn-warning btn-sm ml-2" data-action="edit" data-url="' . $this->getApp()->getRoute()->renderUrl('apiCommentUpdate', ['commentId' => $comment['id']]) . '">' . $this->getApp()->trans('Edit') . '</button>';
						}
						if ((int)$comment['userIdComment'] === $this->getApp()->getUser()->getId() || (int)$comment['userId'] === $this->getApp()->getUser()->getId()) {
							$btnRemove = '<button type="button" class="btn btn-danger btn-sm ml-2" data-action="remove" data-url="' . $this->getApp()->getRoute()->renderUrl('apiCommentRemove', ['commentId' => $comment['id']]) . '">' . $this->getApp()->trans('Remove') . '</button>';
						}
						$commentList[$key] = [
							'thumbUrl' => $thumbUrl,
							'createdAt' => $createdAt->format('U'),
							'nickname' => htmlspecialchars($comment['nickname']),
							'urlProfile' => $this->getApp()->getRoute()->renderUrl('profile', ['userId' => $comment['userIdComment']]),
							'content' => $this->prepareContent($comment['content']),
							'btnEdit' => $btnEdit,
							'btnRemove' => $btnRemove,
						];
					}
					break;
			}
		}

		$this->ajaxParams['commentList'] = $commentList;
		$this->ajaxParams['notifications'] = $commentListFilterValidator->getNotifications();
	}

	public function methodCommentRemove(): void
	{
		$this->ajaxParams['success'] = false;

		$object = (string)$this->getApp()->getRequest()->getForApi('object', null, $this->getApp()->isDev());
		$objectId = (string)$this->getApp()->getRequest()->getForApi('objectId', null, $this->getApp()->isDev());
		$commentId = $this->getApp()->getRoute()->getRouteParams('commentId');
		$commentListFilter = new CommentListFilter();
		$commentListFilter->setObject($object);
		$commentListFilter->setObjectId($objectId);
		$commentListFilterValidator = new CommentListFilterValidator($commentListFilter, $this->getApp());
		$commentListFilterValidator->validate();
		if ($commentListFilterValidator->hasErrors()) {
			$this->ajaxParams['notifications'] = $commentListFilterValidator->getNotifications();
		} else {
			/** @var UserProfileComment|null $userProfileComment */
			$userProfileComment = $this->getApp()->getDb()->findOneBy(['id' => $commentId], UserProfileComment::class);
			if (!$userProfileComment) {
				throw new \Exception("Comment by {$commentId} not found.");
			}
			if ($userProfileComment->getUserId() !== $this->getApp()->getUser()->getId() && $userProfileComment->getUserIdComment() !== $this->getApp()->getUser()->getId()) {
				throw new \Exception("User id {$this->getApp()->getUser()->getId()}  has no right to delete comment with id {$userProfileComment->getId()}.");
			}
			$this->getApp()->getDb()->remove($userProfileComment);
			$this->ajaxParams['success'] = true;
		}
	}

	private function normalizeContent(string $content): string
	{
		$content = preg_replace("~^[ \f\t]?(.*?)[ \f\t]?$~m", "$1", $content);
		$content = preg_replace("~^[ \f\t]+$~m", "", $content);
		$content = preg_replace("~(\r?\n){3,}~m", "$1$1", $content);

		return $content;
	}

	private function prepareContent(string $content): string
	{
		$content = htmlspecialchars($content);
		$content = preg_replace("~\r?\n~", "<br />", $content);

		return $content;
	}
}