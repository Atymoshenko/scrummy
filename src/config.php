<?php

use App\Framework\Db;

return array_merge(
	[
		'env' => /*in_array($_SERVER['REMOTE_ADDR'], ['176.36.20.111']) ? 'dev' : */'prod',
		'db_host' => 'db22.freehost.com.ua',
		'db_user' => 'school154_chat',
		'db_password' => 'kR42uYg9U',
		'db_name' => 'school154_chat'
	]
	, in_array($_SERVER['HTTP_HOST'] ,['vws-local.scrummy','local.scrummy.net']) ? [
		'env' => 'dev',
		'db_host' => '127.0.0.1',
		'db_user' => 'vws-chat',
		'db_password' => '123456',
		'db_name' => 'vws-chat-v2'
		] : []
	, $_SERVER['HTTP_HOST'] === 'dev.scrummy.net' ? [
		'env' => 'dev',
		'db_host' => 'db22.freehost.com.ua',
		'db_user' => 'school154_chtdev',
		'db_password' => '6Zz430bD',
		'db_name' => 'school154_chatdev'
	] : []
	,
	[
		'localeDefault' => 'en',
		'templateDefault' => 'Bootstrap',
		'mailer' => [
			'transport' => 'smtp',
			'host' => 'mail.scrummy.net',
			'port' => 25,
			'user' => 'noreply@scrummy.net',
			'password' => 'nSLgx5sZe',
			'from_email' => 'noreply@scrummy.net',
			'from_name' => 'Scrummy',
		],
		'googleClientId' => '663787509002-3haa12d8r9fqbohgvc33ki0tcvcjbsp6.apps.googleusercontent.com',
		'googleClientSecret' => 'aRnyV0uKvp5QU_ish-vDKfRG',
		'userPhoto' => [
			'maxWidth' => 1200,
			'maxHeight' => 1200,
			'minWidth' => 300,
			'minHeight' => 300,
			'quality' => 80,
			'thumbMaxWidth' => 300,
			'thumbMaxHeight' => 300,
			'thumbQuality' => 80,
			'maxCount' => 7,
		],
		'entity' => [
			'App\Entity\User' => [
				'tableName' => 'user',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'unique' => [
					'email' => ['email'],
					'nickname' => ['nickname'],
				],
				'key' => [
					'lastMessageLoad' => ['lastMessageLoad'],
				],
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
						'nullable' => false,
					],
					'email' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'unique' => true,
					],
					'emailConfirmed' => [
						'type' => Db::DB_TYPE_BOOL,
						'default' => false,
					],
					'emailConfirmToken' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 32,
					],
					'emailConfirmExpire' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
					'emailConfirmSended' => [
						'type' => Db::DB_TYPE_BOOL,
						'default' => false,
					],
					'nickname' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 25,
						'nullable' => false,
						'unique' => true,
					],
					'sex' => [
						'type' => Db::DB_TYPE_ENUM,
						'enumChoices' => ['M','F'],
						'nullable' => false,
					],
					'lifetime' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
						'nullable' => false,
						'default' => 0,
					],
					'passwordHash' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'ipCreated' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 15,
						'nullable' => false,
					],
					'hostCreated' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 255,
						'nullable' => false,
					],
					'userAgentCreated' => [
						'type' => Db::DB_TYPE_TEXT,
						'nullable' => false,
					],
					'userAcceptLanguage' => [
						'type' => DB::DB_TYPE_TEXT,
						'nullable' => false,
					],
					'created' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'roomId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
					],
					'lastVisit' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
					'lastMessageLoad' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
					'apiToken' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
					],
					'apiTokenExpire' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
				],
			],
			'App\Entity\UserProfile' => [
				'tableName' => 'user_profile',
				'primary' => ['userId'],
				'key' => [
					'countryCode' => ['countryCode'],
					'cityId' => ['cityId'],
				],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'locale' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 2,
					],
					'name' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 25,
					],
					'surname' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 25,
					],
					'countryCode' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 2,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\Country',
							'referencedFieldName' => 'code',
						],
					],
					'cityId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\City',
							'referencedFieldName' => 'id',
						],
					],
					'groupCount' => [
						'type' => Db::DB_TYPE_SMALLINT,
						'length' => 5,
						'unsigned' => true,
						'nullable' => false,
						'default' => 0,
					],
					'viewsCount' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'default' => 0,
					],
					'likeCount' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'default' => 0,
					],
				],
			],
			'App\Entity\UserPasswordReset' => [
				'tableName' => 'user_password_reset',
				'primary' => ['userId'],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'expire' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'code' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 32,
						'nullable' => false,
					],
				],
			],
			'App\Entity\UserGoogle' => [
				'tableName' => 'user_google',
				'primary' => ['userId'],
				'unique' => [
					'googleId' => ['googleId'],
				],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'googleId' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 50,
						'nullable' => false,
					],
					'payload' => [
						'type' => Db::DB_TYPE_TEXT,
						'nullable' => false,
					],
				],
			],
			'App\Entity\UserBlock' => [
				'tableName' => 'user_block',
				'primary' => ['userId','userIdBlocked'],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'userIdBlocked' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'createdAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'expiredAt' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
				],
			],
			'App\Entity\UserFriend' => [
				'tableName' => 'user_friend',
				'primary' => ['userId','userIdFriend'],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'userIdFriend' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'createdAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'approvedAt' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
				],
			],
			'App\Entity\Message' => [
				'tableName' => 'message',
				'primary' => ['hash'],
				'preSaveMethodNames' => ['preSave'],
				'field' => [
					'hash' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 32,
						'nullable' => false,
					],
					'roomId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'userIdTo' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
					],
					'created' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'message' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 255,
						'nullable' => false,
					],
				],
			],
			'App\Entity\MessagePrivate' => [
				'tableName' => 'message_private',
				'primary' => ['hash'],
				'preSaveMethodNames' => ['preSave'],
				'field' => [
					'hash' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 32,
						'nullable' => false,
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'userIdTo' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'created' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'fromDeleted' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'toDeleted' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'message' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 255,
						'nullable' => false,
					],
				],
			],
			'App\Entity\Room' => [
				'tableName' => 'room',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'key' => [
					'groupId' => ['groupId'],
				],
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
						'nullable' => false,
					],
					'name' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 15,
						'nullable' => false,
					],
					'default' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'groupId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\Group',
							'referencedFieldName' => 'id',
						],
					],
				],
			],
			'App\Entity\Country' => [
				'tableName' => 'country',
				'primary' => ['code'],
				'field' => [
					'code' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 2,
						'nullable' => false,
					],
					'nameEn' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'nameRu' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'priority' => [
						'type' => Db::DB_TYPE_SMALLINT,
						'unsigned' => true,
						'length' => 3,
						'nullable' => false,
						'default' => 100,
					],
				],
			],
			'App\Entity\City' => [
				'tableName' => 'city',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'countryCode' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 2,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\Country',
							'referencedFieldName' => 'code',
						],
					],
					'nameEn' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'nameRu' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'priority' => [
						'type' => Db::DB_TYPE_TINYINT,
						'unsigned' => true,
						'length' => 3,
						'nullable' => false,
						'default' => 100,
					],
				],
			],
			'App\Entity\Group' => [
				'tableName' => 'group',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'private' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'name' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 50,
						'nullable' => false,
					],
					'description' => [
						'type' => Db::DB_TYPE_TEXT,
						'nullable' => false,
					],
				],
			],
			'App\Entity\Photo' => [
				'tableName' => 'photo',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'unique' => [
					'hash' => ['hash'],
				],
				'key' => [
					'userId' => ['userId'],
					'approvedUserId' => ['approvedUserId'],
				],
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'enabled' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => true,
					],
					'name' => [
						'type' => Db::DB_TYPE_VARCHAR,
						'length' => 255,
					],
					'created' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'approved' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'approvedAt' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
					'approvedUserId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'hash' => [
						'type' => Db::DB_TYPE_CHAR,
						'length' => 32,
						'nullable' => false,
					],
				],
			],
			'App\Entity\PhotoData' => [
				'tableName' => 'photo_data',
				'primary' => ['photoId'],
				'field' => [
					'photoId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\Photo',
							'referencedFieldName' => 'id',
						],
					],
					'data' => [
						'type' => Db::DB_TYPE_TEXT,
						'nullable' => false,
					],
				],
			],
			'App\Entity\GroupUser' => [
				'tableName' => 'group_user',
				'primary' => ['groupId','userId'],
				'key' => [
					'approvedUserId' => ['approvedUserId'],
					'group_user_ibfk_user' => ['userId'],
				],
				'field' => [
					'groupId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\Group',
							'referencedFieldName' => 'id',
						],
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'createdAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'approved' => [
						'type' => Db::DB_TYPE_BOOL,
						'nullable' => false,
						'default' => false,
					],
					'approvedAt' => [
						'type' => Db::DB_TYPE_DATETIME,
					],
					'approvedUserId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_ONE_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
				],
			],
			'App\Entity\UserView' => [
				'tableName' => 'user_view',
				'primary' => ['userId','userIdView'],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'userIdView' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'watchedAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
				],
			],
			'App\Entity\UserProfileComment' => [
				'tableName' => 'user_profile_comment',
				'primary' => ['id'],
				'autoIncrement' => 'id',
				'key' => [
					'userId' => ['userId'],
					'userIdComment' => ['userIdComment'],
				],
				'field' => [
					'id' => [
						'type' => Db::DB_TYPE_INTEGER,
						'unsigned' => true,
						'length' => 11,
						'nullable' => false,
					],
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'userIdComment' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'createdAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
					'content' => [
						'type' => Db::DB_TYPE_TEXT,
						'nullable' => false,
					],
				],
			],
			'App\Entity\UserProfileLike' => [
				'tableName' => 'user_profile_like',
				'primary' => ['userId','userIdLike'],
				'field' => [
					'userId' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'userIdLike' => [
						'type' => Db::DB_TYPE_INTEGER,
						'length' => 11,
						'unsigned' => true,
						'nullable' => false,
						'relation' => [
							'type' => Db::DB_RELATION_TYPE_MANY_TO_ONE,
							'targetEntity' => 'App\Entity\User',
							'referencedFieldName' => 'id',
						],
					],
					'createdAt' => [
						'type' => Db::DB_TYPE_DATETIME,
						'nullable' => false,
					],
				],
			],
		],
	]
);