<?php
/**
 * @var string $object
 * @var mixed $id
 */
?>
<div
	data-component="likes"
	data-object="<?php echo $object; ?>"
	data-id="<?php echo $id; ?>"
	data-url="<?php echo $this->path('apiLikesCount'); ?>"
	class="disabled mt-3"
>
	<div class="likes mt-2">
		<?php echo $this->trans('Likes:'); ?> <span data-like-count></span>
		<button type="submit" data-like-action="create" data-url="<?php echo $this->path('apiLikesCreate'); ?>" class="btn btn-light" disabled><?php echo $this->trans('Like'); ?></button>
		<button type="submit" data-like-action="remove" data-url="<?php echo $this->path('apiLikesRemove'); ?>" class="btn btn-primary hide"><?php echo $this->trans('Liked'); ?></button>
	</div>
</div>