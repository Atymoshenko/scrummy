<?php
/**
 * @var string $object
 * @var mixed $id
 */
?>
<div
	data-user-authorized
	data-component="comments"
	data-object="<?php echo $object; ?>"
	data-id="<?php echo $id; ?>"
	data-url="<?php echo $this->path('apiComments'); ?>"
	class="mt-3"
>
	<div>
		<form action="<?php echo $this->path('apiCommentCreate'); ?>" method="post" name="commentCreate">
			<div class="form-group">
				<label for="comment-content">New comment</label>
				<textarea name="content" class="form-control" rows="3" id="comment-content" placeholder="Enter a comment"></textarea>
				<div class="invalid-feedback"></div>
			</div>
			<div>
				<input type="submit" value="<?php echo $this->trans('Create comment'); ?>" class="btn btn-primary" />
			</div>
		</form>
	</div>
	<div class="comments mt-2">
		<ul></ul>
	</div>
	<div class="progress loader mt-1 mb-1 invisible">
		<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
	</div>
</div>