<?php
$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/profileEdit.js']]);
?>

<h1>&nbsp;</h1>

<div class="row">
	<div class="col-md-3">
		<?php $this->include('Profile/menu.html.php'); ?>
	</div>
	<div class="col-md-9">
		<div class="alert alert-success hide"><?php echo $this->trans('Your email is confirmed.'); ?></div>
		<form method="post" data-action="<?php echo $this->path('apiSignUpConfirm'); ?>" name="profileEdit" novalidate>
			<input type="hidden" name="id" value=""/>
			<div class="form-group">
				<label for="user-email" class="required"><?php echo $this->trans('Email'); ?>:</label>
				<input type="email" name="email" id="user-email" maxlength="50" minlength="5" required disabled data-disabled class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group hide fieldBlock-emailConfirmToken">
				<label for="user-code" class="required"><?php echo $this->trans('Email confirm code'); ?>:</label>
				<input type="text" name="code" id="user-code" maxlength="32" minlength="32" class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
				<small class="form-text text-muted"></small>
			</div>
			<div class="form-group">
				<input type="submit" id="btn-submit" disabled="disabled" value="<?php echo $this->trans('Save'); ?>" class="btn btn-primary" />
			</div>
		</form>
	</div>
</div>

