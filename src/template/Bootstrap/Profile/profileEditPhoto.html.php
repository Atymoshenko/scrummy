<?php
/**
 * @var int $photosCountMax
 */
$this->extend([
	'file' => 'layout.html.php',
	'javascripts' => [
		'js/jQuery/vendor/jquery.ui.widget.js',
		'https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js',
		'https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js',
		'https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js',
		'https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js',
		'js/jQuery/jquery.iframe-transport.js',
		'js/jQuery/jquery.fileupload.js',
		'js/jQuery/jquery.fileupload-process.js',
		'js/jQuery/jquery.fileupload-image.js',
		'js/jQuery/jquery.fileupload-audio.js',
		'js/jQuery/jquery.fileupload-video.js',
		'js/jQuery/jquery.fileupload-validate.js',
		'js/jQuery/jquery.fileupload-validate.' . $this->getLocale() . '.js',
		'js/jQuery/jquery.fileupload-ui.js',
		'js/profilePhoto.js',
	],
	'stylesheets' => [
		'https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css',
		'css/jQuery/jquery.fileupload.css',
		'css/jQuery/jquery.fileupload-ui.css',
	]
]);
?>

<h1>&nbsp;</h1>

<div class="row">
	<div class="col-md-3">
		<?php $this->include('Profile/menu.html.php'); ?>
	</div>
	<div class="col-md-9">
		<?php $this->include('Profile/upload.html.php', ['photosCountMax' => $photosCountMax]); ?>
	</div>
</div>
