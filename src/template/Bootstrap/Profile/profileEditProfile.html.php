<?php
/**
 * @var Country[] $countries
 * @var array $locales
 */

use App\Entity\Country;

$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/profileEdit.js']]);
?>

<h1>&nbsp;</h1>

<div class="row">
	<div class="col-md-3">
		<?php $this->include('Profile/menu.html.php'); ?>
	</div>
	<div class="col-md-9">
		<form method="post" data-action="<?php echo $this->path('apiProfileEditProfile'); ?>" name="profileEdit" novalidate>
			<div class="form-group">
				<label for="profile-locale" class="required"><?php echo $this->trans('Language'); ?>:</label>
				<select name="locale" id="profile-locale" class="custom-select custom-select-sm">
					<option value=""></option>
					<?php foreach ($locales as $key => $locale): ?>
						<?php echo '<option value="' . htmlspecialchars($key) . '">' . htmlspecialchars($locale['name']) . '</option>'; ?>
					<?php endforeach; ?>
				</select>
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group">
				<label for="profile-name"><?php echo $this->trans('Name'); ?>:</label>
				<input type="text" name="name" id="profile-name" maxlength="20" minlength="2" class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group">
				<label for="profile-surname"><?php echo $this->trans('Surname'); ?>:</label>
				<input type="text" name="surname" id="profile-surname" maxlength="20" minlength="2" class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group">
				<label for="profile-country"><?php echo $this->trans('Country'); ?>:</label>
				<select name="countryCode" id="profile-country" class="custom-select custom-select-sm" data-field="country">
					<option value=""></option>
					<?php foreach ($countries as $country): ?>
					<?php echo '<option value="' . htmlspecialchars($country->getCode()) . '">' . htmlspecialchars($country->getName()) . '</option>'; ?>
					<?php endforeach; ?>
				</select>
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group">
				<label for="profile-city"><?php echo $this->trans('City'); ?>:</label>
				<select name="city" id="profile-city" class="custom-select custom-select-sm" data-field="city" data-loading="<?php echo $this->trans('Loading...'); ?>">
					<option value=""></option>
				</select>
				<div class="invalid-feedback"></div>
			</div>
			<div class="form-group">
				<input type="submit" value="<?php echo $this->trans('Save'); ?>" class="btn btn-primary" />
			</div>
		</form>
	</div>
</div>

