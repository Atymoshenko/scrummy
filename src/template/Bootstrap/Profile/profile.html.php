<?php
/**
 * @var App\Entity\User $user
 * @var App\Entity\UserProfile $userProfile
 * @var App\Entity\UserGoogle|null $userGoogle
 * @var App\Entity\Country|null $country
 * @var string|null $photoUrlThumb
*/

$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/profile.js','js/comments.js','js/likes.js','js/profilePhotos.js']]);
?>

<h1><?php echo htmlspecialchars($user->getNickname()); ?></h1>
<div class="row" id="userProfileContainer" data-user-id="<?php echo $user->getId() ?>">
	<div class="col-sm-4">
		<div class="mb-3 text-center">
			<?php if ($photoUrlThumb): ?>
				<img src="<?php echo $photoUrlThumb;?>" alt="<?php echo htmlspecialchars($user->getNickname()); ?>" class="photo-profile" />
			<?php elseif ($userGoogle): ?>
				<?php $googlePayload = json_decode($userGoogle->getPayload(), true); ?>
				<?php if ($googlePayload['picture']): ?>
					<img src="<?php echo $googlePayload['picture'];?>" alt="<?php echo htmlspecialchars($user->getNickname()); ?>" class="photo-profile" />
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<div class="mb-3">
			<button type="button" class="btn btn-primary btn-sm btn-block invisible" data-friend
					data-status
					data-url-add="<?php echo $this->path('apiUserFriendAdd', ['userId' => $user->getId()]); ?>"
					data-url-remove="<?php echo $this->path('apiUserFriendRemove', ['userId' => $user->getId()]); ?>"
					data-text-friend-add="<?php echo $this->trans('Add as friend'); ?>"
					data-text-friend-approve="<?php echo $this->trans('Confirm friendship'); ?>"
					data-text-processing="<?php echo $this->trans('Processing...'); ?>"
					data-text-friend-sent="<?php echo $this->trans('Cancel friend request'); ?>"
					data-text-friend-remove="<?php echo $this->trans('Remove from friends'); ?>"
			></button>
			<button type="button" class="btn btn-warning btn-sm btn-block hide" data-friend-decline
					data-url="<?php echo $this->path('apiUserFriendRemove', ['userId' => $user->getId()]); ?>"
					data-text-processing="<?php echo $this->trans('Processing...'); ?>"
					data-text-decline="<?php echo $this->trans('Refuse to be friends'); ?>"
			></button>
		</div>
		<div class="mb-3">
			<button type="button" class="btn btn-danger btn-sm btn-block invisible" data-block
					data-status
					data-url-add="<?php echo $this->path('apiUserBlockAdd', ['userId' => $user->getId()]); ?>"
					data-url-remove="<?php echo $this->path('apiUserBlockRemove', ['userId' => $user->getId()]); ?>"
					data-text-block-add="<?php echo $this->trans('Block this user'); ?>"
					data-text-processing="<?php echo $this->trans('Processing...'); ?>"
					data-text-block-remove="<?php echo $this->trans('Remove from block'); ?>"
			></button>
		</div>
	</div>
	<div class="col-sm-8">
		<div>
			<?php echo $userProfile->getName() . ' ' . $userProfile->getSurname(); ?>
		</div>
		<?php if ($country): ?>
			<div>
				<?php echo $this->trans('Country'); ?>: <?php echo htmlspecialchars($country->getName()); ?>
			</div>
		<?php endif; ?>
		<div><?php echo $this->trans('Gender'); ?>: <?php echo $user->getSex() === 'M' ? $this->trans('male') : ($user->getSex() === 'F' ? $this->trans('female') : $this->trans('unknown')); ?></div>
		<div><?php echo $this->trans('Sign up date'); ?>: <?php echo $user->getCreated()->format('d.m.Y'); ?></div>
		<div class="disabled"><?php echo $this->trans('Views:'); ?> <span data-views></span></div>
		<div class="disabled"><?php echo $this->trans('Last visit:'); ?> <span data-last-visit></span></div>
		<?php $this->include('Common/likes.html.php', [
			'object' => 'userProfile',
			'id' => $user->getId(),
		]); ?>

		<div
			class="disabled profilePhotos"
			data-component="photos"
			data-url="<?php echo $this->path('apiUserPhotos', ['id' => $userProfile->getUserId()]); ?>"
		>
			<h2><?php echo $this->trans('Photos'); ?></h2>
			<div class="slider">
				<ul></ul>
				<div class="hide"><?php echo $this->trans('User has no photos.'); ?></div>
			</div>
		</div>

		<?php $this->include('Common/comments.html.php', [
			'object' => 'userProfile',
			'id' => $user->getId(),
		]); ?>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="userProfilePhotosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	<div class="modal-dialog modal-carousel modal-dialog-scrollable" role="document"
		 style="max-width:99%;height:99%"
	>
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalScrollableTitle">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div style="display:flex;flex-flow:row wrap;align-content:center;align-items:center;justify-content:center">
					<div style="flex: 0 0 auto">
						<div style="display:flex;flex-flow:row wrap;align-content:center;align-items:center;justify-content:center">
							<div style="order:1;flex: 0 1 auto;min-width:285px" id="userProfilePhotosCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
								<div class="carousel-inner" style="width:auto;">
								</div>
								<a class="carousel-control-prev" href="#userProfilePhotosCarousel" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#userProfilePhotosCarousel" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
							<!--<div style="order:2;flex: 1 1 auto;min-width:320px">Likes and comments soon.</div>-->
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
