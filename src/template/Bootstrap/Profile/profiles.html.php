<?php
/**
 * @var Country[] $countries
 */

use App\Entity\Country;

$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/profiles.js']]);
?>

<h1><?php echo $this->trans('People'); ?></h1>

<div class="mt-4">
	<div class="row">
		<div class="col-md-3">
			<form method="post" action="<?php echo $this->path('apiProfiles'); ?>" name="profiles">
				<div class="form-group">
					<label for="profiles-countryCode"><?php echo $this->trans('Country'); ?></label>
					<select name="countryCode" class="custom-select" id="profiles-countryCode" data-field="country">
						<option value=""></option>
						<?php foreach ($countries as $country): ?>
							<?php echo '<option value="' . htmlspecialchars($country->getCode()) . '">' . htmlspecialchars($country->getName()) . '</option>'; ?>
						<?php endforeach; ?>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<label for="profiles-cityId"><?php echo $this->trans('City'); ?></label>
					<select name="cityId" class="custom-select" id="profiles-cityId" data-field="city" data-loading="<?php echo $this->trans('Loading...'); ?>">
						<option selected></option>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="<?php echo $this->trans('Search profiles'); ?>">
				</div>
			</form>
		</div>
		<div class="col-md-9">
			<div id="profiles">

			</div>
			<div class="progress loader mt-1 mb-1 invisible">
				<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
			</div>
		</div>
	</div>
</div>