<ul class="nav flex-column">
	<li class="nav-item">
		<a id="btn-view-profile" class="nav-link btn btn-primary invisible" href="<?php echo $this->path('profile', ['userId' => 'userId']);?>"><?php echo $this->trans('View profile'); ?></a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $this->path('profile_edit_confirm');?>"><?php echo $this->trans('Confirm'); ?></a>
	</li>
	<li class="nav-item">
		<a class="nav-link _active" href="<?php echo $this->path('profile_edit_profile');?>"><?php echo $this->trans('Profile'); ?></a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $this->path('profile_edit_password');?>"><?php echo $this->trans('Password'); ?></a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $this->path('profile_edit_photo');?>"><?php echo $this->trans('Photos'); ?></a>
	</li>
</ul>
