<?php
$this->extend(['file' => 'layout.html.php']);
?>

<h1>&nbsp;</h1>

<div class="row">
	<div class="col-md-3">
		<?php $this->include('Profile/menu.html.php'); ?>
	</div>
	<div class="col-md-9">
		<form method="post" data-action="<?php echo $this->path('apiProfileEditPassword'); ?>" name="profileEdit" novalidate>
			<div class="form-group">
				<label for="user-password"><?php echo $this->trans('New password'); ?>:</label>
				<input type="password" name="password" id="user-password" maxlength="30" minlength="6" class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
				<small class="form-text text-muted"><?php echo $this->trans("Leave blank if you don't want to change."); ?></small>
			</div>
			<div class="form-group">
				<input type="submit" value="<?php echo $this->trans('Save'); ?>" class="btn btn-primary" />
			</div>
		</form>
	</div>
</div>

