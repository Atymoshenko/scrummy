<?php
/**
 * @var string $lang
 * @var string $env
 * @var string $baseHref
 * @var string $googleClientId
 * @var string $content
 * @var string $route
 * @var integer $staticVersion
 */
$route = $route ?? null;
?><!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
	<?php if ($env === 'prod'): ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136769941-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-136769941-1');
	</script>

	<!-- BEGIN Google Adsense: -->
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({
			google_ad_client: "ca-pub-3376494969292060",
			enable_page_level_ads: true
		});
	</script>
	<!-- END Google Adsense. -->
	<?php endif; ?>
	<script>
		var env = "<?php echo $env; ?>";
	</script>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $this->trans('Scrummy'); ?></title>
	<meta name="description" content="<?php echo $this->trans('Chat with people around the world. Create groups. Find people by interests.'); ?>">
	<meta name="keywords" content="International dating site chat международный чат сайт знакомств">
	<meta name="google-signin-scope" content="profile email openid">
	<meta name="google-signin-client_id" content="<?php echo $googleClientId; ?>">
	<base href="<?php echo $baseHref; ?>">
	<link rel="icon" type="image/x-icon" sizes="32x32" href="favicon.ico?v<?php echo $staticVersion; ?>" />
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css?v<?php echo $staticVersion; ?>">
	<link rel="stylesheet" href="css/common.css?v<?php echo $staticVersion; ?>">
	<?php $this->stylesheets(); ?>
</head>
<body>

<div class="domloader">
	<div class="domloader-modal">
		<div class="text-center display-4"><?php echo $this->trans('Loading...'); ?></div>
		<div class="progress mt-2">
			<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%;"></div>
		</div>
	</div>
</div>

<div class="wrapper<?php if (in_array($route, ['chat', 'group_chat'])) echo ' chat'; ?>">
	<div class="header">
		<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark bg-scrummy">
			<a class="navbar-brand" href="<?php echo $this->path(''); ?>">
				<img src="images/logo.png" class="logo" alt="<?php echo $this->trans('Scrummy'); ?>" />
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">&nbsp;</span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active_">
						<a class="nav-link" href="<?php echo $this->path('chat'); ?>"><?php echo $this->trans('Chat'); ?></a>
					</li>
					<li class="nav-item active_">
						<a class="nav-link" href="<?php echo $this->path('groups'); ?>"><?php echo $this->trans('Groups'); ?></a>
					</li>
					<li class="nav-item active_">
						<a class="nav-link" href="<?php echo $this->path('profiles'); ?>"><?php echo $this->trans('People'); ?></a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
					</li>-->
				</ul>
				<!--<form class="form-inline my-2 my-md-0">
					<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>-->
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active_" data-user-anonyme>
						<a class="nav-link" href="<?php echo $this->path('signup'); ?>"><?php echo $this->trans('Sign up'); ?></a>
					</li>
					<li class="nav-item active_" data-user-anonyme>
						<a class="nav-link" href="<?php echo $this->path('signin'); ?>"><?php echo $this->trans('Sign in'); ?></a>
					</li>
					<li class="nav-item dropdown" data-user-authorized>
						<a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $this->trans('Profile'); ?>
						</a>
						<div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<?php echo $this->path('profile_edit_profile'); ?>"><?php echo $this->trans('Edit profile'); ?></a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" data-action="signout" data-api-method="<?php echo $this->path('apiSignOut'); ?>" href="javascript:void(0);"><?php echo $this->trans('Sign out'); ?></a>
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>

	<div class="middle">
		<div class="wrapper-container">
			<div class="content">
				<?php if (!in_array($route, ['', 'chat', 'group_chat'])) echo '<div class="container">'; ?>
					<?php echo $content; ?>
				<?php if (!in_array($route, ['', 'chat', 'group_chat'])) echo '</div>'; ?>
			</div>
		</div>

		<div class="side-left"></div>

		<div class="side-right"></div>
	</div>
</div>
<footer class="footer<?php if (in_array($route, ['chat', 'group_chat'])) echo ' chat'; ?>">
	<div class="container">
		<a href="<?php echo $this->path('policy'); ?>" target="_self" class="mr-4"><?php echo $this->trans('Privacy policy'); ?></a>
		<a href="<?php echo $this->path('rules'); ?>" target="_self"><?php echo $this->trans('Rules'); ?></a>
	</div>
</footer>

<script src="https://apis.google.com/js/platform.js"></script>
<script src="js/jQuery/jquery-3.3.1.min.js?v<?php echo $staticVersion; ?>"></script>
<script src="js/jQuery/jquery.cookie.js?v<?php echo $staticVersion; ?>"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js?v<?php echo $staticVersion; ?>"></script>
<script src="js/messages.<?php echo $this->getLocale(); ?>.js?v<?php echo $staticVersion; ?>"></script>
<script>
var $body;
var routes = {};
<?php foreach ($this->getRoutesForJsAsArray() as $routeName => $routeData): ?>
routes['<?php echo $routeName; ?>'] = {};
	<?php foreach ($routeData as $key => $val) :?>
routes['<?php echo $routeName; ?>']['<?php echo $key; ?>'] = <?php echo is_bool($val) ? ($val ? 'true' : 'false') : ("'" . $val . "'"); ?>;
	<?php endforeach; ?>
<?php endforeach; ?>
var account;
$(function() {
	$body = $('body');
	account = {
		_user: {},
		_profile: {},
		_checkUserTask: [],
		_checkUserParams: [],
		_data: [],
		_listeners: [],
		get user() {
			return this._user;
		},
		set user(user) {
			this._user = user;
		},
		get profile() {
			return this._profile;
		},
		set profile(profile) {
			this._profile = profile;
			// $.each(this._listeners, function() {
			// 	this();
			// });
		},
		get data() {
			return this._data;
		},
		set data(data) {
			this._data = data;
			$.each(this._listeners, function() {
				this();
			});
		},
		get checkUserTask() {
			return this._checkUserTask;
		},
		set checkUserTask(checkUserTask) {
			this._checkUserTask.push(checkUserTask);
		},
		get checkUserParams() {
			return this._checkUserParams;
		},
		set checkUserParams(checkUserParams) {
			this._checkUserParams.push(checkUserParams);
		},
		set listener(listener) {
			this._listeners.push(listener);
		}
	};
})
</script>

<?php $this->javascripts(); ?>

<!--[if (gte IE 8)&(lt IE 10)]>
<script src="_js/jQuery/cors/jquery.xdr-transport.js"></script>
<![endif]-->

<?php if ($env === 'dev') echo '<script src="js/dev.js?v' . $staticVersion . '"></script>'; ?>
<script src="js/common.js?v<?php echo $staticVersion; ?>"></script>

</body>
</html>