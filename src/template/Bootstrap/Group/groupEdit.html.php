<?php
/**
 * @var int $groupId
 */
$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/groupEdit.js']]);
?>

<h1><?php echo $this->trans('Edit group'); ?></h1>

<div class="progress loader">
	<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
</div>
<div data-component="group-edit-error" class="alert alert-danger hide">
	<?php echo $this->trans('You are not authorized to edit this group.'); ?>
</div>
<div data-component="group-edit-form" data-group-id="<?php echo $groupId; ?>">
	<div>
		<a href="<?php echo $this->path('group', ['groupId' => $groupId]); ?>"><?php echo $this->trans('Go to this group'); ?></a>
	</div>
</div>
