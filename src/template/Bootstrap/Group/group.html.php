<?php
/**
 * @var App\Entity\Group $group
*/

$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/group.js']]);
?>

<h1><?php echo htmlspecialchars($group->getName()); ?></h1>

<div class="row" data-group-id="<?php echo $group->getId(); ?>">
	<div class="col-md-3">
		<?php $this->include('Group/menu.html.php', ['groupId' => $group->getId()]); ?>
		<?php $this->include('Group/requests.html.php'); ?>
	</div>
	<div class="col-md-9">
		<p><?php echo htmlspecialchars($group->getDescription()); ?></p>
		<div class="progress loader">
			<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"></div>
		</div>
		<div data-user-authorized>

		</div>
	</div>
</div>
