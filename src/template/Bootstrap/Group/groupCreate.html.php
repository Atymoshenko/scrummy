<?php
$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/groupCreate.js']]);
?>

<h1><?php echo $this->trans('Creating a new group'); ?></h1>

<div class="alert alert-warning hide" data-component="group-create-require">
	<p>In order to create a group, you must specify the country and city in your profile.</p>
	<p>Attention! After you specify the country, city and create a group, then you can not change this data. We recommend entering the true country and city.</p>
</div>

<div data-component="group-create-block" class="hide">
	<div class="alert alert-warning" data-component="group-create-accept">
		<div>Attention!</div>
		<p>If you create at least one group, then you cannot change the country and city in your profile.</p>
		<p>If you want to later change the country or city in your profile, then you need to delete the groups you created or transfer the rights of the group owner to another user.</p>
		<button class="btn btn-primary" id="button-group-accept"><?php echo $this->trans('I accept'); ?></button>
	</div>
	<div class="alert alert-success hide" data-component="group-create-success">
		Group successfully created.
	</div>
	<div class="progress loader hide">
		<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
	</div>
	<div data-component="group-create-form">
	</div>
</div>