<form method="post" action="<?php echo $this->path('apiGroupCreate'); ?>" novalidate>
	<div class="form-group">
		<label for="group_name" class="required"><?php echo $this->trans('Group name'); ?></label>
		<input type="text" name="name" id="group_name" maxlength="20" minlength="5" required value="" class="form-control form-control-sm" />
		<div class="invalid-feedback"></div>
	</div>
	<div class="form-group">
		<label for="group_description" class="required"><?php echo $this->trans('Description'); ?></label>
		<textarea name="description" class="form-control" id="group_description" rows="3"></textarea>
		<div class="invalid-feedback"></div>
	</div>
	<div class="form-group">
		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input" id="private" name="private" value="1" required>
			<label class="custom-control-label" for="private"><?php echo $this->trans('Private group'); ?></label>
			<div class="invalid-feedback"></div>
		</div>
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary" value="<?php echo $this->trans('Create'); ?>" />
	</div>
</form>
