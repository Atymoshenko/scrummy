<?php
/**
 * @var Country[] $countries
 */

use App\Entity\Country;

$this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/groups.js']]);
?>

<h1><?php echo $this->trans('Groups'); ?></h1>

<ul class="nav" data-user-authorized>
	<li class="nav-item">
		<a class="nav-link active" href="<?php echo $this->path('group_create'); ?>"><?php echo $this->trans('Create new group'); ?></a>
	</li>
</ul>

<div class="mt-4" data-user-authorized>
	<div class="row">
		<div class="col-md-3">
			<form method="post" action="<?php echo $this->path('apiGroups'); ?>" name="groups">
				<div class="form-group">
					<label for="groups-country" class="required"><?php echo $this->trans('Country'); ?></label>
					<select name="country" class="custom-select" id="groups-country" data-field="country">
						<option value=""></option>
						<?php foreach ($countries as $country): ?>
							<?php echo '<option value="' . htmlspecialchars($country->getCode()) . '">' . htmlspecialchars($country->getName()) . '</option>'; ?>
						<?php endforeach; ?>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<label for="groups-city" class="required"><?php echo $this->trans('City'); ?></label>
					<select name="city" class="custom-select" id="groups-city" data-field="city" data-loading="<?php echo $this->trans('Loading...'); ?>">
						<option selected></option>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<label for="groups-category"><?php echo $this->trans('Category'); ?></label>
					<select name="category" class="custom-select" id="groups-category">
						<option selected></option>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="<?php echo $this->trans('Search groups'); ?>">
				</div>
			</form>
			<div data-component="my-groups" class="hide mb-3">
				<div><h2><?php echo $this->trans('My groups:'); ?></h2></div>
			</div>
		</div>
		<div class="col-md-9 groups list-groups" id="groups">
		</div>
	</div>
</div>