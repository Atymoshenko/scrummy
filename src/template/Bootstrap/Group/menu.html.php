<?php
/**
 * @var int $groupId
 */
?>
<ul class="nav flex-column">
	<li class="nav-item hide" data-item-group-join>
		<a class="nav-link" href="javascript:void(0);" data-action="<?php echo $this->path('apiGroupJoin'); ?>" data-text-sent="<?php echo $this->trans('Request sent'); ?>" data-text-join="<?php echo $this->trans('Join the group'); ?>"><?php echo $this->trans('Join the group'); ?></a>
		<a class="hide" href="javascript:void(0);"><?php echo $this->trans('Cancel'); ?></a>
	</li>
	<li class="nav-item hide" data-item-group-leave>
		<a class="nav-link _active" href="javascript:void(0);" data-action="<?php echo $this->path('apiGroupLeave'); ?>"><?php echo $this->trans('Leave group'); ?></a>
	</li>
	<li class="nav-item" data-item-group-chat>
		<a class="nav-link disabled" href="<?php echo $this->path('group_chat', ['groupId' => $groupId]); ?>"><?php echo $this->trans('Group chat'); ?></a>
	</li>
</ul>
