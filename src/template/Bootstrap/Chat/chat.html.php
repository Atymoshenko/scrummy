<?php
/**
 * @var bool $isDev
 * @var string $chatMode
 */
?>
<?php $this->extend(['file' => 'layout.html.php', 'javascripts' => ['js/chat.js']]); ?>
<script>
	var chatMode = '<?php echo $chatMode; ?>';
	var groupId = <?php echo (isset($group) ? $group->getId() : 'null'); ?>;
</script>
<div class="progress loader">
	<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
</div>

<div data-user-authorized class="wrapper-chat hide">
	<div class="content-chat">
		<div class="header-chat">
			<ul class="nav nav-tabs nav-fill" id="chat-tabs">
				<li class="nav-item">
					<a class="nav-link active" data-name="chat-general" href=""><?php echo $this->trans('Chat'); ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-name="chat-private" href=""><?php echo $this->trans('Private'); ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-name="right" href=""><?php echo $this->trans('Rooms'); ?></a>
				</li>
			</ul>
		</div>
		<div class="section section-chat-general active-chat-tabs-container" data-chat-tabs-container="chat-general">
			<div class="chat chat-general"></div>
			<div class="scrollDown hide" data-toggle="tooltip" data-placement="left" title="Scroll bottom">\/</div>
		</div>
		<div class="section section-chat-private" data-chat-tabs-container="chat-private">
			<div class="chat chat-private"></div>
			<div class="scrollDown hide" data-toggle="tooltip" data-placement="left" title="Scroll bottom">\/</div>
		</div>
	</div>
	<div class="side-left-chat">
		<div class="smiles-list">
			<ul>
				<li><img src="images/smiles/1.gif" alt="" /></li>
				<li><img src="images/smiles/2.gif" alt="" /></li>
				<li><img src="images/smiles/3.gif" alt="" /></li>
				<li><img src="images/smiles/4.gif" alt="" /></li>
				<li><img src="images/smiles/5.gif" alt="" /></li>
				<li><img src="images/smiles/6.gif" alt="" /></li>
				<li><img src="images/smiles/7.gif" alt="" /></li>
				<li><img src="images/smiles/8.gif" alt="" /></li>
				<li><img src="images/smiles/9.gif" alt="" /></li>
				<li><img src="images/smiles/A.gif" alt="" /></li>
				<li><img src="images/smiles/B.gif" alt="" /></li>
				<li><img src="images/smiles/C.gif" alt="" /></li>
				<li><img src="images/smiles/D.gif" alt="" /></li>
				<li><img src="images/smiles/E.gif" alt="" /></li>
				<li><img src="images/smiles/F.gif" alt="" /></li>
				<li><img src="images/smiles/G.gif" alt="" /></li>
				<li><img src="images/smiles/H.gif" alt="" /></li>
				<li><img src="images/smiles/I.gif" alt="" /></li>
				<li><img src="images/smiles/J.gif" alt="" /></li>
				<li><img src="images/smiles/K.gif" alt="" /></li>
				<li><img src="images/smiles/L.gif" alt="" /></li>
				<li><img src="images/smiles/M.gif" alt="" /></li>
				<li><img src="images/smiles/N.gif" alt="" /></li>
				<li><img src="images/smiles/O.gif" alt="" /></li>
				<li><img src="images/smiles/P.gif" alt="" /></li>
				<li><img src="images/smiles/Q.gif" alt="" /></li>
				<li><img src="images/smiles/R.gif" alt="" /></li>
				<li><img src="images/smiles/S.gif" alt="" /></li>
				<li><img src="images/smiles/T.gif" alt="" /></li>
				<li><img src="images/smiles/U.gif" alt="" /></li>
				<li><img src="images/smiles/V.gif" alt="" /></li>
				<li><img src="images/smiles/W.gif" alt="" /></li>
				<li><img src="images/smiles/X.gif" alt="" /></li>
				<li><img src="images/smiles/Y.gif" alt="" /></li>
			</ul>
		</div>
	</div>
	<div class="side-right-chat" data-chat-tabs-container="right">
		<div class="section section-rooms">
			<div class="container">
				<h2><?php echo $this->trans('Rooms'); ?></h2>
				<div class="rooms-list"></div>
			</div>
		</div>
		<div class="section section-users">
			<div class="container">
				<div class="section-users-container">
					<h2><?php echo $this->trans('Users'); ?></h2>
					<div class="container-users">
						<div data-sex="M">
							<h3><?php echo $this->trans('Men'); ?></h3>
							<div class="users-list users-list-male">
								<div data-online="1"></div>
								<div data-online="0"></div>
							</div>
						</div>
						<div data-sex="F">
							<h3><?php echo $this->trans('Women'); ?></h3>
							<div class="users-list users-list-female">
								<div data-online="1"></div>
								<div data-online="0"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div data-user-authorized class="footer-chat hide">
	<form method="post" action="<?php echo $this->path('apiChatMessage'); ?>" name="message" novalidate>
	<div class="container mt-1">
		<div class="form-row">
			<div class="col-auto">
				<button type="submit" class="btn btn-primary btn-sm"><?php echo $this->trans('Send'); ?></button>
			</div>
			<div class="col-5">
				<label for="message-message" class="hide"></label>
				<input type="text" name="message" id="message-message" placeholder="Message" autofocus maxlength="255" minlength="1" value="" class="field-message form-control form-control-sm" />
				<div class="invalid-tooltip"></div>
			</div>
			<div class="col-auto">
				<button type="button" name="sendPrivate" class="btn btn-secondary btn-sm"><?php echo $this->trans('Private'); ?></button>
			</div>
		</div>
		<div class="form-row mt-1">
			<div class="col-auto">
				<div class="btn-toolbar">
					<div class="btn-group mr-2">

					</div>
				</div>
			</div>
		</div>
		<?php if ($isDev): ?>
			<div class="form-row mt-1 dev">
				<div class="col-auto">
					<div class="btn-toolbar">
						<div class="btn-group mr-1">
							<button type="button" name="loadMessages" class="btn btn-light btn-sm">Load Messages</button>
							<button type="button" name="loadUsers" class="btn btn-light btn-sm">Load Users</button>
							<button type="button" name="loadRooms" class="btn btn-light btn-sm">Load Rooms</button>
							<button type="button" name="generateMessages" class="btn btn-light btn-sm">Generate messages</button>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	</form>
</div>

<div class="copyright">&copy; <a href="http://vws.com.ua" target="_blank">VWS</a></div>
