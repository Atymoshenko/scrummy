<?php $this->extend(['file' => 'layout.html.php']); ?>

<h1><?php echo $this->trans('Password recovery'); ?></h1>

<div data-component="form-password-forgot">
	<form method="post" action="<?php echo $this->path('apiProfilePasswordForgot'); ?>" name="password-forgot" novalidate>
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="user-login" class="required"><?php echo $this->trans('Email or nickname'); ?>:</label>
				<input type="text" name="login" id="user-login" maxlength="50" minlength="5" autofocus required class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
			</div>
		</div>
		<div>
			<input type="submit" value="<?php echo $this->trans('Send'); ?>" class="btn btn-primary" />
		</div>
	</form>
</div>

<div data-component="form-password-forgot-sent" class="hide">
	<div class="alert alert-success"><?php echo $this->trans('Instructions for password recovery sent to the mail that was specified during registration of this account.'); ?></div>
</div>
