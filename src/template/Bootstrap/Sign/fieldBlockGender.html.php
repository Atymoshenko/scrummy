<?php $count = $count ?? 0; ?>
<fieldset class="group-error">
	<div><?php echo $this->trans('Your gender'); ?>:</div>
	<div class="custom-control custom-radio">
		<input type="radio" id="user-sex-M_<?php echo $count; ?>" name="sex" value="M" class="custom-control-input">
		<label class="custom-control-label" for="user-sex-M_<?php echo $count; ?>"><?php echo $this->trans('Male'); ?></label>
	</div>
	<div class="custom-control custom-radio">
		<input type="radio" id="user-sex-F_<?php echo $count; ?>" name="sex" value="F" class="custom-control-input">
		<div class="invalid-feedback"></div>
		<label class="custom-control-label" for="user-sex-F_<?php echo $count; ?>"><?php echo $this->trans('Female'); ?></label>
	</div>
</fieldset>
