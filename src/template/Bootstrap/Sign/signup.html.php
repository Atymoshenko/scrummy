<?php $this->extend(['file' => 'layout.html.php']); ?>

<h1><?php echo $this->trans('Account creation'); ?></h1>

<div data-user-anonyme data-component="form-sign-up">
	<div class="mt-1">
		<form method="post" action="<?php echo $this->path('apiSignInGoogle'); ?>" name="signinGoogle" novalidate>
			<input type="hidden" name="idToken" value=""/>
			<?php $this->include('Sign/googleSigninButton.html.php'); ?>
			<div data-field-block="nickname" class="form-group hide">
				<?php $this->include('Sign/fieldBlockNickname.html.php', ['count' => 0]); ?>
				<?php $this->include('Sign/fieldBlockGender.html.php', ['count' => 0]); ?>
				<?php $this->include('Sign/fieldBlockAgree.html.php', ['count' => 0]); ?>
				<div class="form-row align-items-center">
					<div class="col-auto">
						<input type="submit" value="<?php echo $this->trans('Sign in'); ?>" class="btn btn-primary"/>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div data-form="signup" class="form-auth mt-3">
		<form method="post" action="<?php echo $this->path('apiSignUp'); ?>" name="sign" novalidate>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="user-email" class="required"><?php echo $this->trans('Email'); ?>:</label>
					<input type="email" name="email" id="user-email" maxlength="50" minlength="5" autofocus required class="form-control form-control-sm" />
					<div class="invalid-feedback"></div>
				</div>
			</div>
			<div class="form-row hide fieldBlock-emailConfirmToken">
				<label for="user-emailConfirmToken" class="required"><?php echo $this->trans('Email confirm code'); ?>:</label>
				<input type="text" name="emailConfirmToken" id="user-emailConfirmToken" maxlength="32" minlength="32" class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
				<small class="form-text text-muted"></small>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="user-password" class="required"><?php echo $this->trans('Password'); ?>:</label>
					<input type="password" name="password" id="user-password" maxlength="30" minlength="6" required class="form-control form-control-sm" />
					<div class="invalid-feedback"></div>
				</div>
			</div>
			<div class="form-group">
				<?php $this->include('Sign/fieldBlockNickname.html.php', ['count' => 1]); ?>
				<?php $this->include('Sign/fieldBlockGender.html.php', ['count' => 1]); ?>
				<?php $this->include('Sign/fieldBlockAgree.html.php', ['count' => 1]); ?>
			</div>
			<div>
				<input type="submit" value="<?php echo $this->trans('Create account'); ?>" class="btn btn-primary" />
			</div>
		</form>
	</div>
</div>

<div data-user-authorized data-component="form-sign-up-success" class="hide">
	<div class="alert alert-success"><?php echo $this->trans('Your account has been created.'); ?></div>
	<p data-dynamic="info"></p>
	<p><a href="<?php echo $this->path('profile_edit_confirm'); ?>" target="_self"><?php echo $this->trans('Go to profile editing'); ?></a>.</p>
</div>

<div data-component="form-sign-up-email-confirm" class="hide">
	<div class="alert alert-success hide"><?php echo $this->trans('Your email is confirmed.'); ?></div>
	<div class="alert alert-danger hide"><?php echo $this->trans('Your email is not confirmed.'); ?></div>
	<div class="progress loader">
		<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
	</div>
	<p data-dynamic="info"></p>
	<p class="hide"><a href="<?php echo $this->path('profile_edit_profile'); ?>" target="_self"><?php echo $this->trans('Go to profile editing.'); ?></a></p>
</div>
