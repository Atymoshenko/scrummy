<?php $this->extend(['file' => 'layout.html.php']); ?>

<h1><?php echo $this->trans('Sign in'); ?></h1>

<div data-user-anonyme>
	<div class="mt-1">
		<form method="post" action="<?php echo $this->path('apiSignInGoogle'); ?>" name="signinGoogle" novalidate>
			<input type="hidden" name="idToken" value=""/>
			<?php $this->include('Sign/googleSigninButton.html.php'); ?>
			<div data-field-block="nickname" class="form-group hide">
				<?php $this->include('Sign/fieldBlockNickname.html.php'); ?>
				<?php $this->include('Sign/fieldBlockGender.html.php'); ?>
				<?php $this->include('Sign/fieldBlockAgree.html.php'); ?>
				<div class="form-row align-items-center">
					<div class="col-auto">
						<input type="submit" value="<?php echo $this->trans('Sign in'); ?>" class="btn btn-primary"/>
					</div>
				</div>
			</div>
			<a href="javascript:void(0);" onclick="googleSignOut();" class="hide"><?php echo $this->trans('Sign out'); ?></a>
		</form>
	</div>

	<div class="mt-3" data-form="signin">
		<form method="post" action="<?php echo $this->path('apiSignIn'); ?>" name="sign" novalidate>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="user-email" class="required"><?php echo $this->trans('Email'); ?>:</label>
					<input type="text" name="login" id="user-email" maxlength="50" minlength="5" autofocus required class="form-control form-control-sm"/>
					<div class="invalid-feedback"></div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="user-password" class="required"><?php echo $this->trans('Password'); ?>:</label>
					<input type="password" name="password" id="user-password" maxlength="30" minlength="6" required class="form-control form-control-sm"/>
					<div class="invalid-feedback"></div>
				</div>
			</div>
			<div class="form-row align-items-center">
				<div class="col-auto">
					<input type="submit" value="<?php echo $this->trans('Sign in'); ?>" class="btn btn-primary"/>
				</div>
				<div class="col-auto">
					<a href="<?php echo $this->path('password_forgot'); ?>" target="_self"><?php echo $this->trans('Forgot password?'); ?></a>
				</div>
			</div>
		</form>
	</div>
</div>
