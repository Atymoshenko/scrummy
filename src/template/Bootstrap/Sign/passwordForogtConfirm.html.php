<?php
/** @var bool $success */
?>
<?php $this->extend(['file' => 'layout.html.php']); ?>

<h1><?php echo $this->trans('Password recovery'); ?></h1>

<?php if ($success): ?>
<div data-component="form-password-reset">
	<form method="post" action="<?php echo $this->path('apiProfilePasswordReset'); ?>" name="password-reset" novalidate>
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="user-password" class="required"><?php echo $this->trans('New password'); ?>:</label>
				<input type="password" name="password" id="user-password" maxlength="50" minlength="5" autofocus required class="form-control form-control-sm" />
				<div class="invalid-feedback"></div>
			</div>
		</div>
		<div>
			<input type="submit" value="<?php echo $this->trans('Save'); ?>" class="btn btn-primary" />
		</div>
	</form>
</div>

<div class="progress loader hide">
	<div class="progress-bar progress-bar-striped progress-bar-animated"></div>
</div>

<div data-component="form-password-reset-success" class="hide">
	<div class="alert alert-success"><?php echo $this->trans('Your password has been successfully changed.'); ?></div>
</div>
<?php else: ?>
	<div class="alert alert-danger"><?php echo $this->trans('Wrong or expired url.'); ?></div>
<?php endif; ?>

