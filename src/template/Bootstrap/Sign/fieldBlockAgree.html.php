<?php $count = $count ?? 0; ?>
<div class="custom-control custom-checkbox mt-2">
	<input type="checkbox" class="custom-control-input" id="agree_<?php echo $count; ?>" name="agree" value="Y" required>
	<label class="custom-control-label" for="agree_<?php echo $count; ?>"><?php echo $this->trans('By clicking &laquo;Create account&raquo; you agree to the'); ?> <a href="/privacyPolicy.html" target="_blank"><?php echo $this->trans('privacy policy'); ?></a> <?php echo $this->trans('and'); ?> <a href="/rules.html" target="_blank"><?php echo $this->trans('rules'); ?></a> <?php echo $this->trans('of this site'); ?>.</label>
	<div class="invalid-feedback"></div>
</div>
