<?php $count = $count ?? 0; ?>
<div class="form-row">
	<div class="form-group col-md-6">
		<label for="user-nickname_<?php echo $count; ?>" class="required"><?php echo $this->trans('Nickname'); ?>:</label>
		<input type="text" name="nickname" id="user-nickname_<?php echo $count; ?>" maxlength="25" minlength="2" required class="form-control form-control-sm"/>
		<div class="invalid-feedback"></div>
	</div>
</div>
