<p>We prohibit to attend Scrummy organizations or individuals who are engaged in the following activities:</p>
<ul>
	<li>Terrorist activity;</li>
	<li>Organized hate;</li>
	<li>Mass or serial murder;</li>
	<li>Human trafficking;</li>
	<li>Organized violence or criminal activity.</li>
</ul>
<p>Calls for violence or statements advocating violence against the following targets (identified by name, title, image, or other reference).</p>
<p>We prohibit to distribute instructions on how to make or use weapons if the goal is to injure or kill people.</p>
<p>We prohibit to distribute instructions for the manufacture or use of explosives in the absence of a context that clearly indicates that the content is intended for a non-violent purpose.</p>
<p>We do not allow content that sexually exploits or endangers children.</p>
<p>The administration of Scrummy is not responsible for the publication of photos and videos of sexual content posted by users.</p>
