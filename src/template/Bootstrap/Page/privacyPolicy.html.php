<?php $this->extend(['file' => 'layout.html.php']); ?>

<h1><?php echo $this->trans('Privacy policy'); ?></h1>
<?php $this->include('Page/privacyPolicyContent.' . $this->getLocale() . '.html.php'); ?>