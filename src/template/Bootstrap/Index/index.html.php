<?php
use App\Entity\Group;

/**
 * @var Group[] $topGroups
 * @var array $chatMessages
 */

$this->extend(['file' => 'layout.html.php']);
?>

<div class="s-wrapper s-wrapper-fixed s-index">
	<div class="s-middle">
		<div class="s-container">
			<div class="s-content">
				<h1><?php echo $this->trans('Scrummy'); ?></h1>
				<p><?php echo $this->trans('Chat with people around the world. Create groups. Find people by interests.'); ?></p>
			</div>
		</div>
		<div class="s-left-sidebar">
			<h2><?php echo $this->trans('Top groups'); ?></h2>
			<div class="groups-top">
				<ul class="noType">
					<?php foreach ($topGroups as $topGroup): ?>
					<li>
						<h3><a href="<?php echo $this->path('group', ['groupId' => $topGroup->getId()]); ?>" target="_self"><?php echo htmlspecialchars($topGroup->getName()); ?></a></h3>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="s-right-sidebar">
			<h2><?php echo $this->trans('Online chat'); ?></h2>
			<div class="chat chat-general">
				<?php foreach ($chatMessages as $message): ?>
					<?php $nicknameToString = $message['nicknameTo'] ? '&gt;<span>' . $message['nicknameTo'] . '</span>:' : ''; ?>
					<div><span><?php echo (\DateTime::createFromFormat('Y-m-d H:i:s', $message['created']))->format('m/d H:i:s'); ?></span>&nbsp;<span><?php echo htmlspecialchars($message['nickname']); ?></span><?php echo $nicknameToString; ?>&nbsp;<span><?php echo preg_replace("~&lt;(.+?)&gt;~m", "<img src=\"images/smiles/$1.gif\" alt=\"\" />", htmlspecialchars($message['message'])); ?></span></div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
