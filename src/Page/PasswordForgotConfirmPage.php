<?php
namespace App\Page;

use App\Entity\UserPasswordReset;
use App\Framework\Response;

class PasswordForgotConfirmPage extends Page {
	public function run(): Response
	{
		$success = false;
		$userId = trim($this->getApp()->getRequest()->get('id', null));
		$code = trim($this->getApp()->getRequest()->get('code', null));

		if ($userId && $code) {
			/** @var UserPasswordReset $userPasswordReset */
			$userPasswordReset = $this->getApp()->getDb()->findOneBy(['userId' => $userId], UserPasswordReset::class);
			if ($userPasswordReset && $userPasswordReset->isExpired()) {
				$this->getApp()->getDb()->remove($userPasswordReset);
				$userPasswordReset = null;
			}
			if ($userPasswordReset && $userPasswordReset->getCode() === $code) {
				$success = true;
			}
		}

		if (!$success) {
			$response = new Response();
			$response->setStatusCode(404);
		}

		$params = [
			'success' => $success,
		];

		return $this->getApp()->renderAjaxOrHtml('Sign/passwordForogtConfirm.html.php', $params, $response ?? null);
	}
}