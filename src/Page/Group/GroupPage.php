<?php
namespace App\Page\Group;

use App\Entity\Group;
use App\Framework\Exception\NotFoundException;
use App\Framework\Response;
use App\Page\Page;

class GroupPage extends Page {
	public function run(): Response
	{
		$groupId = $this->getApp()->getRoute()->getRouteParams('groupId');
		/** @var Group $group */
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new NotFoundException();
		}

		$params = [
			'group' => $group,
		];

		return $this->getApp()->renderAjaxOrHtml('Group/group.html.php', $params);
	}
}