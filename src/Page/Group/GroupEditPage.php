<?php
namespace App\Page\Group;

use App\Entity\Group;
use App\Framework\Exception\NotFoundException;
use App\Framework\Response;
use App\Page\Page;

class GroupEditPage extends Page {
	public function run(): Response
	{
		$groupId = $this->getApp()->getRoute()->getRouteParams('groupId');
		$group = $this->getApp()->getDb()->findOneBy(['id' => $groupId], Group::class);
		if (!$group) {
			throw new NotFoundException();
		}

		$params = [
			'groupId' => $groupId,
		];

		return $this->getApp()->renderAjaxOrHtml('Group/groupEdit.html.php', $params);
	}
}