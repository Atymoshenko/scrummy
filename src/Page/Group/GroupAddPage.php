<?php
namespace App\Page\Group;

use App\Framework\Response;
use App\Page\Page;

class GroupAddPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Group/groupCreate.html.php');
	}
}