<?php
namespace App\Page\Group;

use App\Framework\Response;
use App\Page\Page;

class GroupsPage extends Page {
	public function run(): Response
	{
		$params = [
			'countries' => $this->getApp()->getDb()->getCountries(),
		];

		return $this->getApp()->renderAjaxOrHtml('Group/groups.html.php', $params);
	}
}