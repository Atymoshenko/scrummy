<?php
namespace App\Page;

use App\Framework\Response;

class ProfileEditConfirmPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Profile/profileEditConfirm.html.php');
	}
}