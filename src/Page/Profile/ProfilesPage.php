<?php
namespace App\Page\Profile;

use App\Framework\Response;
use App\Page\Page;

class ProfilesPage extends Page {
	public function run(): Response
	{
		$params = [
			'countries' => $this->getApp()->getDb()->getCountries(),
		];

		return $this->getApp()->renderAjaxOrHtml('Profile/profiles.html.php', $params);
	}
}