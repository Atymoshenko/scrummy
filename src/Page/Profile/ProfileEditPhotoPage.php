<?php
namespace App\Page\Profile;

use App\Framework\Response;
use App\Page\Page;

class ProfileEditPhotoPage extends Page {
	public function run(): Response
	{
		$params = [
			'photosCountMax' => $this->getApp()->getConfig()['userPhoto']['maxCount'],
		];

		return $this->getApp()->renderAjaxOrHtml('Profile/profileEditPhoto.html.php', $params);
	}
}