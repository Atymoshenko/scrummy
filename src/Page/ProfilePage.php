<?php
namespace App\Page;

use App\Entity\Country;
use App\Entity\Photo;
use App\Entity\User;
use App\Entity\UserGoogle;
use App\Entity\UserProfile;
use App\Framework\Exception\NotFoundException;
use App\Framework\Response;

class ProfilePage extends Page {
	public function run(): Response
	{
		$userId = $this->getApp()->getRoute()->getRouteParams('userId');
		/** @var User $user */
		$user = $this->getApp()->getDb()->findOneBy(['id' => $userId], User::class);
		if (!$user) {
			throw new NotFoundException();
		}
		/** @var UserProfile $userProfile */
		$userProfile = $this->getApp()->getDb()->findOneByOrCreate(['userId' => $user->getId()], UserProfile::class, new UserProfile($user));
		/** @var UserGoogle|null $userGoogle */
		$userGoogle = $this->getApp()->getDb()->findOneBy(['userId' => $user->getId()], UserGoogle::class);
		/** @var Country|null $country */
		$country = null;
		if ($userProfile->getCountryCode()) {
			$country = $this->getApp()->getDb()->findOneBy(['code' => $userProfile->getCountryCode()], Country::class);
		}
		/** @var Photo|null $photo */
		$photo = $this->getApp()->getDb()->findOneBy(['userId' => $user->getId()], Photo::class);
		$photoUrlThumb = null;
		if ($photo) {
			$photoUrlThumb = $this->getApp()->getPathUserPhotosRelative($user->getId()) . 'thumb_' . $photo->getId() . '.jpg';

		}

		$params = [
			'user' => $user,
			'userProfile' => $userProfile,
			'userGoogle' => $userGoogle,
			'country' => $country,
			'photoUrlThumb' => $photoUrlThumb,
		];

		return $this->getApp()->renderAjaxOrHtml('Profile/profile.html.php', $params);
	}
}