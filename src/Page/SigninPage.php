<?php
namespace App\Page;

use App\Framework\Response;

class SigninPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Sign/signin.html.php');
	}
}