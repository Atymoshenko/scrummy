<?php
namespace App\Page;

use App\Framework\ {
	App,
	Interfaces\iPage
};

abstract class Page implements iPage {
	private $app;

	public function __construct(App $app)
	{
		$this->app = $app;
	}

	protected function getApp(): App
	{
		return $this->app;
	}
}