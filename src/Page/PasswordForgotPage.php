<?php
namespace App\Page;

use App\Framework\Response;

class PasswordForgotPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Sign/passwordForogt.html.php');
	}
}