<?php
namespace App\Page;

use App\Framework\Response;

class ProfileEditPasswordPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Profile/profileEditPassword.html.php');
	}
}