<?php
namespace App\Page;

use App\Entity\BaseMessage;
use App\Framework\Response;

class ChatPage extends Page {
	public function run(): Response
	{
		$params = [
			'isDev' => $this->getApp()->isDev(),
			'chatMode' => 'chat',
		];

		return $this->getApp()->renderAjaxOrHtml('Chat/chat.html.php', $params);
	}
}