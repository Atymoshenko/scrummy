<?php
namespace App\Page;

use App\Framework\Response;

class StaticPage extends Page {

	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Page/' . $this->getApp()->getRoute()->getPath() . '.php');
	}
}