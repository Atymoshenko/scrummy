<?php
namespace App\Page;

use App\Entity\Country;
use App\Framework\Response;

class ProfileEditProfilePage extends Page {
	public function run(): Response
	{
		$locales = [];
		foreach ($this->getApp()->getLocales() as $key=>$locale) {
			if (!$locale['enabled']) {
				continue;
			}
			$locales[$key] = $locale;
		}

		$params = [
			'locales' => $locales,
			'countries' => $this->getApp()->getDb()->getCountries(),
		];

		return $this->getApp()->renderAjaxOrHtml('Profile/profileEditProfile.html.php', $params);
	}
}