<?php
namespace App\Page;

use App\Framework\Response;

class SignupPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Sign/signup.html.php');
	}
}