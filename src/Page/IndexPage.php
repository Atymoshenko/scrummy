<?php
namespace App\Page;

use App\Framework\Response;

class IndexPage extends Page {
	public function run(): Response
	{
		return $this->getApp()->renderAjaxOrHtml('Index/index.html.php', [
			'topGroups' => $this->getApp()->getDb()->getTopGroups(),
			'chatMessages' => $this->getApp()->getDb()->getMessagesForHomePage(),
		]);
	}
}